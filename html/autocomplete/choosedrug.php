<?php

    // Include the code requried for connections and functions.
    include_once '../connection.php';
    include_once '../code/functions.php';
    include_once '../code/sql_statements.php';

    // Get the search term from the parameters.
    $searchTerm = $_GET['term'];

    // Get the sql.
    $get_drugs = sql_get_drugs_by_search_term($searchTerm);

    // Get the drugs.
    $drugs = getAssociativeArray($get_drugs);

    // Step through each drug and get ready for JSON.
    foreach ($drugs as $drug) {
        $data[] = [
            'drugCode' => $drug['drugCode'],
            'drugInfo' => ucfirst(strtolower($drug['drugInfo'])),
        ];
    }

    echo json_encode($data);
?>