<?php

    // Check if ohip is empty and valid.
    if(empty(trim($_POST["ohipID"]))){
        $has_errors = TRUE;
        $errors['ohipID'] = "Please enter OHIP number.";
    }
    else{
        if (
            preg_match("/^[0-9]{10}[A-Z]{2}$/", $_POST["ohipID"]) &&
            preg_match("/[1-9]{1}[0-9]{8}/", $_POST["ohipID"])
        ) {
            $fields['ohipID'] = trim($_POST["ohipID"]);
        }
        else {
            $has_errors = TRUE;
            $errors['ohipID'] = "Please enter a valid OHIP number.";
            $fields['ohipID'] = trim($_POST["ohipID"]);
        }
    }

    // Get the insurance policy ID.
    $fields['insurancePolicyID'] = trim($_POST["insurancePolicyID"]);
?>