<?php

    // Get the patient info from the diagnosis id.
    $get_patient = sql_get_user_from_diagnosis($type);
    $patient = getAssociativeArray($get_patient);

    // Get the info about the diagnosis.
    $get_diagnosis = sql_get_diagnosis($type);
    $diagnosis = getAssociativeArray($get_diagnosis);

    // Set the fields based on the diagnosis id.
    $fields['diseaseDescription'] = $diagnosis[0]['diagnosisDescription'];
    $fields['diseaseID'] = $diagnosis[0]['diseaseID'];
    $fields['diseaseInfo'] = $diagnosis[0]['diseaseName'];

    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag for has errors.
        $has_errors = FALSE;

        // The array of fields.
        $fields = [];

        // The array of errors.
        $errors = [];

        // Check if description is empty
        if(empty(trim($_POST["diseaseDescription"]))){
            $has_errors = TRUE;
            $errors['diseaseDescription'] = "Please enter a description.";
        }
        else{
            $fields['diseaseDescription'] = trim($_POST["diseaseDescription"]);
        }

        // Check if disease is empty
        if(empty(trim($_POST["diseaseID"])) || empty(trim($_POST["diseaseInfo"]))){
            $has_errors = TRUE;
            $errors['diseaseInfo'] = "Please enter a disease.";
        }
        else{
            $fields['diseaseInfo'] = trim($_POST["diseaseInfo"]);
            $fields['diseaseID'] = trim($_POST["diseaseID"]);
        }


        // If there are no errors, update the diagnosis.
        if (!$has_errors) {

            // Add the fields needed for the diagnosis.
            $fields['doctorID'] = $_SESSION['userID'];
            $fields['diagnosisDate'] = date('Y-m-d');

            // Try the update diagnosis.
            $update_diagnosis = sql_update_diagnosis($type, $fields);
            $update_diagnosis_flag = updateQuery($update_diagnosis);

            if ($update_diagnosis_flag) {
                $_SESSION['success_message'] = 'The diagnosis has successfully been udpated.';

                Header("Location: /patient/view/" . $patient[0]['patientID']);
            }
            else {
                $error_message = 'There was an error updating the diagnosis.';
            }
        }
    }
?>