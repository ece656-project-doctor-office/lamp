container_id=$1

filenames=`ls loader/*`

for eachfile in $filenames
do
  echo "Copying ... $eachfile"
  docker cp $eachfile $container_id:/$eachfile
done
