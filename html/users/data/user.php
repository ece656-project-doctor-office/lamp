<div class="form-group">
    <label>First name</label>
    <input 
        type="text"
        name="firstName"
        class="form-control <?php echo (!empty($errors['firstName'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['firstName'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['firstName']; ?></span>
</div>

<div class="form-group">
    <label>Last name</label>
    <input
        type="text"
        name="lastName"
        class="form-control <?php echo (!empty($errors['lastName'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['lastName'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $fields['lastName']; ?></span>
</div>

<?php if ($entity !== 'profile') { ?>
    <div class="form-group">
        <label>Date of birth</label>
        <input
            type="text"
            id="popupDatepicker"
            name="dateOfBirth"
            class="form-control <?php echo (!empty($errors['dateOfBirth'])) ? 'is-invalid' : ''; ?>"
            value="<?php echo $fields['dateOfBirth'] ?? ''; ?>"
        >
        <span class="invalid-feedback"><?php echo $errors['dateOfBirth']; ?></span>
    </div>
<?php } else { ?>
    <input type="hidden" name="dateOfBirth" value="<?php echo $fields['dateOfBirth'] ?? ''; ?>">
<?php } ?>

<div class="form-group">
    <label>Phone</label>
    <input
        type="text"
        name="phone"
        class="form-control <?php echo (!empty($errors['phone'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['phone'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['phone']; ?></span>
</div>

<div class="form-group">
    <label>Email</label>
    <input
        type="text"
        name="email"
        class="form-control <?php echo (!empty($errors['email'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['email'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['email']; ?></span>
</div>

<div class="form-group">
    <label>Gender</label>
    <select name="gender" class="form-control <?php echo (!empty($errors['gender'])) ? 'is-invalid' : ''; ?>">
        <option value="">-- Select --</option>
        <option value="M" <?php if (isset($fields['gender']) && $fields['gender'] == "M") { echo 'selected'; } ?>>Male</option>
        <option value="F" <?php if (isset($fields['gender']) && $fields['gender'] == "F") { echo 'selected'; } ?>>Female</option>
        <option value="N" <?php if (isset($fields['gender']) && $fields['gender'] == "N") { echo 'selected'; } ?>>Nuetral</option>
    </select>
    <span class="invalid-feedback"><?php echo $errors['gender']; ?></span>
</div>

<div class="form-group">
    <label>Address line one</label>
    <input
        type="text"
        name="addressLineOne"
        class="form-control <?php echo (!empty($errors['addressLineOne'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['addressLineOne'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['addressLineOne']; ?></span>
</div>

<div class="form-group">
    <label>Address line two</label>
    <input
        type="text"
        name="addressLineTwo"
        class="form-control <?php echo (!empty($errors['addressLineTwo'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['addressLineTwo'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['addressLineTwo']; ?></span>
</div>

<div class="form-group">
    <label>City</label>
    <input
        type="text"
        name="city"
        class="form-control <?php echo (!empty($errors['city'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['city'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['city']; ?></span>
</div>

<div class="form-group">
    <label>Province</label>
    <select name="province" class="form-control <?php echo (!empty($errors['province'])) ? 'is-invalid' : ''; ?>">
        <option value="">-- Select --</option>
        <?php foreach ($provinces as $prov) { ?>
            <option value="<?php echo $prov['abbr']; ?>"<?php if (isset($fields['province']) && $fields['province'] == $prov['abbr']) { echo 'selected'; } ?>>
                <?php echo $prov['fullName']; ?>
            </option>
        <?php } ?>
    </select>
    <span class="invalid-feedback"><?php echo $errors['province']; ?></span>
</div>

<div class="form-group">
    <label>Postal code</label>
    <input
        type="text"
        name="postalCode"
        class="form-control <?php echo (!empty($errors['postalCode'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['postalCode'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['postalCode']; ?></span>
</div>

<?php if ($action == 'add') { ?>
    <input type="hidden" value="1" name="status" />
<? } elseif ($entity !== 'profile') { ?>
    <div class="form-group">
        <label>Status</label>
        <select
            name="status"
            class="form-control <?php echo (!empty($errors['select'])) ? 'is-invalid' : ''; ?>"
        >
            <option value="1" <?php if (isset($fields['status']) && $fields['status'] == 1) { echo 'selected'; } ?>>Active</option>
            <option value="0" <?php if (isset($fields['status']) && $fields['status'] == 0) { echo 'selected'; } ?>>Inactive</option>
        </select>
        <span class="invalid-feedback"><?php echo $errors['status']; ?></span>
    </div>
<?php } else { ?>
    <input type="hidden" name="status" value="<?php echo $fields['status'] ?? ''; ?>" />
<?php } ?>