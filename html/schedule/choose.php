<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous">
</script>
<script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
    integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
    crossorigin="anonymous">
</script>
<link rel="stylesheet" href="/css/jquery-ui.min.css">
<script type="text/javascript">
    $(function() {
        $("#userInfo").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/autocomplete/chooseuser.php?userRole=<?php echo $type; ?>&term="+request.term,
                    dataType: "json",
                    type: "POST",
                    data: {
                        keyword: request.term
                    },
                    success: function(data){
                        response( $.map( data, function( item ) {
                            return {
                                    label: item.userName,
                                    value: item.userName,
                                    id: item.userID
                                }
                            
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $("#userID").val(ui.item.id);  // ui.item.value contains the id of the selected label
            }
        });
    });
</script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 500px; padding: 20px; }
</style>

<div class="doc-office__choseuser">
    <div class="wrapper">
        <h2>Edit a <?php echo $type; ?> schedule</h2>
        <form action="/schedule/choose/<?php echo $type; ?>" method="post">

            <div class="form-group ui-widget">
                <label>Enter a <?php echo $type; ?></label>
                <input
                    type="text"
                    id="userInfo"
                    name="userInfo"
                    class="form-control <?php echo (!empty($errors['userInfo'])) ? 'is-invalid' : ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['userInfo']; ?></span>
            </div>

            <input type="hidden" name="userID" id="userID" />
            <input type="hidden" name="type" value="<?php echo $type; ?>" />

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
    </div>
</div>