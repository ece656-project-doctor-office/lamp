<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous">
</script>
<script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
    integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
    crossorigin="anonymous">
</script>
<link rel="stylesheet" href="/css/jquery-ui.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 500px; padding: 20px; }
</style>
<script type="text/javascript">
    $(function() {
        $("#userInfo").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/autocomplete/chooseuser.php?userRole=patient&term="+request.term,
                    dataType: "json",
                    type: "POST",
                    data: {
                        keyword: request.term
                    },
                    success: function(data){
                        response( $.map( data, function( item ) {
                            return {
                                    label: item.userName,
                                    value: item.userName,
                                    id: item.userID
                                }
                            
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $("#userID").val(ui.item.id);  // ui.item.value contains the id of the selected label
            }
        });
    });
</script>

<?php
    $get_doctor = sql_get_user_first_last_name($value3);
    $doctor = getAssociativeArray($get_doctor);
?>
<div class="doc-office__addappointment">

    <?php if (isset($_SESSION['error_message'])) {?>
        <div class="error-message">
            <?php 
                echo $_SESSION['error_message'];
                $_SESSION['error_message'] = NULL;
            ?>
        </div>
    <?php } ?>

    <div class="wrapper">
        <h2>Add an appointment</h2>

        <div class="form-group ui-widget">
            Date: <?php echo date('l F d, Y', strtotime($type)); ?><br />
            Time: <?php echo date('g:i a', $value) . ' - ' . date('g:i a', $value2); ?><br />
            Doctor: <?php echo $doctor[0]['lastName'] . ', ' . $doctor[0]['firstName']; ?>
        </div> 

        <form action="/appointments/add/<?php echo $type; ?>/<?php echo $value; ?>/<?php echo $value2; ?>/<?php echo $value3; ?>" method="post">

            <div class="form-group ui-widget">
                <label>Patient</label>
                <input
                    type="text"
                    id="userInfo"
                    name="userInfo"
                    class="form-control <?php echo (!empty($errors['userInfo'])) ? 'is-invalid' : ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['userInfo'] ?? ''; ?></span>
            </div>

            <input type="hidden" name="userID" id="userID" />

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
    </div>
</div>