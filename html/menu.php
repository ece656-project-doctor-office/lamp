<div class="doc-office__menu">
    <div class="contained-width menu">
        <ul>
            <li>
                <a href="/">Home</a>
            </li>
            <?php if($logged_in && ($_SESSION['userRole'] == 'manager' || $_SESSION['userRole'] == 'admin')) { ?>
                <li>
                    <a href="/manage">Manage</a>
                </li>
            <?php } ?>
            <?php if($logged_in && ($_SESSION['userRole'] == 'doctor' || $_SESSION['userRole'] == 'admin')) { ?>
                <?php if ($_SESSION['userRole'] == 'doctor') { ?>
                <li>
                    <a href="/appointments/view">Appointments</a>
                </li>
                <?php } ?>
                <li>
                    <a href="/patient/choose">Patients</a>
                </li>
                <li>
                    <a href="/drugs/choose">Drugs</a>
                </li>
            <?php } ?>
            <?php if($logged_in && $_SESSION['userRole'] !== 'patient') { ?>
                <li>
                    <a href="/reports/reports">Reports</a>
                </li>
            <?php } ?>
            <?php if($logged_in && $_SESSION['userRole'] == 'admin') { ?>
                <li>
                    <a href="/generate/generate">Generate</a>
                </li>
            <?php } ?>
            <?php if($logged_in) { ?>
                <li>
                    <a href="/profile/edit">Profile</a>
                </li>
            <?php } ?>
            <li>
                <?php if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) { ?>
                    <a href="/users/logout">Logout</a>
                <?php } else { ?>
                    <a href="/users/login">Login</a>
                <?php } ?>
            </li>
        </ul>
    </div>
</div>
