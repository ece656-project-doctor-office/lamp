SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: officeDB
--

DROP DATABASE IF EXISTS officeDB;

CREATE DATABASE officeDB;

use officeDB;

--
-- Create OfficeInfo table
--

CREATE TABLE OfficeInfo (
  name varchar(200) NOT NULL,
  address varchar(200) NOT NULL,
  city varchar(50) NOT NULL,
  province char(2) NOT NULL,
  postalCode char(8) NOT NULL,
  phone varchar(50) NOT NULL,
  fax varchar(50),
  email varchar(200),
  welcomeMessage varchar(2000) NOT NULL
);

--
-- Insert default office info
-- 

INSERT INTO OfficeInfo VALUES (
  'University of Waterloo',
  '200 University Avenue West',
  'Waterloo',
  'ON',
  'N2L 3G1',
  '(519) 888-4567',
  '(519) 888-4455',
  'offce@someoffice.com',
  '
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim diam vulputate ut pharetra sit amet. Accumsan tortor posuere ac ut consequat semper viverra nam. Cursus metus aliquam eleifend mi in nulla posuere sollicitudin. Sed vulputate odio ut enim blandit volutpat maecenas. In aliquam sem fringilla ut morbi tincidunt augue interdum. Dignissim sodales ut eu sem. Leo vel orci porta non pulvinar neque laoreet suspendisse. Nec feugiat in fermentum posuere urna nec tincidunt. Nec feugiat nisl pretium fusce id velit ut. In cursus turpis massa tincidunt dui ut ornare lectus sit. Nulla porttitor massa id neque aliquam vestibulum morbi. Vestibulum lorem sed risus ultricies tristique. Pretium lectus quam id leo in vitae turpis. In est ante in nibh mauris cursus mattis molestie a. Vestibulum morbi blandit cursus risus at ultrices mi tempus imperdiet. Eget felis eget nunc lobortis mattis. Lobortis mattis aliquam faucibus purus in massa tempor.</p>
    <p>Odio euismod lacinia at quis risus sed vulputate. Sagittis aliquam malesuada bibendum arcu vitae. Elementum eu facilisis sed odio morbi quis commodo odio. Blandit aliquam etiam erat velit scelerisque in dictum. Lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor. Semper eget duis at tellus at urna. Turpis massa sed elementum tempus egestas sed sed risus. Aliquet bibendum enim facilisis gravida neque convallis. Parturient montes nascetur ridiculus mus. Eget velit aliquet sagittis id consectetur purus ut. Magnis dis parturient montes nascetur ridiculus mus. Arcu vitae elementum curabitur vitae. Mauris cursus mattis molestie a iaculis at. Consectetur purus ut faucibus pulvinar elementum integer enim neque. Elit duis tristique sollicitudin nibh. Dis parturient montes nascetur ridiculus mus. Tellus mauris a diam maecenas sed enim ut sem viverra. Sapien eget mi proin sed libero enim sed faucibus. Magna fermentum iaculis eu non diam phasellus vestibulum lorem sed.</p>
    <p>Sed vulputate mi sit amet mauris commodo quis imperdiet massa. Nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Eleifend donec pretium vulputate sapien nec sagittis aliquam malesuada bibendum. Eu non diam phasellus vestibulum lorem sed. Amet justo donec enim diam vulputate ut pharetra. Semper eget duis at tellus. Suspendisse sed nisi lacus sed viverra tellus in hac habitasse. Sed viverra tellus in hac habitasse platea dictumst vestibulum. Ac tortor vitae purus faucibus ornare suspendisse sed nisi. Faucibus turpis in eu mi.</p>
  '
);

--
-- Create UserRoles table
--

CREATE TABLE UserRoles (
  userRoleID int NOT NULL AUTO_INCREMENT,
  userRole varchar(20) NOT NULL,
  PRIMARY KEY (userRoleID)
);

--
-- Insert default User roles
-- 
INSERT INTO UserRoles (userRoleID, userRole) VALUES
(1, 'admin'),
(2, 'doctor'),
(3, 'manager'),
(4, 'patient');

--
-- Create Users table
--

CREATE TABLE Users (
  userID int NOT NULL AUTO_INCREMENT,
  username varchar(50) NOT NULL,
  password varchar(255) NOT NULL,
  userRoleID int NOT NULL,
  firstName varchar(25) NOT NULL,
  lastName varchar(30) NOT NULL,
  dateOfBirth date NOT NULL,
  phone varchar(10) NOT NULL,
  email varchar(125) NOT NULL,
  gender char(1) NOT NULL,
  addressLineOne varchar(200) NOT NULL,
  addressLineTwo varchar(200),
  city varchar(25) NOT NULL,
  province varchar(2) NOT NULL,
  postalCode varchar(6) NOT NULL,
  status tinyint(1) NOT NULL,
  FOREIGN KEY (userRoleID) REFERENCES UserRoles (userRoleID),
  PRIMARY KEY (userID),
  UNIQUE KEY (username),
  CHECK (regexp_like(postalCode, '^[0-9A-Z]{6}$') AND regexp_like(postalCode, '([A-Z]{1}[0-9]{1}){3}')),
  CHECK (regexp_like(phone, '^[0-9]{10}$') AND regexp_like(phone, '[1-9]{1}[0-9]{9}'))
);

--
-- Insert admin user into Users
--

INSERT INTO Users VALUES
(1, 'admin', 'admin', 1, 'adminstrator', 'administrator', '1979-12-18', '5195777918', 'admin@admin.com', 'M', '106 Pepperwood Crescent', '', 'Kitchener', 'ON', 'N2A2R3', 1);

--
-- Create Patients table
--

CREATE TABLE Patients (
  patientID int NOT NULL,
  insurancePolicyID varchar(25),
  ohipID varchar(25),
  FOREIGN KEY (patientID) REFERENCES Users (userID),
  PRIMARY KEY (patientID),
  CHECK (regexp_like(ohipID, '^[0-9]{10}[A-Z]{2}$') AND regexp_like(ohipID, '[1-9]{1}[0-9]{9}[A-Z]{2}'))
);

--
-- Create Provinces table
--

CREATE TABLE Provinces (
  fullName varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  abbr char(2) NOT NULL
);

--
-- Insert provinces into Provinces
--

INSERT INTO Provinces (fullName, abbr) VALUES
('Alberta', 'AB'),
('British Columbia', 'BC'),
('Manitoba', 'MB'),
('New Brunswick', 'NB'),
('Newfoundland and Labrador', 'NL'),
('Northwest Territories', 'NT'),
('Nova Scotia', 'NS'),
('Nunavut', 'NU'),
('Ontario', 'ON'),
('Prince Edward Island', 'PE'),
('Quebec', 'QC'),
('Saskatchewan', 'SK'),
('Yukon', 'YT');

--
-- Create Doctor table
--

CREATE TABLE Doctor (
  doctorID int NOT NULL,
  sin int NOT NULL, 
  designation char(30) NOT NULL,
  PRIMARY KEY (doctorID),
  FOREIGN KEY (doctorID) REFERENCES Users(userID),
  CHECK (regexp_like(sin, '^[0-9]{9}$') AND regexp_like(sin, '[1-9]{1}[0-9]{8}'))
);

CREATE INDEX DoctorIdIdx ON Doctor (doctorID);

--
-- Create DoctorSchedule table
--

CREATE TABLE DoctorSchedule (
  doctorID int NOT NULL,
  scheduleDate date NOT NULL,
  PRIMARY KEY (doctorID, scheduleDate),
  FOREIGN KEY (doctorID) REFERENCES Users(userID)
);

CREATE INDEX DoctorScheduleIdx ON DoctorSchedule (doctorID, scheduleDate);

--
-- Create Manager table
--

CREATE TABLE Manager (
  managerID int NOT NULL,
  sin int NOT NULL, 
  PRIMARY KEY (managerID),
  FOREIGN KEY (managerID) REFERENCES Users(userID),
  CHECK (regexp_like(sin, '^[0-9]{9}$') AND regexp_like(sin, '[1-9]{1}[0-9]{8}'))
);

CREATE INDEX MangerIdIdx ON Manager (managerID);

--
-- Create Appointment table
--

CREATE TABLE Appointment (
  appointmentID int NOT NULL AUTO_INCREMENT,
  doctorID int NOT NULL,
  patientID int NOT NULL,
  appointmentDate date NOT NULL,
  appointmentStartTime TIMESTAMP  NOT NULL,
  appointmentEndTime TIMESTAMP  NOT NULL,
  FOREIGN KEY (doctorID) REFERENCES Users(userID),
  FOREIGN KEY (patientID) REFERENCES Users(userID),
  PRIMARY KEY (appointmentID),
  CHECK (appointmentEndTime > appointmentStartTime)
);

CREATE INDEX AppointmentIdx1 ON Appointment (doctorID, appointmentDate);
CREATE INDEX AppointmentIdx2 ON Appointment (patientID, appointmentDate);

--
-- Create Appointment table
--

CREATE TABLE Disease (
  diseaseID int NOT NULL,
  diseaseName char(100) NOT NULL,
  PRIMARY KEY (diseaseID)
);

CREATE INDEX DiseaseIdx1 ON Disease (diseaseID);

--
-- Create Diagnosis table
--

CREATE TABLE Diagnosis (
  diagnosisID int NOT NULL AUTO_INCREMENT,
  doctorID int NOT NULL,
  patientID int NOT NULL,
  diagnosisDescription varchar(100) NOT NULL,
  diagnosisDate datetime not NULL,
  diseaseID int,
  PRIMARY KEY (diagnosisID),
  FOREIGN KEY (doctorID) REFERENCES Users(userID),
  FOREIGN KEY (patientID) REFERENCES Users(userID)
);

CREATE INDEX AppointmentIdx1 ON Diagnosis (patientID);

--
-- Create Prescription table
--

CREATE TABLE Prescription (
  prescriptionID int NOT NULL AUTO_INCREMENT,
  diagnosisID  int NOT NULL,
  prescription char(100),
  PRIMARY KEY (prescriptionID ),
  FOREIGN KEY (diagnosisID) REFERENCES Diagnosis(diagnosisID)
);

CREATE INDEX PrescriptionIdx1 ON Prescription (diagnosisID);

--
-- Create Drugs table
--

CREATE TABLE Drugs (
 drugCode int NOT NULL,
  productCategorization  char(200),
  class  char(100) NOT NULL,
  drugIdentificationNumber int NOT NULL,
  brandName char(200),
  descriptor char(200),
  pediatricFlag ENUM('Y', 'N'), 
  accessionNumber varchar(50),
  numberOfAis int, 
  lastUpdateDate date not null,
  aiGroupNo varchar(50),
  classF varchar(100),
  brandNameF varchar(500),
  descriptorF varchar(200),
  PRIMARY KEY (drugCode)
);

CREATE INDEX DrugsIdx1 ON Drugs (drugCode);

--
-- Create PrescriptionDrugs table
--

CREATE TABLE PrescriptionDrugs (
  prescriptionID int NOT NULL,
  drugCode int NOT NULL,
  dosage varchar(200) NOT NULL,
  instructions varchar(255),
  PRIMARY KEY (prescriptionID, drugCode),
  FOREIGN KEY (prescriptionID) REFERENCES Prescription(prescriptionID),
  FOREIGN KEY (drugCode) REFERENCES Drugs(drugCode) 
);

CREATE INDEX PrescriptionDrugsIdx1 ON PrescriptionDrugs (prescriptionID);
CREATE INDEX PrescriptionDrugsIdx2 ON PrescriptionDrugs (drugCode);

CREATE TABLE DrugStatus (
 drugCode int NOT NULL,
  statusFlag ENUM('Y', 'N'), 
  status varchar(50),
  historyDate date,
  statusF varchar(50),
  PRIMARY KEY (drugCode,historyDate),
  FOREIGN KEY (drugCode) REFERENCES Drugs(drugCode)
);

CREATE INDEX DrugStatusIdx1 ON DrugStatus (drugCode, historyDate);

CREATE TABLE DrugCompanies (
  companyCode int NOT NULL,
  companyName varchar(80), 
  companyType varchar(40),
  suiteNumber varchar(20),
  streetName varchar(80),
  cityName varchar(60),
  province varchar(40),
  country varchar(40),
  postalCode varchar(20),
  PRIMARY KEY (companyCode)
);

CREATE INDEX CompaniesIdx1 ON DrugCompanies (companyCode);

CREATE TABLE DrugSeller (
  companyCode int NOT NULL,
  drugCode int NOT NULL,
  PRIMARY KEY (drugCode,companyCode),
  FOREIGN KEY (drugCode) REFERENCES Drugs(drugCode),
  FOREIGN KEY (companyCode) REFERENCES DrugCompanies(companyCode)
);

CREATE INDEX DrugSellerIdx ON DrugSeller (drugCode,companyCode);


COMMIT;

COMMIT;
