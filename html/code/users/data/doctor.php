<?php

    // Check if sin is empty and valid.
    if(empty(trim($_POST["sin"]))){
        $has_errors = TRUE;
        $errors['sin'] = "Please enter SIN number.";
    }
    else {
        if (
            preg_match("/^[0-9]{9}$/", $_POST["sin"]) &&
            preg_match("/[1-9]{1}[0-9]{8}/", $_POST["sin"])
        ) {
            $fields['sin'] = trim($_POST["sin"]);
        }
        else {
            $has_errors = TRUE;
            $errors['sin'] = "Please enter a valid SIN number.";
            $fields['sin'] = trim($_POST["sin"]);
        }
    }

    // Check if designation is empty.
    if(empty(trim($_POST["designation"]))){
       $has_errors = TRUE;
       $errors['designation'] = "Please enter designation.";
   }
   else{
       $fields['designation'] = trim($_POST["designation"]);
   }

?>