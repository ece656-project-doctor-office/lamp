<?php
    $host = "mysql-server";
    $user = "root";
    $pass = "secret";
    $db = "officeDB";

    global $conn;

    try {

        // Create connection
        $conn = new mysqli(
            $host,
            $user,
            $pass,
            $db
        );

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

    } catch(PDOException $e) {
        die("Connection failed: " . $e->getMessage());
    }