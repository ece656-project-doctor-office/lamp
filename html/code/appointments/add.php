<?php

    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag for has errors.
        $has_errors = FALSE;

        // The array of fields.
        $fields = [];

        // The array of errors.
        $errors = [];

        // Check if user info is empty
        if(empty(trim($_POST["userInfo"]))){
            $has_errors = TRUE;
            $errors['userInfo'] = "Please enter a patient.";
        }
        else{
            $fields['userInfo'] = trim($_POST["userInfo"]);
        }

        // Check if user id is empty
        if(empty(trim($_POST["userID"]))){
            $has_errors = TRUE;
            $errors['userInfo'] = "Please enter a patient.";
        }
        else{
            $fields['userID'] = trim($_POST["userID"]);
        }

        if (!$has_errors) {

            // Set columns.
            $date = $type;
            $start_time = date('Y-m-d g:i:s', $value);
            $end_time = date('Y-m-d g:i:s', $value2);
            $doctorID = $value3;

            // Try to insert the appointment.
            $insert_appointment = sql_insert_appointment($fields['userID'], $doctorID, $date, $start_time, $end_time);
            $insert_appointment_flag = insertQuery($insert_appointment);

            // If insert worked, reset variables and success message.
            // If insert didn't work, set error message.
            if ($insert_appointment_flag) {

                // Reset the varaibles.
                $fields = [];
                $errors = [];

                // Set the success message.
                $_SESSION['success_message'] = 'The appointment has successfully been added.';

                // Send back to the appointment edit page.
                header("Location: /appointments/edit/" . $date);
            }
            else {

                // Set the error message, will stay on this page.
                $_SESSION['error_message'] = 'There was an error adding the appointment.';
            }
        }
    }
?>