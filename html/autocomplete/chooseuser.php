<?php

    // Include the code requried for connections and functions.
    include_once '../connection.php';
    include_once '../code/functions.php';
    include_once '../code/sql_statements.php';

    // Get the search term from the parameters.
    $searchTerm = $_GET['term'];

    // Get the user role from the parameters.
    $userRole = $_GET['userRole'];

    // Get the users via a role.
    $get_users = sql_get_users_by_role_and_search_term($userRole, $searchTerm, 'lastName', 'all');

    // Get the users.
    $users = getAssociativeArray($get_users);

    // Step through each user and set the data for the autocomplete.
    foreach ($users as $user) {
        $data[] = [
            'userName' => $user['lastName'] . ', ' . $user['firstName'] . ' (' . $user['dateOfBirth'] . ')',
            'userID' => $user['userID']
        ];
    }

    echo json_encode($data);
?>