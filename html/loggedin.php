<?php if (isset($_SESSION['loggedin'])) { ?>

    <?php
        if ($_SESSION['loggedin']) {
            $user_info = sql_get_user_first_last_name($_SESSION['userID']);
            $user_info = getAssociativeArray($user_info);
        }
    ?>

    <div class="doc-office__loggedin">
        <div class="contained-width">
            <?php if (isset($user_info)) { ?>
                <span class="doc-office__user-info">
                    <?php
                        if ($user_info[0]['firstName']) {
                            echo $user_info[0]['firstName'] . ' ' . $user_info[0]['lastName'];
                        }
                        else {
                            echo 'Administrator';
                        }
                    ?>
                </span>
            <?php } ?>
        </div>
    </div>
<?php } ?>