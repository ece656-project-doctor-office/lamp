<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="/css/jquery.datepick.css" rel="stylesheet">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 500px; padding: 20px; }
</style>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/jquery.plugin.min.js"></script>
<script src="/js/jquery.datepick.js"></script>

<script>
$(function() {
	$('#popupDatepicker').datepick({dateFormat: 'yyyy-mm-dd', yearRange: 'c-90:c+0'});
});
</script>

<div class="doc-office__profile">

    <?php if (isset($success_message)) {?>
        <div class="success-message">
            <?php echo $success_message; ?>
        </div>
    <?php } ?>

    <?php if (isset($error_message)) {?>
        <div class="error-message">
            <?php echo $error_message; ?>
        </div>
    <?php } ?>

    <div class="wrapper">
        <h2>Edit profile</h2>
        <form action="/profile/edit" method="post">

            <?php 
                include_once 'users/data/user.php'; 

                switch ($_SESSION['userRole']) {
                    case 'patient':
                        include_once 'users/data/patient.php';
                        break;
                    case 'doctor':
                        include_once 'users/data/doctor.php';
                        break;
                    case 'manager':
                        include_once 'users/data/manager.php';
                        break;
                }
            ?>

            <input type="hidden" name="type" value="<?php echo $type; ?>" />

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
    </div>
</div>