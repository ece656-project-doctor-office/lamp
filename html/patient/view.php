<?php
    $get_patient_info = sql_get_user_info($type);
    $patient_info = getAssociativeArray($get_patient_info);

    $get_patient_diagnosis = sql_get_patient_diagnosis($type);
    $patient_diagnosis = getAssociativeArray($get_patient_diagnosis);

    $get_patient_prescriptions = sql_get_all_perscriptions($type);
    $patient_prescriptions = getAssociativeArray($get_patient_prescriptions);    
?>
<div class="doc-office__patient--view">

    <?php if (isset($_SESSION['success_message'])) {?>
        <div class="success-message">
            <?php 
                echo $_SESSION['success_message']; 
                $_SESSION['success_message'] = NULL;
            ?>
        </div>
    <?php } ?>

    <?php if (isset($_SESSION['error_message'])) {?>
        <div class="error-message">
            <?php 
                echo $_SESSION['error_message'];
                $_SESSION['error_message'] = NULL;
            ?>
        </div>
    <?php } ?>

    <h2><?php echo $patient_info[0]['firstName'] . ' ' . $patient_info[0]['lastName']; ?> - Patient Record</h2>

    <details class="doc-office__details">
        <summary class="doc-office__details--summary">
            Details
        </summary>
        <div class="doc-office__details--content">
            <b>Date of Birth:</b> <?php echo date('M d, Y', strtotime($patient_info[0]['dateOfBirth'])); ?><br />
            <b>Age:</b> <?php echo get_age($patient_info[0]['dateOfBirth']); ?><br />
            <b>Gender:</b> <?php echo $patient_info[0]['gender']; ?><br />
            <b>Address:</b><br />
            <?php echo $patient_info[0]['addressLineOne']; ?><br />
            <?php echo $patient_info[0]['city']; ?>, <?php echo $patient_info[0]['province']; ?><br />
            <?php echo $patient_info[0]['postalCode']; ?>
            <br />
            <b>Phone:</b> <?php echo $patient_info[0]['phone']; ?><br />
            <b>Email:</b> <?php echo $patient_info[0]['email']; ?>  
        </div>
    </details>

    <details class="doc-office__details">
        <summary class="doc-office__details--summary">
            Diagnosis
        </summary>
        <div class="doc-office__details--content doc-office__table">
            <?php
                $html = '';

                if (count($patient_diagnosis) > 0) {

                    $html .= '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Description</th>';
                    $html .= '<th>Disease</th>';
                    $html .= '<th>Diagnosed by</th>';
                    $html .= '<th>Diagnosed On</th>';
                    $html .= '<th></th>';
                    $html .= '<th></th>';
                    $html .= '</tr>';

                    foreach ($patient_diagnosis as $pd) {
                        $html .= '<tr>';
                        $html .= '<td>' . $pd['diagnosisDescription'] . '</td>';
                        $html .= '<td>' . $pd['diseaseName'] . '</td>';
                        $html .= '<td>' . $pd['doctorLastName'] . ', ' . $pd['doctorFirstName'] . '</td>';
                        $html .= '<td>' . date('M j, Y', strtotime($pd['diagnosisDate'])) . '</td>';
                        $html .= '<td style="text-align: center"><a href="/diagnosis/edit/' . $pd['diagnosisID'] . '">Edit</a></td>';
                        $html .= '<td style="text-align: center"><a href="/diagnosis/delete/' . $pd['diagnosisID'] . '/' . $type . '">Delete</a></td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';
                }
                else {
                    $html .= '<div class="error-message">There are no diagnosis entered yet.</div>';
                }

                echo $html;
            ?>

            <div class="doc-office__actions">
                <span><a href="/diagnosis/add/<?php echo $type; ?>">Add</a></span>
            </div>
        </div>
    </details>

    <details class="doc-office__details">
        <summary class="doc-office__details--summary">
            Prescriptions
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '';

                    if (count($patient_prescriptions) > 0) {
                        $html .= '<table>';

                        $html .= '<tr>';
                        $html .= '<th>Prescription</th>';
                        $html .= '<th>Prescribed On</th>';
                        $html .= '<th>Prescribed By</th>';
                        $html .= '<th>Actions</th>';
                        $html .= '</tr>';

                        foreach ($patient_prescriptions as $patient_prescription) {
                            $html .= '<tr>';

                            $html .= '<td>';
                            if (isset($patient_prescription['drugName'])) {
                                $html .= '<b>Drug:</b> ';
                                $html .= '<a href="/drugs/view/' . $patient_prescription['drugCode'] . '">';
                                $html .= ucfirst(strtolower($patient_prescription['drugName']));
                                $html .= '</a>';
                                $html .= '<br />';
                                $html .= '<b>Dosage:</b> ' . $patient_prescription['dosage'];
                                if ($patient_prescription['instructions']) {
                                    $html .= '<br /><b>Instructions:</b> ' . $patient_prescription['instructions'];
                                }
                            }
                            else {
                                $html .= $patient_prescription['prescription'];
                            }
                            $html .= '</td>';

                            $html .= '<td>' . date('M d, Y', strtotime($patient_prescription['prescribedDate'])) . '</td>';
                            $html .= '<td>' . $patient_prescription['doctorLastName'] . ', ' . $patient_prescription['doctorFirstName'] . '</td>';

                            $html .= '<td>';
                            $html .= '<a href="/prescriptions/view/' . $patient_prescription['prescriptionID'] . '/' . $type . '">View</a>&nbsp;&nbsp;';
                            $html .= '<a href="/prescriptions/delete/' . $patient_prescription['prescriptionID'] . '/' . $type . '">Delete</a>';
                            $html .= '</td>';

                            $html .= '</tr>';
                        }

                        $html .= '</table>';
                    }
                    else {
                        $html .= '<div class="error-message">There are no perscriptions entered yet.</div>';
                    }

                    echo $html;
                ?>

                <div class="doc-office__actions">
                    <span><a href="/prescriptions/add/<?php echo $type; ?>">Add</a></span>
                </div>
            </div>
        </div>
    </details>
</div>