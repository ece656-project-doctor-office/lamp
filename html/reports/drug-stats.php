<?php
    $get_num_drugs = sql_get_num_drugs();
    $num_drugs = getAssociativeArray($get_num_drugs);

    $get_num_prescription_drugs = sql_get_num_prescription_drugs();
    $num_prescription_drugs = getAssociativeArray($get_num_prescription_drugs);

    $get_avg_drugs = sql_get_avg_drugs();
    $avg_drugs = getAssociativeArray($get_avg_drugs);

    $get_patients_with_max_drugs = sql_get_patients_with_max_or_min_drugs('max');
    $patients_with_max_drugs = getAssociativeArray($get_patients_with_max_drugs);

    $get_patients_with_min_drugs = sql_get_patients_with_max_or_min_drugs('min');
    $patients_with_min_drugs = getAssociativeArray($get_patients_with_min_drugs);
?>

<div class="doc-office__reports doc-office__reports--drug">
    <h2>Drug Statistics</h2>
    <h3></h3>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Details
        </summary>
        <div class="doc-office__details--content">
            <b>Number of drugs:</b> <?php echo $num_drugs[0]['numOfDrugs']; ?><br />
            <b>Number of drugs used in a prescription:</b> <?php echo $num_prescription_drugs[0]['numOfDrugs']; ?><br />
            <b>Average number of drugs per patient:</b> <?php echo $avg_drugs[0]['avgNumOfDrugs']; ?><br />
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Patient(s) with most drugs
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Patient</th>';
                    $html .= '<th>Number of Drugs</th>';
                    $html .= '<th>Age</th>';
                    $html .= '</tr>';

                    foreach ($patients_with_max_drugs as $patient_with_max_drugs) {
                        $html .= '<tr>';
                        $html .= '<td>';
                        $html .= '<a href="/patient/view/' . $patient_with_max_drugs['patientID'] . '">';
                        $html .= $patient_with_max_drugs['lastName'] . ', ' . $patient_with_max_drugs['firstName'];
                        $html .= '</a>';
                        $html .= '</td>';
                        $html .= '<td>' . $patient_with_max_drugs['numOfDrugs'] . '</td>';
                        $html .= '<td>' . $patient_with_max_drugs['age'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Patient(s) with least drugs
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Patient</th>';
                    $html .= '<th>Number of Drugs</th>';
                    $html .= '<th>Age</th>';
                    $html .= '</tr>';

                    foreach ($patients_with_min_drugs as $patient_with_min_drugs) {
                        $html .= '<tr>';
                        $html .= '<td>';
                        $html .= '<a href="/patient/view/' . $patient_with_min_drugs['patientID'] . '">';
                        $html .= $patient_with_min_drugs['lastName'] . ', ' . $patient_with_min_drugs['firstName'];
                        $html .= '</a>';
                        $html .= '</td>';
                        $html .= '<td>' . $patient_with_min_drugs['numOfDrugs'] . '</td>';
                        $html .= '<td>' . $patient_with_min_drugs['age'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

</div>