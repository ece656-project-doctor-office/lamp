<?php
    define("ADAY", (60*60*24));

    // Get the current date.
    $nowArray = getdate();

    // Set the month, either the post variable,
    // or the current month.
    if (isset($_POST['month'])) {
        $month = $_POST['month'];
    }
    else {
        $month = $nowArray['mon'];
    }

    // Set the month, either the post variable,
    // or the current month.
    if (isset($_POST['year'])) {
        $year = $_POST['year'];
    }
    else {
        $year = $nowArray['year'];
    }
 
    $start = mktime (12, 0, 0, $month, 1, $year);
    $firstDayArray = getdate($start);
 ?>
 <div class="doc-office__appointments--show">
    <form method="post" action="/appointments/show">
        <select name="month">
            <?php
                $months = [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ];

                for ($x=1; $x <= count($months); $x++) {
                    print "\t<option value=\"$x\"";
                    print ($x == $month)?" SELECTED":"";
                    print ">".$months[$x-1]."\n";
                }
            ?>
        </select>

        <select name="year">
            <?php
                for ($x = 2018; $x <= 2023; $x++) {
                    print "\t<option";
                    print ($x == $year)?" SELECTED":"";
                    print ">$x\n";
                }
            ?>
        </select>
        <input type="submit" value="Go!">
    </form>

    <?php
        $html = '';

        $days = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
        ];

        $html .= '<table class="doc-office__appointments--calendar">';
        $html .= '<caption>';
        $counter = 1;
        foreach ($months as $m) {
            if ($counter == $month) {
                $html .= $m;
                break;
            }
            $counter++;
        }
        
        $html .= ' ' . $year . '</caption>';
        
        $html .= '<tbody>';

        $html .= '<tr class="weekdays">';

        foreach ($days as $day) {
            $html .= '<th scope="col">' . $day . '</th>';
        }

        $html .= '</tr>';

        for ($count=0; $count < (6*7); $count++) {
            $dayArray = getdate($start);
            if (($count % 7) == 0) {
                if ($dayArray['mon'] != $month) {
                    break;
                }
                else {
                    $html .= "</tr><tr>";
                }
            }

            if ($count < $firstDayArray['wday'] || $dayArray['mon'] != $month) {
                $html .= '<td class="other-month"><br></td>';
            }
            else {

                if ($dayArray['weekday'] == 'Saturday' || $dayArray['weekday'] == 'Sunday') {
                    $html .= '<td class="other-month">';
                    $html .= '<span class="doc-office__day--link">' . $dayArray['mday'] . '</span>';
                    $html .= '</td>';
                }
                else {
                // If the month is less than 10 add the leading zero.
                // If greater than 10, just use the month.
                if ($month < 10) {
                    $get_month = '0' . $month;
                }
                else {
                    $get_month = $month;
                }

                // If the day is less than 10 add the leading zero.
                // If greater than 10, just use the day.
                if ($dayArray['mday'] < 10) {
                    $get_day = '0' . $dayArray['mday'];
                }
                else {
                    $get_day = $dayArray['mday'];
                }

                // Set the current date.
                $current_date = $year . '-' . $get_month . '-' . $get_day;

                $html .= '<td>';
                $html .= '<span class="doc-office__day--link"><a href="/appointments/edit/' . $current_date . '">' . $dayArray['mday'] . '</a></span>';

                $num_of_appointments = get_num_of_appointments_by_date($current_date);

                if ($num_of_appointments > 0) {
                    $class = 'available';
                }
                else {
                    $class = 'not-available';
                    $num_of_appointments = '0';
                }

                $html .= '<div class="doc-office__day--num-of-appointments">';
                $html .= '<span class="' . $class . '">' . $num_of_appointments . ' appointment(s) are available</span>';
                $html .= '</div>';

                $html .= '</td>';
            }
                $start += ADAY;
            }
        }
        $html .= "</tr></table>";

        echo $html;
    ?>
</div>