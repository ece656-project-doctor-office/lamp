<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous">
</script>
<script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
    integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
    crossorigin="anonymous">
</script>
<link rel="stylesheet" href="/css/jquery-ui.min.css">
<script type="text/javascript">
    $(function() {
        $("#drugInfo").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/autocomplete/choosedrug.php?term="+request.term,
                    dataType: "json",
                    type: "POST",
                    data: {
                        keyword: request.term
                    },
                    success: function(data){
                        response( $.map( data, function( item ) {
                            return {
                                    label: item.drugInfo,
                                    value: item.drugInfo,
                                    id: item.drugCode
                                }
                            
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $("#drugCode").val(ui.item.id);  // ui.item.value contains the id of the selected label
            }
        });
    });
</script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="/css/jquery.datepick.css" rel="stylesheet">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 500px; padding: 20px; }
</style>

<div class="doc-office__drugs--choose">

    <?php if (isset($local_error_message)) {?>
        <div class="error-message">
            <?php 
                echo $local_error_message;
                $local_error_message = NULL;
            ?>
        </div>
    <?php } ?>

    <h2>Search for Drug</h2>

    <div class="wrapper">

        <form action="/drugs/choose" method="post">

            <div class="form-group">

                <label>Drug</label>
                <input
                    type="text"
                    id="drugInfo"
                    name="drugInfo"
                    class="form-control <?php echo (!empty($errors['drugInfo'])) ? 'is-invalid' : ''; ?>"
                    value="<?php if (isset($fields['drugInfo'])) { echo $fields['drugInfo']; } ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['drugInfo']; ?></span>
                <input type="hidden" name="drugCode" id="drugCode" value="<?php if (isset($fields['drugCode'])) { echo $fields['drugCode']; } ?>">
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>

        </form>
    </div>
</div>