<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="/css/jquery.datepick.css" rel="stylesheet">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 500px; padding: 20px; }
</style>

<style>
    .doc-office__drug-info,
    .doc-office__drug-prescription {
        display: none;
    }

    input[id="requireDrug"]:checked ~ .doc-office__drug-info {
        display: block;
    }

    input[id="noRequireDrug"]:checked ~ .doc-office__drug-prescription {
        display: block;
    }
}
</style>

<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous">
</script>
<script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
    integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
    crossorigin="anonymous">
</script>
<link rel="stylesheet" href="/css/jquery-ui.min.css">
<script type="text/javascript">
    $(function() {
        $("#drugInfo").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/autocomplete/choosedrug.php?term="+request.term,
                    dataType: "json",
                    type: "POST",
                    data: {
                        keyword: request.term
                    },
                    success: function(data){
                        response( $.map( data, function( item ) {
                            return {
                                    label: item.drugInfo,
                                    value: item.drugInfo,
                                    id: item.drugCode
                                }
                            
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $("#drugCode").val(ui.item.id);  // ui.item.value contains the id of the selected label
            }
        });
    });
</script>

<div class="doc-office__pperscriptons--add">

    <?php if (isset($local_error_message)) {?>
        <div class="error-message">
            <?php 
                echo $local_error_message;
                $local_error_message = NULL;
            ?>
        </div>
    <?php } ?>

    <h2>Add Prescription for <?php echo $patient_info[0]['firstName'] . ' ' . $patient_info[0]['lastName']; ?>

    <div class="wrapper">

        <form action="/prescriptions/add/<?php echo $patientID; ?>" method="post">

            <div class="form-group">
                <label>Diagnosis</label>
                <select name="diagnosisID" class="form-control <?php echo (!empty($errors['diagnosisID'])) ? 'is-invalid' : ''; ?>">
                    <option value="">-- Select --</option>
                    <?php 
                        $html = '';
                        foreach ($diagnosis as $d) {
                            $html .= '<option value="' . $d['diagnosisID'] . '"';
                            if (isset($fields['diagnosisID']) && $d['diagnosisID'] == $fields['diagnosisID']) {
                                $html .= ' selected';
                            }
                            $html .= '>' . $d['diagnosisDescription'] . ' (' . date('M j, Y', strtotime($d['diagnosisDate'])) . ')';
                            $html .= '</option>';

                            echo $html;
                        }
                    ?>
                </select>
                <span class="invalid-feedback"><?php echo $errors['diagnosisID'] ?? ''; ?></span>
            </div>

            <div class="form-group">
                <label>Does this require a drug?</label>
                <br />
                <?php if (isset($errors['requireDrug'])) { ?>
                    <div class="error-message"><?php echo $errors['requireDrug']; ?></div>
                <?php } ?>
                <input type="radio" id="requireDrug" name="requireDrug" value="require" <?php if (isset($fields['requireDrug']) && $fields['requireDrug'] == 'require') { echo 'checked'; } ?>>
                <label>Yes</label>
                <input type="radio" id="noRequireDrug" name="requireDrug" value="norequire" <?php if (isset($fields['requireDrug']) && $fields['requireDrug'] == 'norequire') { echo 'checked'; } ?>>
                <label>No</label>

                <div class="doc-office__drug-info">
                    <label>Drug</label>
                    <input
                        type="text"
                        id="drugInfo"
                        name="drugInfo"
                        class="form-control <?php echo (!empty($errors['drugInfo'])) ? 'is-invalid' : ''; ?>"
                        value="<?php if (isset($fields['drugInfo'])) { echo $fields['drugInfo']; } ?>"
                    >
                    <span class="invalid-feedback"><?php echo $errors['drugInfo']; ?></span>
                    <input type="hidden" name="drugCode" id="drugCode" value="<?php if (isset($fields['drugCode'])) { echo $fields['drugCode']; } ?>">

                    <label>Dosage</label>
                    <input
                        type="text"
                        name="dosage"
                        class="form-control <?php echo (!empty($errors['dosage'])) ? 'is-invalid' : ''; ?>"
                        value="<?php if (isset($fields['dosage'])) { echo $fields['dosage']; } ?>"
                    >
                    <span class="invalid-feedback"><?php echo $errors['dosage'] ?? ''; ?></span>

                    <label>Instructions</label>
                    <textarea
                        name="instructions"
                        class="form-control"
                        rows="4"
                        cols="60"
                    ><?php echo $fields['instructions'] ?? ''; ?></textarea>
                </div>

                <div class="doc-office__drug-prescription">
                    <label>Prescription</label>
                    <textarea
                        name="prescription"
                        class="form-control <?php echo (!empty($errors['prescription'])) ? 'is-invalid' : ''; ?>"
                        rows="4"
                        cols="60"
                    ><?php echo $fields['prescription'] ?? ''; ?></textarea>
                    <span class="invalid-feedback"><?php echo $errors['prescription']; ?></span>
                </div>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>

        </form>
    </div>
</div>