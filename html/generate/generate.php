<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="/css/jquery.datepick.css" rel="stylesheet">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 500px; padding: 20px; }
</style>


<div class="doc-office__generate">

    <?php include_once 'success_error.php'; ?>

    <div class="wrapper">
        <h2>Generate Data</h2>

        <form action="/generate/generate" method="post">
            <div class="form-group">
                <select
                    name="gen_type"
                    class="form-control"
                >
                    <option value="">-- Select --</option>
                    <option value="doctor" <?php if (isset($gen_type) && $gen_type == 'doctor') { echo 'selected'; } ?>>Doctor</option>
                    <option value="patient" <?php if (isset($gen_type) && $gen_type == 'patient') { echo 'selected'; } ?>>Patient</option>
                    <option value="manager" <?php if (isset($gen_type) && $gen_type == 'manager') { echo 'selected'; } ?>>Manager</option>
                </select>
            </div>

            <div class="form-group">
                <label>Number to generate</label>
                <input
                    type="text"
                    name="numToGenerate"
                    class="form-control"
                >
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>

        </form>
    </div>
</div>