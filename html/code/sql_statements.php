<?php

/**
 * SQL to insert a patient.
 *
 * @param array $fields
 *   An array of fields to insert.
 *
 * @return string
 *   The sql to insert a patient.
 */
function sql_insert_patient(array $fields): string {

    // SQL to insert the patient.
    $insert_patient = '
        INSERT INTO
        Patients (
            patientID,
            insurancePolicyID,
            ohipID
        )
        VALUES (
            ' . $fields['userID'] . ',
            "' . $fields['insurancePolicyID'] . '",
            "' . $fields['ohipID'] . '"
        )
    ';

    return $insert_patient;
}

/**
 * SQL to insert a doctor.
 *
 * @param array $fields
 *   An array of fields to insert.
 *
 * @return string
 *   The sql to insert a patient.
 */
function sql_insert_doctor(array $fields): string {

    // SQL to insert the doctor.
    $insert_doctor = '
        INSERT INTO
        Doctor (
            doctorID,
            sin,
            designation
        )
        VALUES (
            ' . $fields['userID'] . ',
            "' . $fields['sin'] . '",
            "' . $fields['designation'] . '"
        )
    ';


    return $insert_doctor;
}

/**
 * SQL to insert a manager.
 *
 * @param array $fields
 *   An array of fields to insert.
 *
 * @return string
 *   The sql to insert a patient.
 */
function sql_insert_manager(array $fields): string {

    // SQL to insert the manager.
    $insert_manager = '
        INSERT INTO
        Manager (
            managerID,
            sin
        )
        VALUES (
            ' . $fields['userID'] . ',
            "' . $fields['sin'] . '"
        )
    ';


    return $insert_manager;
}

/**
 * SQL to update a user.
 *
 * @param array $fields
 *   An array of fields to insert.
 * @param int $userID
 *   The userID to update.
 *
 * @return string
 *   The sql to update a user.
 */
function sql_update_user(array $fields, int $userID): string {

    // SQL to update a user.
    $update_user = '
        UPDATE Users
        SET
            firstName = "' . $fields['firstName'] . '",
            lastName = "' . $fields['lastName'] . '",
            dateOfBirth = "' . $fields['dateOfBirth'] . '",
            phone = "' . $fields['phone'] . '",
            email = "' . $fields['email'] . '",
            gender = "' . $fields['gender'] . '",
            addressLineOne = "' . $fields['addressLineOne'] . '",
            addressLineTwo = "' . $fields['addressLineTwo'] . '",
            city = "' . $fields['city'] . '",
            province = "' . $fields['province'] . '",
            postalCode = "' . $fields['postalCode'] . '",
            status = ' . $fields['status'] . '
        WHERE
            userID = ' . $userID;

    return $update_user;
}

/**
 * SQL to update a patient.
 *
 * @param array $fields
 *   An array of fields to insert.
 * @param int $userID
 *   The user id.
 *
 * @return string
 *   The sql to update a patient.
 */
function sql_update_patient(array $fields, int $userID): string {

    // SQL to update the patient.
    $update_patient = '
        UPDATE Patients
        SET
            insurancePolicyID = "' . $fields['insurancePolicyID'] . '",
            ohipID = "' . $fields['ohipID'] . '"
        WHERE
            patientID = ' . $userID;

    return $update_patient;
}

/**
 * SQL to update a doctor.
 *
 * @param array $fields
 *   An array of fields to insert.
 * @param int $userID
 *   The user id.
 *
 * @return string
 *   The sql to update a doctor.
 */
function sql_update_doctor(array $fields, int $userID): string {

    // SQL to update the doctor.
    $update_doctor = '
        UPDATE Doctor
        SET
            sin = "' . $fields['sin'] . '",
            designation = "' . $fields['designation'] . '"
        WHERE
            doctorID = ' . $userID;

    return $update_doctor;
}

/**
 * SQL to update a manager.
 *
 * @param array $fields
 *   An array of fields to insert.
 * @param int $userID
 *   The user id.
 *
 * @return string
 *   The sql to update a manager.
 */
function sql_update_manager(array $fields, int $userID): string {

    // SQL to update the maanger.
    $update_maanger = '
        UPDATE Manager
        SET
            sin = "' . $fields['sin'] . '"
        WHERE
            managerID = ' . $userID;

    return $update_maanger;
}

/**
 * SQL to get the users by role and a search term.
 *
 * @param string $userRole
 *   The user role.
 * @param string $search_term
 *   The searh term.
 * @param string $search_column
 *   The column to search.
 * @param string $status_type
 *   The status type to look for.
 *
 * @return string
 *   The sql to get users.
 */
function sql_get_users_by_role_and_search_term(string $userRole, string $search_term, string $search_column, string $status_type = 'active') {

    // SQL to get the users.
    $get_users = '
        SELECT
            userID,
            firstName,
            lastName,
            dateOfBirth
        FROM
            Users
            INNER JOIN UserRoles USING (userRoleID)
        WHERE
            userRole = "' . $userRole . '" AND
            ' . $search_column . ' LIKE "%' . $search_term . '%"
    ';

    // Add the status if requried.
    switch ($status_type) {
        case 'active':
            $get_users .= ' AND status = 1';
            break;
        case 'inactive':
            $get_users .= ' AND status = 0';
            break;
    }

    return $get_users;
}

/**
 * SQL to get the provinces.
 *
 * @return string
 *   The sql to get provinces.
 */
function sql_get_provinces(): string {

    // SQL to get the provinces.
    $get_provinces = '
        SELECT
            fullName,
            abbr
        FROM
            Provinces
        ORDER BY
            abbr
    ';

    return $get_provinces;
}


/**
 * SQL to get the users by role and a search term.
 *
 * @param string|null $userRole
 *   The user role.
 * @param string $status_type
 *   The status type to look for.
 *
 * @return string
 *   The sql to get users.
 */
function sql_get_users_by_role(?string $userRole, string $status_type = 'active') {

    // SQL to get the number of users.
    $get_num_of_users = '
    SELECT
        count(userID) AS numOfUsers
    FROM
        Users
        INNER JOIN UserRoles USING (userRoleID)';

    if ($userRole) {
        $get_num_of_users .= '
            WHERE
                userRole = "' . $userRole . '"
        ';
    }
    else {
        $get_num_of_users .= '
            WHERE
                userName != "admin"
        ';
    }

    // Add the status if requried.
    switch ($status_type) {
    case 'active':
        $get_num_of_users .= ' AND status = 1';
        break;
    case 'inactive':
        $get_num_of_users .= ' AND status = 0';
        break;
    }

    return $get_num_of_users;
}

/**
 * SQL to get user by username.
 *
 * @param string $username
 *   The username.
 *
 * @return string
 *   SQL to get users by username.
 */
function sql_get_users_by_username(string $username): string {

    // SQL to get users by username.
    $get_users = '
        SELECT
            userID
        FROM
            Users
        WHERE
            username LIKE "' . $username . '%"
    ';

    return $get_users;
}

/**
 * SQL to insert a user.
 *
 * @param array $fields
 *   The array of fields.
 * @param int $user_role_id
 *   The user role id.
 * @param string $username
 *   The username
 *
 * @return string
 *   The SQL to insert a user.
 */
function sql_insert_user(array $fields, int $user_role_id, $username): string {

    // SQL to insert a new user.
    $insert_user = '
    INSERT INTO
    Users (
        username,
        password,
        userRoleID,
        firstName,
        lastName,
        dateOfBirth,
        phone,
        email,
        gender,
        addressLineOne,
        addressLineTwo,
        city,
        province,
        postalCode,
        status
    )
    VALUES (
        "'. $username . '",
        "' . $username . '",
        ' . $user_role_id . ',
        "' . $fields['firstName'] . '",
        "' . $fields['lastName'] . '",
        "' . $fields['dateOfBirth'] . '",
        "' . $fields['phone'] . '",
        "' . $fields['email'] . '",
        "' . $fields['gender'] . '",
        "' . $fields['addressLineOne'] . '",
        "' . $fields['addressLineTwo'] . '",
        "' . $fields['city'] . '",
        "' . $fields['province'] . '",
        "' . $fields['postalCode'] . '",
        1
    )';

    return $insert_user;
}

/**
 * SQL to get user role id from a user role.
 *
 * @param string $userRole
 *   The user role.
 *
 * @return string
 *   SQL to get a user from user role.
 */
function sql_get_user_role_id_from_user_role(string $userRole) {

    // SQL to get the role id.
    $get_role_id = '
        SELECT
            userRoleID
        FROM
            UserRoles
        WHERE
            userRole = "' . $userRole . '"
    ';

    return $get_role_id;
}

/**
 * SQL to get user id from the username.
 *
 * @param string $username
 *   The username
 *
 * @return string
 *   The SQL to get user id from the username.
 */
function sql_get_user_from_username(string $username): string {

    // SQL to get the user id of newly created user.
    $get_user_id = '
        SELECT
            userID
        FROM
            Users
        WHERE
            username = "' . $username . '"
    ';

    return $get_user_id;
}

/**
 * SQL to get all user fields.
 *
 * @param int $userID
 *   The user id.
 * @param string $table
 *   The extra table (i.e. Patients, Doctor, etc ...).
 * @param string $type
 *   The type of user (i.e. patient, doctor, etc ...).
 *
 * @return string
 *   The sql to get all user fields.
 */
function sql_get_user_fields(int $userID, string $table, string $type): string {

    // SQL to get all the user fueklds
    $get_user = '
        SELECT *
        FROM
            Users
            INNER JOIN ' . $table . ' ON ' . $type . 'ID = userID
        WHERE
            userID = ' . $userID;

    return $get_user;
}

/**
 * SQL to get office name.
 *
 * @return string
 *   The sql to get office name.
 */
function sql_get_office_name(): string {

    // SQL to get office name.
    $get_office_name = 'SELECT name FROM OfficeInfo';

    return $get_office_name;
}

/**
 * SQL to get info to go in the footer.
 *
 * @return string
 *   The sql of footer info.
 */
function sql_get_footer_info(): string {

    $get_footer_info = '
        SELECT
            address,
            city,
            province,
            postalCode,
            phone,
            fax,
            email
        FROM
            OfficeInfo
    ';

    return $get_footer_info;
}

/**
 * SQL to get user from username and password.
 *
 * @param string $username
 *   The username.
 * @param string $password
 *   The password.
 *
 * @return string
 *   The sql to get user from username and password.
 */
function sql_get_user_from_username_password(string $username, string $password): string {

    // Query to get the user.
    $get_user = '
        SELECT 
            userID,
            userRole,
            status
        FROM
            Users
            INNER JOIN UserRoles USING (userRoleID)
        WHERE
            username = "' . $username . '" AND
            `password` = "' . $password . '"';

    return $get_user;
}

/**
 * SQL to get office info.
 *
 * @return string
 *   The sql to get the office info.
 */
function sql_get_office_info(): string {

    // SQL to get the office info.
    $get_office_info = 'SELECT * from OfficeInfo';

    return $get_office_info;
}

/**
 * SQL to get welcome message.
 *
 * @return string
 *   The sql to get the welcome message.
 */
function sql_get_welcome_message(): string {

    // SQL to get welcome message.
    $get_welcome_message = '
        SELECT
            welcomeMessage
        FROM
            OfficeInfo
    ';

    return $get_welcome_message;
}

/**
 * SQL to update the offce info.
 *
 * @param array $fields
 *   The array of fields to update.
 *
 * @return string
 *   The sql to update the office info.
 */
function sql_update_office_info(array $fields): string {

    // SQL to update the office info.
    $update_office_info = '
        UPDATE OfficeInfo
        SET
            name = "' . $fields['name'] . '",
            address = "' . $fields['address'] . '",
            city = "' . $fields['city'] . '",
            province = "' . $fields['province'] . '",
            postalCode = "' . $fields['postalCode'] . '",
            phone = "' . $fields['phone'] . '",
            fax = "' . $fields['fax'] . '",
            email = "' . $fields['email'] . '",
            welcomeMessage = "' . $fields['welcomeMessage'] . '"
    ';

    return $update_office_info;
}

/**
 * SQL to get number of appointments by day.
 *
 * @param string $date
 *   The date to get number of appointments.
 *
 * @return string
 *   The sql to get the number of appointments by day.
 */
function sql_get_num_of_appointments_by_day(string $date): string {

    // SQL to get number of appointments.
    $num_of_appts = '
        SELECT
            count(appointmentID) as numOfAppointments
        FROM
            Appointment
        WHERE
            appointmentDate = "' . $date . '"
    ';

    return $num_of_appts;
}

/**
 * SQL to get number of appointments by day and start time.
 *
 * @param string $date
 *   The date to get number of appointments.
 * @param string $start_time
 *   The start time of the appointment
 *
 * @return string
 *   The sql to get the number of appointments by day.
 */
function sql_get_num_of_appointments_by_day_and_start_time(string $date, string $start_time): string {

    // SQL to get number of appointments.
    $num_of_appts = '
        SELECT
            count(appointmentID) as numOfAppointments
        FROM
            Appointment
        WHERE
            appointmentDate = "' . $date . '" AND
            appointmentStartTime = "' . $start_time . '"
    ';

    return $num_of_appts;
}

/**
 * SQL to get number of doctors.
 *
 * @return string
 *   The sql to get number of docs.
 */
function sql_get_num_of_docs(): string {

    $num_of_docs = '
        SELECT
            COUNT(userID) AS numOfDocs
        FROM
            Users
            INNER JOIN UserRoles USING (userRoleID)
        WHERE
            userRole = "doctor" AND
            status = 1
    ';

    return $num_of_docs;
}

/**
 * SQL to get num of doctors off by date.
 *
 * @param string $date
 *   The date to check.
 *
 * @return string
 *   The sql to get the number of doctors off by date.
 */
function sql_get_num_of_docs_off_by_date(string $date): string {

    // SQL to get number of doctors off by date.
    $num_of_docs = '
        SELECT
	        count(userID) as numOfDocs
        FROM
	        Users
            INNER JOIN UserRoles USING (userRoleID)
            INNER JOIN DoctorSchedule ON Users.userID = DoctorSchedule.doctorID
        WHERE
	        userRole = "doctor" AND
            status = 1 AND
            scheduleDate = "' . $date . '"
    ';

    return $num_of_docs;
}

/**
 * SQL to get a users first name and last name.
 *
 * @param string $userID
 *   The user id.
 *
 * @return string
 *   The sql of a users first name and last name given user id.
 */
function sql_get_user_first_last_name(int $userID): string {

    // SQL to get a users first and last name.
    $get_user_first_last_name = '
        SELECT
            firstName,
            lastName
        FROM
            Users
        WHERE
            userID = ' . $userID;

    return $get_user_first_last_name;
}

/**
 * SQL to get a users first name, last name and date of birth.
 *
 * @param string $userID
 *   The user id.
 *
 * @return string
 *   The sql of a users first name, last name and date of birth of given user id.
 */
function sql_get_user_first_last_name_dob(int $userID): string {

    // SQL to get users first name, last name and date 
    // of birth of given user id.
    $get_user_first_last_name_dob = '
        SELECT
            firstName,
            lastName,
            dateOfBirth
        FROM
            Users
        WHERE
            userID = ' . $userID;

    return $get_user_first_last_name_dob;
}

/**
 * SQL to get a doctors schedule.
 *
 * @param string $date
 *   Todays date.
 * @param int $doctorID
 *   The id of the doctor.
 *
 * @return string
 *   The sql to get a doctors schedule.
 */
function sql_get_doc_schedule(string $date, int $doctorID): string {

    // SQL to get a docs schedule.
    $get_doc_schedule = '
        SELECT
            scheduleDate
        FROM
            DoctorSchedule
        WHERE
            scheduleDate >= "' . $date . '" AND
            doctorID = ' . $doctorID . '
        ORDER BY
            scheduleDate DESC
    ';

    return $get_doc_schedule;
}

/**
 * SQL to delete the doctor scehdule.
 *
 * @param int $doctorID
 *   The id of the doctor.
 * @param string $date
 *   The date to delete.
 *
 * @return string
 *   The sql to delete a doctor scehdule.
 */
function sql_delete_doctor_schedule(int $doctorID, string $date): string {

    // SQL to delete a doctor schedule.
    $delete_doctor_schedule = '
        DELETE FROM
            DoctorSchedule
        WHERE
            scheduleDate = "' . $date . '" AND
            doctorID = ' . $doctorID;

    return $delete_doctor_schedule;
}

/**
 * SQL to add doctor scehdule.
 *
 * @param int $doctorID
 *   The id of the doctor.
 * @param string $date
 *   The date to add.
 *
 * @return string
 *   The sql to add a doctor scehdule.
 */
function sql_add_doctor_schedule(int $doctorID, string $date): string {

    // SQL to add a doctor schedule.
    $add_doctor_schedule = '
        INSERT INTO
            DoctorSchedule
        VALUES (
            ' . $doctorID . ',
            "' . $date . '"
        )
    ';

    return $add_doctor_schedule;
}

/**
 * SQL to get doctors avaiable by date.
 *
 * @return string
 *   The sql to get active doctors.
 */
function sql_get_doctors_available_by_date(string $date): string {

    // SQL to get active doctor info.
    $get_doctors = '
        SELECT
            userID AS doctorID,
            firstName,
            lastName
        FROM
            Users
            INNER JOIN UserRoles USING (userRoleID)
        WHERE
            userRole = "doctor" AND
            status = 1 AND
            userID NOT IN (
                SELECT
                    doctorID
                FROM
                    DoctorSchedule
                WHERE
                    scheduleDate = "' . $date . '"
            )
        ORDER BY lastName ASC
    ';

    return $get_doctors;
}

/**
 * SQL to get an appointment.
 *
 * @param int $doctorID
 *   The id of the doctor.
 * @param string $date
 *   The date of the appointment.
 * @param string $start_time
 *   The start time of the appointment.
 * @param string $end_time
 *   The end time of the appointment.
 *
 * @return string
 *   The sql to get the appointment.
 */
function sql_get_appointment(int $doctorID, string $date, string $start_time, string $end_time): string {

    // SQL to get the appointment.
    $get_appointment = '
        SELECT
            appointmentID,
            firstName,
            lastName
        FROM
            Appointment
            INNER JOIN Users ON patientID = userID
        WHERE
            appointmentDate = "' . $date . '" AND
            appointmentStartTime = "' . $start_time . '" AND
            appointmentEndTime = "' . $end_time . '" AND
            doctorID = ' . $doctorID;

    return $get_appointment;
}

/**
 * SQL to insert an appointment.
 *
 * @param int $patientID
 *   The id of the patient.
 * @param int $doctorID
 *   The id of the doctor.
 * @param string $date
 *   The date of the appointment.
 * @param string $start_time
 *   The start time of the appointment.
 * @param string $end_time
 *   The end time of the appointment.
 *
 * @return string
 *   The sql to insert an appointment.
 */
function sql_insert_appointment(int $patientID, int $doctorID, string $date, string $start_time, string $end_time) {

    // SQL to insert and appointment.
    $insert_appointment = '
        INSERT INTO Appointment 
            (
                doctorID,
                patientID,
                appointmentDate,
                appointmentStartTime,
                appointmentEndTime
            )
            VALUES (
                ' . $doctorID . ',
                ' . $patientID . ',
                "' . $date . '",
                "' . $start_time . '",
                "' . $end_time . '"
            )
    ';

    return $insert_appointment;
}

/**
 * SQL to delete an appointment.
 *
 * @param int $appointmentID
 *   The id of the appointment.
 *
 * @return string
 *   The sql to delete and appointment.
 */
function sql_delete_appointment(int $appointmentID): string {

    // SQL to delete an appointment.
    $delete_appointment = '
        DELETE FROM
            Appointment
        WHERE
            appointmentID = ' . $appointmentID;

    return $delete_appointment;
}

/**
 * SQL to get all doctor appointments by day.
 *
 * @param string $date
 *   The date to get the appointments.
 * @param int $doctorID
 *   The id of the doctor.
 *
 * @return string
 *   The SQL to get doctor appointments by day.
 */
function sql_get_doctor_appointments_by_day(string $date, int $doctorID): string {

    // SQL to get doctor appointments by day.
    $get_doctor_appointments = '
        SELECT
            userID AS patientID,
            lastName,
            firstName,
            appointmentStartTime,
            appointmentEndTime
        FROM
            Appointment
            INNER JOIN Users ON userID = patientID
        WHERE
            appointmentDate = "' . $date . '" AND
            doctorID = ' . $doctorID . '
        ORDER BY
            appointmentStartTime
        ';

    return $get_doctor_appointments;
}

/**
 * SQL to get all doctor appointments by day.
 *
 * @param string $date
 *   The date to get the appointments.
 *
 * @return string
 *   The SQL to get doctor appointments by day.
 */
function sql_get_doctor_appointments_by_day_and_start_time(string $date): string {

    // SQL to get doctor ids.
    $get_doctor_ids = '
        SELECT
            userID AS doctorID
        FROM
            Users
            INNER JOIN UserRoles USING (userRoleID)
        WHERE
            userID NOT IN (
                SELECT
                    doctorID
                FROM
                    DoctorSchedule
                WHERE
                    scheduleDate = "' . $date . '"
            )
            AND
            userRole = "doctor"
        ';

    return $get_doctor_ids;
}

/**
 * SQL to get count of appointments by start time and doctor ID.
 *
 * @param int $doctorID
 *   The id of the doctor.
 * @param string $start_tme
 *   The date to get the appointments.
 *
 * @return string
 *   The SQL to get count of appointments by start time and doctor ID.
 */
function sql_get_appointment_count(int $doctorID, string $start_time): string {

    // SQL to get count of appointments by start time and doctor ID.
    $get_appointment_count = '
        SELECT
            COUNT(appointmentID) as numOfAppointments
        FROM
            Appointment
        WHERE
            doctorID = ' . $doctorID . ' AND
            appointmentStartTime = "' . $start_time . '"
        ';

    return $get_appointment_count;
}

/**
 * SQL to get patient diagnosis.
 *
 * @param int $patientID
 *   The id of the patient.
 *
 * @return string
 *   The sql to get a patient diagnosis.
 */
function sql_get_patient_diagnosis(int $patientID): string {

    // SQL to get a patient diagnosis.
    $get_patient_diagnosis = '
        SELECT
            diagnosisID,
            lastName as doctorLastName,
            firstName as doctorFirstName,
            diseaseName,
            diagnosisDescription,
            diagnosisDate
        FROM
            Diagnosis
            INNER JOIN Users ON doctorID = userID
            INNER JOIN Disease USING (diseaseID)
        WHERE
            patientID = '. $patientID . '
        ORDER BY
            diagnosisDate DESC
    ';

    return $get_patient_diagnosis;
}

/**
 * SQL to get disease by search term.
 *
 * @param string $searchTerm
 *   The thing to search for.
 *
 * @return string
 *   The sql to get diseases by a search term.
 */
function sql_get_diseases_by_search_term(string $searchTerm): string {

    $get_diseases = '
        SELECT
            diseaseName,
            diseaseID
        FROM
            Disease
        WHERE
            diseaseName LIKE "%' . $searchTerm . '%"
        ORDER BY
            diseaseName
        LIMIT 25
    ';

    return $get_diseases;
}

/**
 * SQL to insert a diagnosis.
 *
 * @param int $doctorID
 *   The id of the doctor.
 * @param int $patientID
 *   The id of the patient.
 * @param string $diseaseDescription
 *   The descrption of the diagnosis.
 * @param int $diseaseID
 *   The id of the disease.
 * @param string $date
 *   The date of the diagnosis.
 *
 * @return string
 *   The sql to insert the diagnosis
 */
function sql_insert_diagnosis(int $doctorID, int $patientID, string $diseaseDescription, int $diseaseID, string $date): string {

    // SQL to insert the diagnosis.
    $insert_diagnosis = '
        INSERT INTO Diagnosis (
            doctorID,
            patientID,
            diagnosisDescription,
            diagnosisDate,
            diseaseID
        )
        VALUES (
            ' . $doctorID . ',
            ' . $patientID . ',
            "' . $diseaseDescription . '",
            "' . $date . '",
            ' . $diseaseID . '
        )
    ';

    return $insert_diagnosis;
}

/**
 * SQL to get prescription drugs by diagnosis.
 *
 * @param int $diagnosisID
 *   The id of the diagnosis.
 *
 * @return string
 *   The sql to get prescription drugs by diagnosis.
 */
function sql_get_prescription_drugs_by_diagnosis(int $diagnosisID): string {

    // SQL to get prescription drugs by diagnosis.
    $drugs = '
        SELECT
            prescriptionID
        FROM
            PrescriptionDrugs
            INNER JOIN Prescription USING (prescriptionID)
        WHERE
            diagnosisID = ' . $diagnosisID;

    return $drugs;
}

/**
 * SQL to delete a prescription by a diagnosis.
 *
 * @param int $diagnosisID
 *   The id of the diagnosis.
 *
 * @return string
 *   The sql to delete a diagnosis.
 */
function sql_delete_prescrition_by_diagnosis(int $diagnosisID): string {

    // SQL to delete a prescription by a diagnosis..
    $delete_prescription = '
        DELETE FROM
            Prescription
        WHERE
            diagnosisID = ' . $diagnosisID;

    return $delete_prescription;
}

/**
 * SQL to delete a diagnosis.
 *
 * @param int $diagnosisID
 *   The id of the diagnosis.
 *
 * @return string
 *   The sql to delete a diagnosis.
 */
function sql_delete_diagnosis(int $diagnosisID): string {

    // SQL to delete a diagnosis.
    $delete_diagnosis = '
        DELETE FROM
            Diagnosis
        WHERE
            diagnosisID = ' . $diagnosisID;

    return $delete_diagnosis;
}

/**
 * SQL get all patient perscriptions.
 *
 * @param int $patientID
 *   The id of the patient.
 *
 * @return string
 *   The sql to get all the patient perscriptions.
 */
function sql_get_all_perscriptions(int $patientID): string {

    // SQL to get all patient prescriptions.
    $get_perscriptions = '
        SELECT
            prescriptionID,
            patientID,
            prescription,
            drugCode,
            brandName AS drugName,
            dosage,
            instructions,
            lastName AS doctorLastName,
            firstName AS doctorFirstName,
            diagnosisDate AS prescribedDate
        FROM
            Prescription
            LEFT OUTER JOIN Diagnosis USING (diagnosisID)
            LEFT OUTER JOIN PrescriptionDrugs using (prescriptionID)
            LEFT OUTER JOIN Drugs Using (drugCode)
            INNER JOIN Users ON doctorID = userID
        WHERE
            patientID = ' . $patientID . '
        ORDER BY
            prescribedDate DESC
    ';

    return $get_perscriptions;
}

/**
 * SQL to get a patient from a diagnosis.
 *
 * @param int $diagnosisID
 *   The id of the diagnosis.
 *
 * @return string
 *   The sql to get a patient from a diagnosis.
 */
function sql_get_user_from_diagnosis(int $diagnosisID): string {

    // SQL to get a patient from a diagnosis.
    $get_user = '
        SELECT
            userID as patientID,
            lastName,
            firstName
        FROM
            Users
            INNER JOIN Diagnosis ON patientID = userID
        WHERE
            diagnosisID = ' . $diagnosisID;

    return $get_user;
}

/**
 * SQL to get info about a diagnosis.
 *
 * @param int $diagnosisID
 *   The id of the diagnosis.
 *
 * @return string
 *   The sql to get info about a diagnosis.
 */
function sql_get_diagnosis(int $diagnosisID): string {

    // SQL to get info about a diagnosis.
    $diagnosis = '
        SELECT
            diagnosisDescription,
            diseaseName,
            diseaseID
        FROM
            Diagnosis
            INNER JOIN Disease USING (diseaseID)
        WHERE
            diagnosisID = ' . $diagnosisID;

    return $diagnosis;
}

/**
 * SQL to get all patients diagnosis.
 *
 * @param int $patientID
 *   The id of the patient.
 *
 * @return string
 *   The sql to get all the patients diagnosis.
 */
function sql_get_all_diagnosis($patientID): string {

    // SQL to get all the patients diagnosis.
    $get_diagnosis = '
        SELECT
            diagnosisID,
            diagnosisDescription,
            diagnosisDate
        FROM
            Diagnosis
        WHERE
            patientID = ' . $patientID .'
        ORDER BY
            diagnosisDate DESC
    ';

    return $get_diagnosis;
}

/**
 * SQL to update a diagnosis.
 *
 * @param int $diagnosisID
 *   The id of the diagnosis.
 * @param array $fields
 *   The array of fields for the diagnosis.
 *
 * @return string
 *   The sql to update a diagnosis.
 */
function sql_update_diagnosis(int $diagnosisID, array $fields): string {

    // SQL to update a diagnosis.
    $update_diagnosis = '
        UPDATE
            Diagnosis
        SET
            doctorID = ' . $fields['doctorID'] . ',
            diagnosisDescription = "' . $fields['diseaseDescription'] . '",
            diagnosisDate = "' . $fields['diagnosisDate'] . '",
            diseaseID = ' . $fields['diseaseID'] . '
        WHERE
            diagnosisID = ' . $diagnosisID;

    return $update_diagnosis;
}

/**
 * SQL to get drugs using a seach term.
 *
 * @param string $searchTerm
 *   The thing to search for.
 *
 * @return string
 *   The sql to get drugs using a search term.
 */
function sql_get_drugs_by_search_term(string $searchTerm): string {

    // SQL to get drugs using a search term.
    $get_drugs = '
        SELECT
            drugCode,
            brandName AS drugInfo
        FROM
            Drugs
        WHERE
            brandName LIKE "%' . $searchTerm . '%"
        LIMIT 50
    ';

    return $get_drugs;
}

/**
 * SQL to insert a prescription.
 *
 * @param array $fields
 *   An array of fields to insert in the prescription.
 *
 * @return string
 *   The sql to insert a prescription.
 */
function sql_insert_prescription(array $fields): string {

    // SQL to insert a prescription.
    $insert_prescription = '
        INSERT INTO
        Prescription (
            diagnosisID,
            prescription
        )
        VALUES (
            ' . $fields['diagnosisID'] . ',
            "' . $fields['prescription'] . '"
        )
    ';

    return $insert_prescription;
}

/**
 * SQL to insert a prescription drug.
 *
 * @param array $fields
 *   An array of fields to insert in the prescription drug.
 *
 * @return string
 *   The sql to insert a prescription drug.
 */
function sql_insert_prescription_drug($fields) {

    // SQL to insert a prescription drug.
    $insert_prescription_drug = '
        INSERT INTO
        PrescriptionDrugs (
            prescriptionID,
            drugCode,
            dosage,
            instructions
        )
        VALUES (
            ' . $fields['prescriptionID'] . ',
            ' . $fields['drugCode'] . ',
            "' . $fields['dosage'] . '",
            "' . $fields['instructions'] . '"
        )
    ';

    return $insert_prescription_drug;
}

/**
 * SQL to get a prescription.
 *
 * @param int $prescriptionID
 *   The id of the prescription.
 *
 * @return string
 *   The sql to get a prescription.
 */
function sql_get_prescription(int $prescriptionID) {

    // SQL to get a prescription.
    $get_prescription = '
        SELECT
            prescriptionID,
            patientID,
            prescription,
            brandName AS drugName,
            dosage,
            instructions,
            lastName AS doctorLastName,
            firstName AS doctorFirstName,
            diagnosisDate AS prescribedDate
        FROM
            Prescription
            LEFT OUTER JOIN Diagnosis USING (diagnosisID)
            LEFT OUTER JOIN PrescriptionDrugs using (prescriptionID)
            LEFT OUTER JOIN Drugs Using (drugCode)
            INNER JOIN Users ON doctorID = userID
        WHERE
            prescriptionID = ' . $prescriptionID . '
        ORDER BY
            prescribedDate DESC
    ';

    return $get_prescription;
}

/**
 * SQL to delete a prescription.
 *
 * @param int $prescriptionID
 *   The id of the prescription.
 *
 * @return string
 *   The sql to delete a prescription.
 */
function sql_delete_prescription(int $prescriptionID): string {

    // SQL to delete a prescription.
    $delete_prescription = '
        DELETE FROM
            Prescription
        WHERE
            prescriptionID = ' . $prescriptionID;

    return $delete_prescription;
}

/**
 * SQL to get the number of prescription drugs.
 *
 * @param int $prescriptionID
 *   The id of the prescription.
 *
 * @return string
 *   The sql to get the number of prescription drug.
 */
function sql_get_num_prescription_drug(int $prescriptionID): string {

    // SQL to get the number of prescription drug.
    $num_prescription_drugs = '
        SELECT
            count(prescriptionID) as numDrugs
        FROM
            PrescriptionDrugs
        WHERE
            prescriptionID = ' . $prescriptionID;

    return $num_prescription_drugs;
}

/**
 * SQL to delete prescription drug.
 *
 * @param int $prescriptionID
 *   The id of the prescription.
 *
 * @return string
 *   The sql to delete prescription drug.
 */
function sql_delete_prescription_drug($prescriptionID) {

    // SQL to delete prescription drug.
    $delete_prescription_drug = '
        DELETE FROM
            PrescriptionDrugs
        WHERE
            prescriptionID = ' . $prescriptionID;

    return $delete_prescription_drug;
}

/**
 * SQL to get a drug.
 *
 * @param int $drugCode
 *   The drug code.
 *
 * The sql to get a drug.
 */
function sql_get_drug(int $drugCode): string {

    // The sql to get a drug.
    $get_drug = '
        SELECT
            productCategorization,
            class,
            drugIdentificationNumber as DIN,
            brandName as drugName,
            descriptor,
            pediatricFlag,
            statusFlag,
            status,
            historyDate
        FROM
            Drugs
            INNER JOIN DrugStatus USING (drugCode)
        WHERE
            drugCode = ' . $drugCode;
    
    return $get_drug;
}

/**
 * SQL to get a drug company.
 *
 * @param int $drugCode
 *   The drug code.
 *
 * The sql tto get a drug company.
 */
function sql_get_drug_company($drugCode) {

    // SQL to get a drug company.
    $drug_company = '
        SELECT
            companyName,
            suiteNumber,
            streetName,
            cityName,
            province,
            country,
            postalCode
        FROM
            DrugSeller
            INNER JOIN DrugCompanies USING (companyCode)
        WHERE
            drugCode = ' . $drugCode;

    return $drug_company;
}

/**
 * SQL to get info about the user.
 *
 * @param int $userID
 *   The id of the user.
 *
 * @return string
 *   The sql to get info about a user.
 */
function sql_get_user_info(int $userID) {

    // SQL to get info about a user.
    $get_user_info = '
        SELECT
            firstName,
            lastName,
            dateOfBirth,
            phone,
            email,
            gender,
            addressLineOne,
            city,
            province,
            postalCode
        FROM
            Users
        WHERE
            userID = ' . $userID;

    return $get_user_info;
}

/**
 * SQL to get upcoming patient appointments.
 *
 * @param int $patientID
 *   The id of the patient.
 * @param string $date
 *   The date to check past.
 *
 * @return string
 *   The sql to get upcoming patient appointments.
 */
function sql_get_patient_upcoming_appointments(int $patientID, string $date): string {

    // SQL to get upcoming patient appointments.
    $upcoming_patient_appointments = '
        SELECT
            appointmentDate,
            appointmentStartTime,
            appointmentEndTime
        FROM
            Appointment
        WHERE
            patientID = ' . $patientID . ' AND
            appointmentDate >= "' . $date . '"
        ORDER BY
            appointmentDate ASC
        LIMIT 10
    ';

    return $upcoming_patient_appointments;
}

/**
 * SQL to get number of users by an attribute.
 *
 * @param string $userRole
 *   The user role.
 * @param string|null $attribute
 *   The attribute to use.
 *
 * @return $string
 *   The sql to get users by an attribute. 
 */
function sql_get_users_by_attribute(?string $userRole, string $attribute): string {

    // SQL to get users by an attribute.
    $users_by_attribute = '
        SELECT
            ' . $attribute . ',
            count(userID) AS numOfUsers
        FROM
            Users
            INNER JOIN UserRoles USING (userRoleID)';

    if ($userRole) {
    
        $users_by_attribute .= '
            WHERE
                userRole = "' . $userRole . '"
        ';
    }
    else {
        $users_by_attribute .= '
            WHERE
                userRole <> "admin"
        ';
    }

    $users_by_attribute .= '
        GROUP BY
            ' . $attribute . '
        ORDER BY
            ' . $attribute . '
    ';

    return $users_by_attribute;
}

/**
 * SQL to get users age ranges.
 *
 * @param string $userRole
 *   The user role.
 *
 * @return string
 *   The sql to get users age ranges.
 */
function sql_get_users_age_ranges(string $userRole): string {

    // SQL to get users age ranges.
    $age_ranges = '
        SELECT
            SUM(IF(age < 1,1,0)) AS "Under 1",
            SUM(IF(age BETWEEN 1 and 4,1,0)) AS "1 - 4",
            SUM(IF(age BETWEEN 5 and 9,1,0)) AS "5 - 9",
            SUM(IF(age BETWEEN 10 and 14,1,0)) AS "10 - 14",
            SUM(IF(age BETWEEN 15 and 19,1,0)) AS "15 - 19",
            SUM(IF(age BETWEEN 20 and 29,1,0)) AS "20 - 29",
            SUM(IF(age BETWEEN 30 and 39,1,0)) AS "30 - 39",
            SUM(IF(age BETWEEN 40 and 49,1,0)) AS "40 - 49",
            SUM(IF(age BETWEEN 50 and 59,1,0)) AS "50 - 59",
            SUM(IF(age BETWEEN 60 and 69,1,0)) AS "60 - 69",
            SUM(IF(age BETWEEN 70 and 79,1,0)) AS "70 - 79",
            SUM(IF(age >= 80, 1, 0)) AS "Over 80",
            SUM(IF(age >= 0, 1, 0)) AS "Total"
        FROM (
            SELECT 
                TIMESTAMPDIFF(YEAR, dateOfBirth, CURDATE()) AS age 
            FROM
                Users
                INNER JOIN UserRoles USING (userRoleID)
            WHERE
                userRole = "' . $userRole . '"
        ) AS derived
    ';

    return $age_ranges;
}

/**
 * Function to get the average number of prescriptions.
 *
 * @return string
 *   The sql to get the average number of prescriptions.
 */
function sql_get_avg_num_prescriptions(): string {

    // SQL to get the average number of prescriptions.
    $avg_num_prescriptions = '
        WITH NumOfPrescriptions AS (
            SELECT
                count(prescriptionID) AS numOfPrescriptions
            FROM
                Diagnosis
                INNER JOIN Prescription USING (diagnosisID)
            GROUP BY
                patientID
        )
        SELECT
            avg(numOfPrescriptions) As avgNumOfPrescriptions
        FROM
            NumOfPrescriptions
    ';

    return $avg_num_prescriptions;
}

/**
 * Function to get the patient with max number of prescriptions.
 *
 * @param string $type
 *   The type to get, either max or min.
 *
 * @return string
 *   The sql to get the patient with max number of prescriptions.
 */
function sql_get_patients_with_max_or_min_prescriptions(string $type): string {

    // SQL to get the user with max number of prescriptions.
    $num_prescriptions = '
        WITH NumOfPrescriptions AS (
            SELECT
                patientID,
                firstName,
                lastName,
                TIMESTAMPDIFF(YEAR, dateOfBirth, CURDATE()) AS age,
                count(prescriptionID) AS numOfPrescriptions
            FROM
                Diagnosis
                INNER JOIN Prescription USING (diagnosisID)
                INNER JOIN Users ON patientID = userID
            GROUP BY
                patientID,
                firstName,
                lastName,
                age
        )
        SELECT
            patientID,
            firstName,
            lastName,
            age,
            numOfPrescriptions
        FROM
            NumOfPrescriptions
        WHERE
            numOfPrescriptions = (
                SELECT';

    if ($type == 'max') {
        $num_prescriptions .= '
            max(numOfPrescriptions)
        ';
    }
    else {
        $num_prescriptions .= '
            min(numOfPrescriptions)
        ';
    }

    $num_prescriptions .= '
        FROM
            NumOfPrescriptions
    )
    ORDER BY
        lastName ASC';

    return $num_prescriptions;
}

/**
 * SQL to get number of prescriptions.
 *
 * @return string
 *   The sql to get number of prescriptions.
 */
function sql_get_num_of_prescriptions(): string {

    // SQL to get number of prescriptions.
    $num_prescriptions = '
        SELECT
            count(prescriptionID) as numOfPrescriptions
        FROM
            Prescription
    ';

    return $num_prescriptions;
}

/**
 * SQL to get the number of drugs.
 *
 * @return string
 *   The sql to get the number of drugs.
 */
function sql_get_num_drugs(): string {

    // SQL to get the number of drugs.
    $num_drugs = '
        SELECT
            count(drugCode) As numOfDrugs
        FROM
            Drugs
    ';

    return $num_drugs;
}

/**
 * SQL to get number of prescription drugs.
 *
 * @return string
 *   The sql to get number of prescription drugs.
 */
function sql_get_num_prescription_drugs(): string {

    // SQL to get number of prescription drugs.
    $num_prescription_drugs = '
        SELECT
            COUNT(DISTINCT drugCode) AS numOfDrugs
        FROM
            PrescriptionDrugs
    ';

    return $num_prescription_drugs;
}

/**
 * SQL to get the average number of drugs.
 *
 * @return string
 *   The sql to get the average number of drugs.
 */
function sql_get_avg_drugs(): string {

    $avg_num_drugs = '
        WITH NumOfDrugs AS (
            SELECT
                COUNT(patientID) as numOfDrugs
            FROM
                Diagnosis
                INNER JOIN Prescription USING (diagnosisID)
                INNER JOIN PrescriptionDrugs USING (prescriptionID)
            GROUP BY
                patientID
        )
        SELECT
            avg(numOfDrugs) AS avgNumOfDrugs
        FROM
            NumOfDrugs
    ';

    return $avg_num_drugs;
}

/**
 * Function to get the patient with max number of drugs.
 *
 * @param string $type
 *   The type to get, either max or min.
 *
 * @return string
 *   The sql to get the patient with max number of drugs.
 */
function sql_get_patients_with_max_or_min_drugs(string $type): string {

    // SQL to get the user with max number of prescriptions.
    $num_drugs = '
        WITH NumOfDrugs AS (
            SELECT
                patientID,
                firstName,
                lastName,
                TIMESTAMPDIFF(YEAR, dateOfBirth, CURDATE()) AS age,
                COUNT(patientID) as numOfDrugs
            FROM
                Diagnosis
                INNER JOIN Prescription USING (diagnosisID)
                INNER JOIN PrescriptionDrugs USING (prescriptionID)
                INNER JOIN Users ON patientID = userID
            GROUP BY
                patientID,
                firstName,
                lastName,
                age
        )
        SELECT
            patientID,
            firstName,
            lastName,
            age,
            numOfDrugs
        FROM
            NumOfDrugs
        WHERE
            numOfDrugs = (
                SELECT';

    if ($type == 'max') {
        $num_drugs .= '
            max(numOfDrugs)
        ';
    }
    else {
        $num_drugs .= '
            min(numOfDrugs)
        ';
    }

    $num_drugs .= '
        FROM
            NumOfDrugs
    )
    ORDER BY
        lastName ASC';

    return $num_drugs;
}

/**
 * SQL to get number of drug companies.
 *
 * @return string
 *   The sql to get number of drug companies.
 */
function sql_get_num_drug_companies(): string {

    // SQL to get number of drug companies.
    $num_drug_companies = '
        SELECT
            COUNT(companyCode) as numOfDrugCompanies
        FROM
            DrugCompanies
    ';

    return $num_drug_companies;
}

/**
 * SQL to get the max or min drug company.
 *
 * @param string $type
 *   The type of request, max or min.
 *
 * @return string
 *   THe sql to get the max or min drug company.
 */
function sql_get_max_min_drug_companies(string $type): string {

    // SQL to get the max or min drug company.
    $drug_companies = '
        WITH NumOfDrugs AS (
            SELECT
                companyCode,
                count(CompanyCode) as numOfDrugs
            FROM
                PrescriptionDrugs
                INNER JOIN Drugs USING (drugCode)
                INNER JOIN DrugSeller USING (drugCode)
                INNER JOIN DrugCompanies USING (companyCode)
            GROUP BY
                companyName,
                companyCode 
        )
        SELECT
            numOfDrugs,
            companyName,
            suiteNumber,
            streetName,
            cityName,
            province,
            country,
            postalCode
        FROM 
            DrugCompanies
            INNER JOIN NumOfDrugs USING (companyCode)
        WHERE
            numOfDrugs = (
                SELECT';

    if ($type == 'max') {
        $drug_companies .= ' max(numOfDrugs)';
    }
    else {
        $drug_companies .= ' min(numOfDrugs)';
    }

    $drug_companies .= '
                FROM
                    NumOfDrugs
            )
    ';

    return $drug_companies;
}

/**
 * SQL to get average number of drugs per drug company.
 *
 * @return string
 *   The sql to get average number of drugs per drug company.
 */
function sql_get_avg_drugs_per_drug_company(): string {

    // SQL to get average number of drugs per drug company.
    $avg_drugs_per_drug_company = '
        WITH NumOfDrugs AS (
            SELECT
                count(companyCode) as numOfDrugs
            FROM
                PrescriptionDrugs
                INNER JOIN Drugs USING (drugCode)
                INNER JOIN DrugSeller USING (drugCode)
                INNER JOIN DrugCompanies USING (companyCode)
            GROUP BY
                companyCode
        )
        SELECT
            AVG(numOfDrugs) As avgDrugs
        FROM
            NumOfDrugs
    ';

    return $avg_drugs_per_drug_company;
}

/**
 * SQL to get number of diseases.
 *
 * @return string
 *   The sql to get the number of diseases.
 */
function sql_get_num_diseases(): string {

    // SQL to get number of diseases.
    $num_of_diseases = '
        SELECT
            COUNT(diseaseID) as numOfDiseases
        FROM
            Disease
    ';

    return $num_of_diseases;
}

/**
 * SQL to get average number of diseases.
 *
 * @return string
 *   The sql to get average number of diseases.
 */
function sql_get_avg_diseases(): string {

    // SQL to get average number of diseases.
    $avg_diseases = '
        WITH NumOfDiseases AS (
            SELECT
                COUNT(patientID) AS numOfDiseases
            FROM
                Diagnosis
            GROUP BY
                patientID
        )
        SELECT
            AVG(numOfDiseases) as avgNumOfDiseases
        FROM
            NumOfDiseases
    ';

    return $avg_diseases;
}

/**
 * SQL to get the max or min number of diseases.
 *
 * @param string $type
 *   The type to get, either min or max.
 *
 * @return string
 *   The sql to get the max or min number of diseases.
 */
function sql_get_max_or_min_diseases(string $type): string {

    // SQL to get the max or min number of diseases.
    $num_diseases = '
        WITH NumOfDiseases AS (
            SELECT
                diseaseID,
                COUNT(diseaseID) AS numOfDiseases
            FROM
                Diagnosis
            GROUP BY
                diseaseID
        )
        SELECT
            diseaseName,
            numOfDiseases
        FROM
            Disease
            INNER JOIN NumOfDiseases USING (diseaseID)
        WHERE
            numOfDiseases = (
                SELECT
    ';

    if ($type == 'max') {
        $num_diseases .= ' MAX(numOfDiseases)';
    }
    else {
        $num_diseases .= ' MIN(numOfDiseases)';
    }

    $num_diseases .= '

                FROM
                    NumOfDiseases
            )
    ';

    return $num_diseases;
}

/**
 * SQL to get number of undiagnosed diseases.
 *
 * @return string
 *   The sql to get number of undiagnosed diseases.
 */
function sql_get_undiagnosed_diseases(): string {

    // SQL to get number of undiagnosed diseases.
    $undiagnosed_diseases = '
        WITH DiagnosedDiseases AS (
            SELECT DISTINCT
                diseaseID
            FROM
                Diagnosis
        )
        SELECT
            COUNT(diseaseID) as numOfDiseases
        FROM
            Disease
        WHERE
            diseaseID NOT IN (
                SELECT
                    diseaseID
                FROM
                    DiagnosedDiseases
            )
    ';

    return $undiagnosed_diseases;
}

/**
 * SQL to get number of appointments.
 *
 * @return string
 *   The sql to get number of appointments.
 */
function sql_get_num_appointments(): string {

    // SQL to get number of appointments.
    $num_appointments = '
        SELECT
            COUNT(appointmentID) as numOfAppointments
        FROM
            Appointment
    ';

    return $num_appointments;
}

/**
 * SQL to get doctor with max/min number of appointments.
 *
 * @param string $year
 *   The year to get.
 * @param string $type
 *   Either a max or min.
 *
 * @return string
 *   The sql to get doctor with max/min number of appointments.
 */
function sql_get_doctor_max_min_appoinments_by_year(string $year, string $type): string {

    // SQL to get doctor with max number of appointments.
    $num_appointments = '
        WITH NumOfAppointments AS (
            SELECT
                doctorID,
                COUNT(doctorID) as numOfAppointments
            FROM
                Appointment
            WHERE
                YEAR(appointmentDate) = ' . $year . '
            GROUP BY
                doctorID
        ),
        NumOfDaysOff AS (
            SELECT
                doctorID,
                COUNT(doctorID) as numOfDaysOff
               FROM
                DoctorSchedule
            WHERE
                YEAR(scheduleDate) = ' . $year . '
            GROUP BY
                doctorID
        )
        SELECT
            numOfAppointments,
            numOfDaysOff,
            lastName,
            firstName
        FROM
            Users
            INNER JOIN NumOfAppointments ON doctorID = userID
            INNER JOIN NumOfDaysOff USING (doctorID)
        WHERE
            numOfAppointments = (
                SELECT';

    if ($type == 'max') {
        $num_appointments .= '
            MAX(numOfAppointments)
        ';
    }
    else {
        $num_appointments .= '
            MIN(numOfAppointments)
        ';
    }

    $num_appointments .= '
                FROM
                    NumOfAppointments
            )
    ';

    return $num_appointments;
}

/**
 * SQL to get average number of patient appointments.
 *
 * @param string $type
 *   The type of user, doctor or patient.
 * @param string $year
 *   The year to get the appointments, null for all years.
 *
 * @return string
 *   The sql to get average number of patient appointments.
 */
function sql_get_avg_appointments(string $type, string $year = NULL): string {

    // SQL to get average number of patient appointments.
    $avg_appointments = '
        WITH NumOfAppointments AS (
            SELECT
                COUNT(' . $type . 'ID) AS numOfAppointments
            FROM
                Appointment';

    if ($year) {
        $avg_appointments .= '
            WHERE
                YEAR(appointmentDate) = ' . $year;
    }

    $avg_appointments .= '
            GROUP BY
                ' . $type . 'ID
        )
        SELECT
            AVG(numOfAppointments) as avgAppointments
        FROM
            NumOfAppointments
    ';

    return $avg_appointments;
}
