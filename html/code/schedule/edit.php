<?php
    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag for has errors.
        $has_errors = FALSE;

        // Check if schedule date is empty.
        if(empty(trim($_POST["scheduleDate"]))){
            $has_errors = TRUE;
            $errors['scheduleDate'] = "Please enter schedule date.";
        }
        else{

            // Get the schedule date.
            $fields['scheduleDate'] = trim($_POST["scheduleDate"]);

            // If the schedule date is in proper formay (YYYY-MM-DD),
            // then continue to check it.
            // If not send error about format.
            if (preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $fields['scheduleDate'])) {

                // Break the schedule date into parts.
                $scheduleDate_parts = explode('-', $fields['scheduleDate']);

                // Get the year, month and day from the schedule date.
                $year = (int) $scheduleDate_parts[0];
                $month = (int) $scheduleDate_parts[1];
                $day = (int) $scheduleDate_parts[2];

                // Get the current year to compare.
                $current_year = (int) date('Y');

                // Check for month.
                if ($month < 1 || $month > 12) {
                    $has_errors = TRUE;
                    $errors['scheduleDate'] = "schedule date month must be between 01 and 12.";
                }
                // Check for day.
                else if ($day < 1 || $day > 31) {
                    $has_errors = TRUE;
                    $errors['scheduleDate'] = "schedule date day must be between 01 and 31.";
                }
            }
            else {
                $has_errors = TRUE;
                $errors['scheduleDate'] = "schedule date format is YYYY-MM-DD,";
            }
        }

        // If there are no errors, try and add the doctor schedule.
        if (!$has_errors) {

            // Add the doctor schedule.
            $add_doctor_schedule = sql_add_doctor_schedule($value, $fields['scheduleDate']);
            $add_doctor_schedule_flag = insertQuery($add_doctor_schedule);

            // Set the message based on insert query.
            if ($add_doctor_schedule_flag) {

                // Reset the fields and errors.
                $fields = [];
                $errors = [];

                // Success message.
                $_SESSION['success_message'] = 'The doctor schedule has successfully added.';
            }
            else {

                // Error message.
                $_SESSION['error_message'] = 'There was an error adding the doctor schedule';
            }
        }
    }
?>