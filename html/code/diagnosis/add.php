<?php

    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag for has errors.
        $has_errors = FALSE;

        // The array of fields.
        $fields = [];

        // The array of errors.
        $errors = [];

        // Check if description is empty
        if(empty(trim($_POST["diseaseDescription"]))){
            $has_errors = TRUE;
            $errors['diseaseDescription'] = "Please enter a description.";
        }
        else{
            $fields['diseaseDescription'] = trim($_POST["diseaseDescription"]);
        }

        // Check if disease is empty
        if(empty(trim($_POST["diseaseID"]))){
            $has_errors = TRUE;
            $errors['diseaseInfo'] = "Please enter a disease.";
        }
        else{
            $fields['diseaseInfo'] = trim($_POST["diseaseInfo"]);
            $fields['diseaseID'] = trim($_POST["diseaseID"]);
        }

        // If there are no errors, insert the diagnosis.
        if (!$has_errors) {

            // Try to insert the diagnosis.
            $insert_diagnosis = sql_insert_diagnosis($_SESSION['userID'], $type, $fields['diseaseDescription'], $fields['diseaseID'], date('Y-m-d H:i:s'));
            $insert_diagnosis_flag = insertQuery($insert_diagnosis);

            if ($insert_diagnosis_flag) {

                // Reset the variables.
                $fields = [];
                $errors = [];

                // Set the success message.
                $_SESSION['success_message'] = 'The diagnosis has successfully been added.';

                // Redirect back to the patient record.
                Header("Location: /patient/view/" . $type);
            }
            else {
                $_SESSION['error_message'] = 'There was an error adding the diagnosis.';
            }
        }
    }
?>