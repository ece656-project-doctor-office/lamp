<?php

    // Get any prescription drugs by diagnosis.
    $get_prescription_drugs_by_diagnosis = sql_get_prescription_drugs_by_diagnosis($type);
    $prescription_drugs_by_diagnosis = getAssociativeArray($get_prescription_drugs_by_diagnosis);

    // If there are drugs, then delete them first because
    // there is a foreign key.
    if (count($prescription_drugs_by_diagnosis) > 0) {

        // Step through each of the prescription drugs and delete them.
        foreach ($prescription_drugs_by_diagnosis as $drug) {

            // Try and delete the prescription drugs.
            $delete_prescription_drug = sql_delete_prescription_drug($drug['prescriptionID']);
            $delete_prescription_drug_flag = deleteQuery($delete_prescription_drug);

            // If there is an error, set message and return.
            if (!$delete_prescription_drug_flag) {
                $_SESSION['error_message'] = 'There was an error deleting the prescriptions from the diagnosis.';
                Header("Location: /patient/view/" . $value);
            }
        }
    }

    // Try and delete the prescriptions first, since there
    // is a foreign key in diagnosis on prescription.
    $delete_prescription = sql_delete_prescrition_by_diagnosis($type);
    $delete_prescription_flag = deleteQuery($delete_prescription);

    // If there is a successful delete, can finally delete
    // the diagnosis.
    if ($delete_prescription_flag) {

        // Try and delete diagnosis.
        $delete_diagnosis = sql_delete_diagnosis($type);
        $delete_diagnosis_flag = deleteQuery($delete_diagnosis);

        if ($delete_diagnosis_flag) {
            $_SESSION['success_message'] = 'The diagnosis was successfully deleted.';
        }
        else {
            $_SESSION['error_message'] = 'There was an error deleting the diagnosis.';
        }
    }
    else {
        $_SESSION['error_message'] = 'There was an error deleting the prescriptions from the diagnosis.';
    }

    Header("Location: /patient/view/" . $value);
?>