# How To Install

- Clone this repo
- Go into the directory
- Run docker-compose up
- Once container is up visit http://localhost:5000
- Go to Import and choose the create_tables.sql from the cloned directory, database and tables will be created
- Visit http://localhost:8080
- Superuser is admin and admin


# Loading data to officeDB
- Find the container ID for MYSQL container (docker ps)
- run: ./copy_site.sh <container_id>
- docker exec -it <container_id> bash
- mysql -u root -psecret officeDB < /loader/load.sql

# Generating other data for the database
- Find the container ID for MYSQL container (docker ps)
- docker exec -it <container_id> bash
- mysql -u root -p officeDB < /loader/generate_Data.sql

