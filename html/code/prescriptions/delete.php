<?php

    $get_num_prescription_drug = sql_get_num_prescription_drug($type);

    $num_prescription_drug = getAssociativeArray($get_num_prescription_drug);

    if ($num_prescription_drug[0]['numDrugs'] > 0) {

        $delete_prescription_drug = sql_delete_prescription_drug($type);

        $delete_prescription_drug_flag = deleteQuery($delete_prescription_drug);

        if (!$delete_prescription_drug) {
            $_SESSION['error_message'] = 'There was an error deleting the prescription drugs.';
            Header("Location: /patient/view/" . $value);
        }
    }

    $delete_prescription = sql_delete_prescription($type);

    $delete_prescription_flag = deleteQuery($delete_prescription);


    if ($delete_prescription_flag) {

        $_SESSION['success_message'] = 'The prescription has successfully been deleted.';
    }
    else {
        $_SESSION['error_message'] = 'There was an error deleting the prescription.';
    }

    Header("Location: /patient/view/" . $value);
?>