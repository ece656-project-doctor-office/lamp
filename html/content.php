<div class="doc-office__content contained-width">
    <?php
        if ($entity && !$action) {
            $file = $entity . '.php';
        }
        else if ($entity && $action) {
            $file = $entity . '/' . $action . '.php';
        }
        else {
            $file = 'home.php';
        }

        if (file_exists($file)) {
            include_once $file;
        }
    ?>
</div>