<?php

    // Include the code requried for connections and functions.
    include_once '../connection.php';
    include_once '../code/functions.php';
    include_once '../code/sql_statements.php';

    // Get the search term from the parameters.
    $searchTerm = $_GET['term'];

    $get_diseases = sql_get_diseases_by_search_term($searchTerm);
    $diseases = getAssociativeArray($get_diseases);

    foreach ($diseases as $disease) {
        $data[] = [
            'diseaseName' => $disease['diseaseName'],
            'diseaseID' => $disease['diseaseID'],
        ];
    }

    echo json_encode($data);
?>