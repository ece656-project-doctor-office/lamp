<?php

    // Get the current fields for the user.
    $fields = get_user_fields($value, $type);

    // Get the provinces.
    $provinces = get_provinces();

    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag for has errors.
        $has_errors = FALSE;

        // The array of fields.
        $fields = [];

        // The array of errors.
        $errors = [];

        // Get the fields, errors and flag if there are errors.
        $has_errors = get_user_fields_errors($fields, $errors, $_POST);

        // File to check for extra data.
        $file_to_check = 'code/' . $entity . '/data/' . $type . '.php';

        // If file of extra data exists, include it.
        if (file_exists($file_to_check)) {
            include_once $file_to_check;
        }

        // If we have no form errors, update user.
        if (!$has_errors) {

            // Get the SQL query to update the user.
            $update_user = sql_update_user($fields, $value);

            // Perform the update
            $update_user_flag = updateQuery($update_user);

            if ($update_user_flag) {

                // Get the sql statement to insert user into specific table.
                switch ($type) {
                    case 'patient':
                        $update_user_table = sql_update_patient($fields, $value);
                        break;
                    case 'doctor':
                        $update_user_table = sql_update_doctor($fields, $value);
                        break;
                    case 'manager':
                        $update_user_table = sql_update_manager($fields, $value);
                        break;
                }

                // Perform the update.
                $update_user_table_flag = updateQuery($update_user_table);

                if ($update_user_table_flag) {

                    // Reset the fields and errors.
                    $fields = get_user_fields($value, $type);
                    $errors = [];
 
                    // Set the success message.
                    $success_message = 'The ' . $type . ' has successfully been updated.';
                }
                else {
                    // Error message for failed updating user to a specific table.
                    $error_message = 'There was an error updating the ' . $type . '.';
                }
            }
            else {

                // Error message for failed updating user.
                $error_message = 'There was an error update the ' . $type .' as a user.';
            }
        }
    }
?>