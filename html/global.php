<?php

    // The entity to be acted on.
    $entity = $_GET['entity'];

    // Get the action to be performed.
    $action = $_GET['action'] ??  NULL;

    // Get the type of action.
    $type = $_GET['type'] ?? NULL;

    // Get the first value.
    $value = $_GET['value'] ?? NULL;

    // Get the second value.
    $value2 = $_GET['value2'] ?? NULL;

    // Get the third value.
    $value3 = $_GET['value3'] ?? NULL;

    // Get the code file to check for.
    $file_to_check = 'code/' . $entity . '/' . $action . '.php';

    // If the code file exits, add it.
    if (file_exists($file_to_check)) {
        include_once $file_to_check;
    }

    // Setup the logged_in variable based on session
    // varaibles.
    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
        $logged_in = TRUE;
    }
    else {
        $logged_in = FALSE;
    }
?>