<?php
    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(empty(trim($_POST["userID"]))){
            $errors['userInfo'] = 'You must select a doctor.';
        }
        else {
            $location = "/schedule/edit/" . $type . '/' . trim($_POST["userID"]);
            header("Location: " . $location);
        }
    }
?>