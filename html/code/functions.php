<?php

    /**
     * Function to return associative array of query result.
     *
     * @param string $sql
     *   The sql statement.
     *
     * @return $assoc_array
     *   The associative array of the query result.
     */
    function getAssociativeArray(string $sql): array {

        global $conn;

        // Ensure that we return something.
        $assoc_array = [];

        // Get the result of the sql statement.
        $result = $conn->query($sql);

        // Step through the result and get the array.
        while ($data = $result->fetch_assoc()) {
            $assoc_array[] = $data;
        }

        // Return the associative array of the query.
        return $assoc_array;
    }

    /**
     * Function to perform an update query.
     *
     * @param string $sql
     *   The sql to be performed.
     *
     * @return bool
     *   Whether the update was successful or not.
     */
    function updateQuery(string $sql): bool {

        global $conn;

        return $conn->query($sql);
    }

    /**
     * Function to perform an delete.
     *
     * @param string $sql
     *   The sql to be performed.
     *
     * @return bool
     *   Whether the delete was successful or not.
     */
    function deleteQuery(string $sql): bool {

        global $conn;

        return $conn->query($sql);
    }

    /**
     * Function to perform an insert query.
     *
     * @param string $sql
     *   The sql to be performed.
     *
     * @return bool
     *   Whether the insert was successful or not.
     */
    function insertQuery(string $sql): bool {

        global $conn;

        return $conn->query($sql);
    }

    /**
     * Function to create a user.
     *
     * @param array $fields
     *   The first name.
     * @param string $userRoleName
     *   The name of the user role.
     *
     * @return string|null
     *   The username that was created or NULL for error.
     */
    function create_user(array $fields, string $userRoleName) {

        // Get the username.
        $username = $fields['firstName'] . $fields['lastName'];

        // SQL to check for the username.
        $check_username = sql_get_users_by_username($username);

        // Get the number of users with that username.
        $num_of_users = getAssociativeArray($check_username);

        // If the username is already taken, increment by 1.
        if (count($num_of_users) > 0) {
            if (count($num_of_users) == 1) {
                $username = $username . '1';
            }
            else {
                $username = $username . (count($num_of_users) + 1);
            }
        }

        // Get the user role id.
        $user_role_id = get_user_role_id($userRoleName);

        // SQL to insert a new user.
        $insert_user = sql_insert_user($fields, $user_role_id, $username);

        // Flag to see if insert worked.
        $insert_user_flag = insertQuery($insert_user);

        // If we insert, return the username, if not reutnr NULL.
        if ($insert_user_flag) {
            return $username;
        }
        else {
            return NULL;
        }
    }

    /**
     * Function to get the user role id from a role name.
     *
     * @param string $userRole
     *   The role name.
     *
     * @return int
     *   The user role id.
     */
    function get_user_role_id(string $userRole): int {

        // SQL to get the role id.
        $get_role_id = sql_get_user_role_id_from_user_role($userRole);

        // Get the patient role id.
        $role_id = getAssociativeArray($get_role_id);

        // Return the user role id.
        return $role_id[0]['userRoleID'];
    }

    /**
     * Function to get the user id from a username.
     *
     * @param string $username
     *   The username to get for the user id.
     *
     * @return int
     *   The user id.
     */
    function get_user_id_from_username(string $username): int {

        // SQL to get the user id of newly created user.
        $get_user_id = sql_get_user_from_username($username);

        // Get the user id of newly created user.
        $user_id = getAssociativeArray($get_user_id);

        // If there is a user, return the user id, of not return -1.
        if (isset($user_id[0]['userID'])) {
            return $user_id[0]['userID'];
        }
        else {
            return -1;
        }
    }

    /**
     * Function to get the fields and errors for a add user page.
     *
     * @param array &$fields
     *   Reference to the fields array.
     * @param array &$errors
     *   Reference to the errors array.
     * @param array $post_variables
     *   Array of post variables.
     *
     * @return bool
     *   Wether or not there are errors in the fields.
     */
    function get_user_fields_errors(array &$fields, array &$errors, array $post_variables): bool {

        // Flag if there are errors in the fields.
        $has_errors = FALSE;

        // Check if first name is empty
        if(empty(trim($post_variables["firstName"]))){
            $has_errors = TRUE;
            $errors['firstName'] = "Please enter first name.";
        }
        else{
            $fields['firstName'] = trim($post_variables["firstName"]);
        }

        // Check if last name is empty.
        if(empty(trim($post_variables["lastName"]))){
            $has_errors = TRUE;
            $errors['lastName'] = "Please enter last name.";
        }
        else{
            $fields['lastName'] = trim($post_variables["lastName"]);
        }

        // Check if date of birth is empty.
        if(empty(trim($post_variables["dateOfBirth"]))){
            $has_errors = TRUE;
            $errors['dateOfBirth'] = "Please enter date of birth.";
        }
        else{

            // Get the date of birth.
            $fields['dateOfBirth'] = trim($post_variables["dateOfBirth"]);

            // If the date of birth is in proper formay (YYYY-MM-DD),
            // then continue to check it.
            // If not send error about format.
            if (preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $fields['dateOfBirth'])) {

                // Break the date of birth into parts.
                $dateOfBirth_parts = explode('-', $fields['dateOfBirth']);

                // Get the year, month and day from the date of birth.
                $year = (int) $dateOfBirth_parts[0];
                $month = (int) $dateOfBirth_parts[1];
                $day = (int) $dateOfBirth_parts[2];

                // Get the current year to compare.
                $current_year = (int) date('Y');

                // Check for year.
                if ($year > $current_year || $year < $current_year - 125) {
                    $has_errors = TRUE;
                    $errors['dateOfBirth'] = "Date of birth year is out of range.";
                }
                // Check for month.
                else if ($month < 1 || $month > 12) {
                    $has_errors = TRUE;
                    $errors['dateOfBirth'] = "Date of birth month must be between 01 and 12.";
                }
                // Check for day.
                else if ($day < 1 || $day > 31) {
                    $has_errors = TRUE;
                    $errors['dateOfBirth'] = "Date of birth day must be between 01 and 31.";
                }
            }
            else {
                $has_errors = TRUE;
                $errors['dateOfBirth'] = "Date of birth format is YYYY-MM-DD,";
            }
        }

        
        // Check if phone is empty and valid.
        if(empty(trim($post_variables["phone"]))){
            $has_errors = TRUE;
            $errors['phone'] = "Please enter phone number.";
        }
        else{
            if (
                preg_match("/^[0-9]{10}$/", $post_variables["phone"]) &&
                preg_match("/[1-9]{1}[0-9]{9}/", $post_variables["phone"])
            ) {
                $fields['phone'] = trim($post_variables["phone"]);
            }
            else {
                $has_errors = TRUE;
                $fields['phone'] = trim($post_variables["phone"]);
                $errors['phone'] = "Please enter a valid phone number";
            }
        }

        // Check if email is empty.
        if(empty(trim($post_variables["email"]))){
            $has_errors = TRUE;
            $errors['email'] = "Please enter email address.";
        }
        else{
            $fields['email'] = trim($post_variables["email"]);
        }

        // Check if gender is empty.
        if(empty(trim($post_variables["gender"]))){
            $has_errors = TRUE;
            $errors['gender'] = "Please select the gender.";
        }
        else{
            $fields['gender'] = trim($post_variables["gender"]);
        }

        // Check if address line one is empty.
        if(empty(trim($post_variables["addressLineOne"]))){
            $has_errors = TRUE;
            $errors['addressLineOne'] = "Please enter last name.";
        }
        else{
            $fields['addressLineOne'] = trim($post_variables["addressLineOne"]);
        }

        // Get the address line two.
        $fields['addressLineTwo'] = trim($post_variables["addressLineTwo"]);

        // Check if city is empty.
        if(empty(trim($post_variables["city"]))){
            $has_errors = TRUE;
            $errors['city'] = "Please enter the city.";
        }
        else{
            $fields['city'] = trim($post_variables["city"]);
        }

        // Check if province is empty.
        if(empty(trim($post_variables["province"]))){
            $has_errors = TRUE;
            $errors['province'] = "Please select the province.";
        }
        else{
            $fields['province'] = trim($post_variables["province"]);
        }

        // Check if postal code is empty and valid.
        if(empty(trim($post_variables["postalCode"]))){
            $has_errors = TRUE;
            $errors['postalCode'] = "Please enter the postal code.";
        }
        else{
            if (
                preg_match("/^[0-9A-Z]{6}$/", $post_variables["postalCode"]) &&
                preg_match("/([A-Z]{1}[0-9]{1}){3}/", $post_variables["postalCode"])
            ) {
                $fields['postalCode'] = trim($post_variables["postalCode"]);
            }
            else {
                $has_errors = TURE;
                $fields['postalCode'] = trim($post_variables["postalCode"]);
                $errors['postalCode'] = "Please enter a valid postal code";
            }
        }

        // Check if status is empty.
        if(empty(trim($post_variables["status"]))){
            $has_errors = TRUE;
            $errors['status'] = "Please select the status.";
        }
        else{
            $fields['status'] = trim($post_variables["status"]);
        }

        return $has_errors;
    }

    /**
     * Function to get the fields and errors for a office info.
     *
     * @param array &$fields
     *   Reference to the fields array.
     * @param array &$errors
     *   Reference to the errors array.
     * @param array $post_variables
     *   Array of post variables.
     *
     * @return bool
     *   Wether or not there are errors in the fields.
     */
    function get_office_info_fields_errors(array &$fields, array &$errors, array $post_variables): bool {

        // Flag to see if there are errors in the form.
        $has_errors = FALSE;

        // Check if name is empty.
        if(empty(trim($post_variables["name"]))){
            $has_errors = TRUE;
            $errors['name'] = "Please enter the office name.";
        }
        else{
            $fields['name'] = trim($post_variables["name"]);
        }

        // Check if address is empty.
        if(empty(trim($post_variables["address"]))){
            $has_errors = TRUE;
            $errors['address'] = "Please enter the address.";
        }
        else{
            $fields['address'] = trim($post_variables["address"]);
        }

        // Check if city is empty.
        if(empty(trim($post_variables["city"]))){
            $has_errors = TRUE;
            $errors['city'] = "Please enter the city.";
        }
        else{
            $fields['city'] = trim($post_variables["city"]);
        }

        // Check if province is empty.
        if(empty(trim($post_variables["province"]))){
            $has_errors = TRUE;
            $errors['province'] = "Please select the province.";
        }
        else{
            $fields['province'] = trim($post_variables["province"]);
        }

        // Check if postal code is empty.
        if(empty(trim($post_variables["postalCode"]))){
            $has_errors = TRUE;
            $errors['postalCode'] = "Please enter the postal code.";
        }
        else{
            $fields['postalCode'] = trim($post_variables["postalCode"]);
        }

        // Check if phone is empty.
        if(empty(trim($post_variables["phone"]))){
            $has_errors = TRUE;
            $errors['phone'] = "Please enter the phone.";
        }
        else{
            $fields['phone'] = trim($post_variables["phone"]);
        }

        // Get fax field, it can be empty.
        $fields['fax'] = trim($post_variables["fax"]);

        // Get email field, it can be empty.
        $fields['email'] = trim($post_variables["email"]);

        // Check if welcome message is empty.
        if(empty(trim($post_variables["welcomeMessage"]))){
            $has_errors = TRUE;
            $errors['welcomeMessage'] = "Please enter the postal code.";
        }
        else{
            $fields['welcomeMessage'] = trim($post_variables["welcomeMessage"]);
        }

        return $has_errors;
    }

    /**
     * Function to get the provinces.
     *
     * @return array
     *   Array of provinces.
     */
    function get_provinces(): array {

        // SQL to get provinces.
        $get_provinces = sql_get_provinces();

        // Return the provinces.
        return getAssociativeArray($get_provinces);
    }

    /**
     * Function to get the current number users by role.
     *
     * @param string $userRole
     *   The user role.
     * @param string $status_type
     *   The status of the users to get.
     *
     * @return int
     *   The number of active users in that role.
     */
    function get_num_of_users_by_role(string $userRole, string $status_type = 'active') {

        // SQL to get the number of users.
        $get_num_of_users = sql_get_users_by_role($userRole, $status_type);

        // Get the number of users by role.
        $num_of_users = getAssociativeArray($get_num_of_users);

        // Return the actual number of users by role.
        return $num_of_users[0]['numOfUsers'];
    }

    /**
     * Function to get all the fields with of a user.
     *
     * @param int $userID
     *   The user id.
     * @param string $type
     *   The type of user.
     *
     * @return array
     *   The array of fields for the user.
     */
    function get_user_fields(int $userID, string $type): array {

        // Get the table to join on for extra data.
        switch ($type) {
            case 'patient':
                $table = 'Patients';
                break;
            case 'doctor':
                $table = 'Doctor';
                break;
            case 'manager':
                $table = 'Manager';
                break;
        }

        // Query to get the user.
        $get_user = sql_get_user_fields($userID, $table, $type);

        // Get the users.
        $users = getAssociativeArray($get_user);

        // Step through each of the users and get the fields.
        foreach ($users[0] as $index => $value) {
            $fields[$index] = $value;
        }

        // Return the fields.
        return $fields;
    }

    /**
     * Function to get the fields for the office info.
     *
     * @return array
     *   Array of office info fields.
     */
    function get_office_info_fields(): array {

        // SQL to get the office info fields.
        $get_office_info_fields = sql_get_office_info();

        // Get the office info fields.
        $office_info_fields = getAssociativeArray($get_office_info_fields);

        // Step through each of the fields and setup return array.
        foreach ($office_info_fields[0] as $index => $office_info_field) {
            $fields[$index] = $office_info_field;
        }

        return $fields;
    }

    /**
     * Function to get the number of appointments for a given day.
     *
     * @param string $date
     *   The date to look at.
     *
     * @return int
     *   The number of available appointments.
     */
    function get_num_of_appointments_by_date(string $date): int {

        // Get the total number of active docs.
        $get_num_of_docs = sql_get_num_of_docs($date);
        $num_of_docs = getAssociativeArray($get_num_of_docs);

        // Get the number of doctors off that day.
        $get_num_of_docs_off = sql_get_num_of_docs_off_by_date($date);
        $num_of_docs_off = getAssociativeArray($get_num_of_docs_off);

        // Caculate the number of available docs, which is 20 min
        // appointments * 7 hours, so 21.
        // @TODO in later version this should be set in settings.
        $num_of_available_docs = ($num_of_docs[0]['numOfDocs'] - $num_of_docs_off[0]['numOfDocs']) * 18;

        // Get the number of appointments alreay for the given day.
        $get_num_of_appointments = sql_get_num_of_appointments_by_day($date);
        $num_of_appointments = getAssociativeArray($get_num_of_appointments);

        // Return the number of appointments for that day.
        return $num_of_available_docs - $num_of_appointments[0]['numOfAppointments'];
    }

    /**
     * Function to get the age from a date of birth.
     *
     * @param string $dob
     *   The date of birth.
     *
     * @return string
     *   The age.
     */
    function get_age(string $dob): string {

        // Return the age.
        return intval(date('Y', time() - strtotime($dob))) - 1970;
    }
?>