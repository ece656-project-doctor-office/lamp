<?php

    // Get the provinces.
    $provinces = get_provinces();

    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag for has errors.
        $has_errors = FALSE;

        // The array of fields.
        $fields = [];

        // The array of errors.
        $errors = [];

        // Get the fields, errors and flag if there are errors.
        $has_errors = get_user_fields_errors($fields, $errors, $_POST);

        // File to check for extra data.
        $file_to_check = 'code/' . $entity . '/data/' . $type . '.php';

        // If file of extra data exists, include it.
        if (file_exists($file_to_check)) {
            include_once $file_to_check;
        }

        // If we have no form errors, create user and add
        // to user type table.
        if (!$has_errors) {

            // Get the username, if not created correctly, it will be NULL.
            $username = create_user($fields, $type);

            // If the insert worked, we can add the patient.
            if ($username) {
        
                // Get the user id from the username.
                $fields['userID'] = get_user_id_from_username($username);

                // Include the list of sql statements.
                include_once 'code/sql_statements.php';

                // Get the sql statement to insert user into specific table.
                switch ($type) {
                    case 'patient':
                        $insert_user_table = sql_insert_patient($fields);
                        break;
                    case 'doctor':
                        $insert_user_table = sql_insert_doctor($fields);
                        break;
                    case 'manager':
                        $insert_user_table = sql_insert_manager($fields);
                        break;
                }

                // Flag to see if inserting user into specific worked.
                $insert_user_table_flag = insertQuery($insert_user_table);

                // If the insert work, clear the variables and set
                // success message.
                if ($insert_user_table_flag) {

                    // Reset the fields and errors.
                    $fields = [];
                    $errors = [];

                    $success_message = 'The ' . $type . ' has successfully been added with username set to ' . $username . '.';
                }
                else {

                    // Error message for failed inserting user to a specific table.
                    $error_message = 'There was an error adding the ' . $type . '.';
                }
            }
            else {

                // Error message for failed inserting user.
                $error_message = 'There was an error adding the ' . $type .' as a user.';
            }
        }
    }
?>