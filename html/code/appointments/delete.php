<?php

    // Get the appointment id.
    $appointmentID = $value;

    // Try and delete the appointment.
    $delete_appointment = sql_delete_appointment($appointmentID);
    $delete_appointment_flag = deleteQuery($delete_appointment);

    // Set message based on flag.
    if ($delete_appointment_flag) {
        $_SESSION['success_message'] = 'The appointment has been successfully deleted.';
    }
    else {
        $_SESSION['error_message'] = 'There was an error deleting the appointment.';
    }

    header("Location: /appointments/edit/" . $type);
?>