<?php
    $get_doctors = sql_get_doctors_available_by_date($type);
    $doctors = getAssociativeArray($get_doctors);

    $doc_count = count($doctors);

    if ($doc_count == 0) {
        $class = 'doc-office__edit-appointment-no-docs';
    }
    else {
        $class = 'doc-office__edit-appointment';
    }
?>
<div class="<?php echo $class; ?>">

    <?php include_once 'success_error.php'; ?>

    <?php
        $discard_start_times = [
            '12:00 pm',
            '12:20 pm',
            '12:40 pm',
        ];

        $get_doctors = sql_get_doctors_available_by_date($type);
        $doctors = getAssociativeArray($get_doctors);

        if ($doc_count > 0) {

            $html = '<table>';

            $html .= '<caption>Edit appointments for ' . date('l F d, Y', strtotime($type)) . '</caption>';

            $html .= '<tr>';

            $html .= '<th scope="col">Time</th>';

            foreach ($doctors as $doctor) {
                $html .= '<th scope="col">' . $doctor['lastName'] . ', ' . $doctor['firstName'] . '</th>';
            }

            $html .= '</tr>';

            $date_parts = explode('-', $type);
            $start_time = date('g:i a', mktime(9, 0, 0, $date_parts[1], $date_parts[2], $date_parts[0]));
            $end_time = date('g:i a', strtotime($start_time . ' +20 minutes'));

            for ($i = 0; $i < 21; $i++) {

                $html .= '<tr>';

                if (!in_array($start_time, $discard_start_times)) {
                    $html .= '<th scope="row">';
                    $html .= $start_time . ' - ' . date('g:i a', strtotime($start_time . ' +20 minutes'));
                    $html .= '</th>';
                }
                else {
                    if ($start_time == '12:00 pm') {
                        $html .= '<tr>';
                        $html .= '<th>Closed (Lunch)</th>';
                    }
                }

                $get_start_time = $type . ' ' . date('G:i:s', strtotime($start_time));
                $get_end_time = $type . ' ' . date('G:i:s', strtotime($start_time . ' +20 minutes'));

                foreach ($doctors as $doctor) {

                    if (!in_array($start_time, $discard_start_times)) {
                        $get_appointment = sql_get_appointment($doctor['doctorID'], $type, $get_start_time, $get_end_time);
                        $appointment = getAssociativeArray($get_appointment);

                        if (count($appointment) > 0) {
                            $html .= '<td class="doc-office__edit-appointment--delete">';
                            $html .= $appointment[0]['lastName'] . ', ' . $appointment[0]['firstName'];
                            $html .= '<a href="/appointments/delete/' . $type . '/'. $appointment[0]['appointmentID'] . '">&ndash;</a>';
                            $html .= '</td>';
                        }
                        else {
                            $html .= '<td class="doc-office__edit-appointment--add">';
                            $link = $type . '/' . strtotime($get_start_time) . '/' . strtotime($get_end_time) . '/' . $doctor['doctorID'];
                            $html .= '<a href="/appointments/add/' . $link . '">+</a>';
                            $html .= '</td>';
                        }
                    }
                    else {
                        if ($start_time == '12:00 pm') {
                            $html .= '<td class="closed"></td>';
                        }
                    }
                }

                $html .= '</tr>';

                $start_time = date('g:i a', strtotime($start_time . ' +20 minutes'));
            }

        
            $html .= '</table>';
        }
        else {
            $html = '<h2>Edit appointments for ' . date('l F d, Y', strtotime($type)) . '</h2>';
            $html .= '<div class="error-message">There are no doctors available for this date.</div>';
        }

        echo $html;
    ?>
</div>