<?php

    // If the type is doctor, delete the schedule.
    if ($type == 'doctor') {

        // Delete the doctor schedule.
        $delete_doctor_schedule = sql_delete_doctor_schedule($value, $value2);
        $delete_doctor_schedule_flag = deleteQuery($delete_doctor_schedule);

        // Set the message based on delete query.
        if ($delete_doctor_schedule_flag) {
            $_SESSION['success_message'] = 'The doctor schedule has successfully been deleted.';
        }
        else {
            $_SESSION['error_message'] = 'There was an error deleting the doctor schedule.';
        }
    }

    // Return back to the edit schedule page.
    header("Location: /schedule/edit/doctor/" . $value);
?>