<div class="doc-office__manage">
    <h2>Appointments</h2>
    <a href="appointments/show">Show appointments</a>

    <h2>Patient Management</h2>
    <h3>(<?php echo get_num_of_users_by_role('patient'); ?> active users)</h3>
    <a href="users/add/patient">Add patient</a>
    <a href="users/choose/patient">Edit patient</a>

    <h2>Doctor Management</h2>
    <h3>(<?php echo get_num_of_users_by_role('doctor'); ?> active users)</h3>
    <a href="users/add/doctor">Add doctor</a>
    <a href="users/choose/doctor">Edit doctor</a>
    <a href="schedule/choose/doctor">Edit doctor schedule</a>

    <h2>Manager Management</h2>
    <h3>(<?php echo get_num_of_users_by_role('manager'); ?> active users)</h3>
    <a href="users/add/manager">Add manager</a>
    <a href="users/choose/manager">Edit manager</a>

    <h2>Office Management</h2>
    <a href="info/edit">Edit office info</a>
</div>