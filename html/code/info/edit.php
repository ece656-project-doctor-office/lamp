<?php

    // Get the provinces.
    $provinces = get_provinces();

    $fields = get_office_info_fields();

    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag for has errors.
        $has_errors = FALSE;

        // The array of fields.
        $fields = [];

        // The array of errors.
        $errors = [];

        // Get the fields, errors and flag if there are errors.
        $has_errors = get_office_info_fields_errors($fields, $errors, $_POST);

        if (!$has_errors) {

            // SQL to update the office info.
            $update_office_info = sql_update_office_info($fields);

            // Flag of update was completed.
            $update_office_info_flag = updateQuery($update_office_info);

            // If update succeed set success mesage and reset variables.
            // If not set the error message.
            if ($update_office_info_flag) {
                $fields = [];
                $errors = [];
                $success_message = 'The office info has successfully been updated.';
            }
            else {
                $error_message = 'There was an error updating the office info.';
            }
        }
    }

?>