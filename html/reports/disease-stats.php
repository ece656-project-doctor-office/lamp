<?php
    $get_num_diseases = sql_get_num_diseases();
    $num_diseases = getAssociativeArray($get_num_diseases);

    $get_avg_diseases = sql_get_avg_diseases();
    $avg_diseases = getAssociativeArray($get_avg_diseases);

    $get_undiagnosed_diseases = sql_get_undiagnosed_diseases();
    $undiagnosed_diseases = getAssociativeArray($get_undiagnosed_diseases);

    $get_max_diseases = sql_get_max_or_min_diseases('max');
    $max_diseases = getAssociativeArray($get_max_diseases);

    $get_min_diseases = sql_get_max_or_min_diseases('min');
    $min_diseases = getAssociativeArray($get_min_diseases);
?>

<div class="doc-office__reports doc-office__reports--disease">
    <h2>Disease Statistics</h2>
    <h3></h3>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Details
        </summary>
        <div class="doc-office__details--content">
            <b>Number of diseases:</b> <?php echo $num_diseases[0]['numOfDiseases']; ?><br />
            <b>Number of undiagnosed diseases:</b> <?php echo $undiagnosed_diseases[0]['numOfDiseases']; ?><br />
            <b>Average number of diseases per patient:</b> <?php echo $avg_diseases[0]['avgNumOfDiseases']; ?><br />
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Most Diagnosed Disease
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Number of diagnosed</th>';
                    $html .= '<th>Disease Name</th>';
                    $html .= '</tr>';

                    foreach ($max_diseases as $max_disease) {
                        $html .= '<tr>';
                        $html .= '<td>' . $max_disease['numOfDiseases'] . '</td>';
                        $html .= '<td>' . $max_disease['diseaseName'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Least Diagnosed Disease
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
            <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Number of diagnosed</th>';
                    $html .= '<th>Disease Name</th>';
                    $html .= '</tr>';

                    foreach ($min_diseases as $min_disease) {
                        $html .= '<tr>';
                        $html .= '<td>' . $min_disease['numOfDiseases'] . '</td>';
                        $html .= '<td>' . $min_disease['diseaseName'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

</div>