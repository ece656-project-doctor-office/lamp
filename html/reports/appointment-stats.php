<?php
    $get_num_appointments = sql_get_num_appointments();
    $num_appointments = getAssociativeArray($get_num_appointments);

    $get_avg_patient_appointments = sql_get_avg_appointments('patient');
    $avg_patient_appointments = getAssociativeArray($get_avg_patient_appointments);

    $get_avg_doctor_appointments = sql_get_avg_appointments('doctor');
    $avg_doctor_appointments = getAssociativeArray($get_avg_doctor_appointments);

    for ($year = 2022; $year >= 2018; $year--) {

        $get_avg = sql_get_avg_appointments('patient', $year);
        $avg = getAssociativeArray($get_avg);

        $avg_by_years[$year]['patient'] = $avg[0]['avgAppointments'];

        $get_avg = sql_get_avg_appointments('doctor', $year);
        $avg = getAssociativeArray($get_avg);

        $avg_by_years[$year]['doctor'] = $avg[0]['avgAppointments'];

        $get_max_doctor = sql_get_doctor_max_min_appoinments_by_year($year, 'max');

        $max_doctors[$year] = getAssociativeArray($get_max_doctor);

        $get_min_doctor = sql_get_doctor_max_min_appoinments_by_year($year, 'min');

        $min_doctors[$year] = getAssociativeArray($get_min_doctor);
    }
?>

<div class="doc-office__reports doc-office__reports--appointments">
    <h2>Appointment Statistics</h2>
    <h3></h3>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Details
        </summary>
        <div class="doc-office__details--content">
            <b>Number of Appointments:</b> <?php echo $num_appointments[0]['numOfAppointments']; ?><br />
            <b>Total average appointments by patients since 2018:</b> <?php echo $avg_patient_appointments[0]['avgAppointments']; ?><br />
            <b>Total average appointments by doctors since 2018:</b> <?php echo $avg_doctor_appointments[0]['avgAppointments']; ?>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Average number of appointments by year
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Year</th>';
                    $html .= '<th>Doctor Average Appointments</th>';
                    $html .= '<th>Patient Average Appointments</th>';
                    $html .= '</tr>';

                    foreach ($avg_by_years as $year => $avg_by_year) {
                        $html .= '<tr>';
                        $html .= '<td>' . $year . '</td>';
                        $html .= '<td>' . $avg_by_year['doctor'] . '</td>';
                        $html .= '<td>' . $avg_by_year['patient'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Doctor With Most Appointments
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
            <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Year</th>';
                    $html .= '<th>Doctor</th>';
                    $html .= '<th>Number of Appointments</th>';
                    $html .= '<th>Number of days off</th>';
                    $html .= '<th>Average Appointments Per Day</th>';
                    $html .= '</tr>';

                    foreach ($max_doctors as $year => $max_doctor) {
                        foreach ($max_doctor as $doctor) {
                            $html .= '<tr>';
                            $html .= '<td>' . $year . '</td>';
                            $html .= '<td>' . $doctor['lastName'] . ', ' . $doctor['firstName']  . '</td>';
                            $html .= '<td>' . $doctor['numOfAppointments'] . '</td>';
                            $html .= '<td>' . $doctor['numOfDaysOff'] . '</td>';
                            $html .= '<td>' . round($doctor['numOfAppointments'] / (261 - $doctor['numOfDaysOff']), 4) . '</td>';
                            $html .= '</tr>';
                        }
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Doctor With Least Appointments
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
            <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Year</th>';
                    $html .= '<th>Doctor</th>';
                    $html .= '<th>Number of Appointments</th>';
                    $html .= '<th>Number of days off</th>';
                    $html .= '<th>Average Appointments Per Day</th>';
                    $html .= '</tr>';

                    foreach ($min_doctors as $year => $min_doctor) {
                        foreach ($min_doctor as $doctor) {
                            $html .= '<tr>';
                            $html .= '<td>' . $year . '</td>';
                            $html .= '<td>' . $doctor['lastName'] . ', ' . $doctor['firstName']  . '</td>';
                            $html .= '<td>' . $doctor['numOfAppointments'] . '</td>';
                            $html .= '<td>' . $doctor['numOfDaysOff'] . '</td>';
                            $html .= '<td>' . round($doctor['numOfAppointments'] / (261 - $doctor['numOfDaysOff']), 4) . '</td>';
                            $html .= '</tr>';
                        }
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

</div>