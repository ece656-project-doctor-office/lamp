<?php
    include_once 'code/sql_generate.php';
    include_once 'code/functions_generate.php';
    include_once 'code/sql_statements.php';
    include_once 'code/functions.php';

    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        $has_errors = FALSE;

        if (isset($_POST['gen_type']) && $_POST['gen_type']) {

            $gen_type = $_POST['gen_type'];

            switch ($gen_type) {
                case 'doctor';
                case 'patient':
                case 'manager':
                    if (isset($_POST['numToGenerate']) && $_POST['numToGenerate']) {
                        $num_to_gen = $_POST['numToGenerate'];
                    }
                    else {
                        $has_errors = TRUE;
                        $_SESSION['error_message'] = 'You must enter a number to generate.';
                    }
            }
        }
        else {
            $has_errors = TRUE;
            $_SESSION['error_message'] = 'You must select a generation type.';
        }

        if (!$has_errors) {

            switch ($gen_type) {
                case 'doctor';
                case 'patient':
                case 'manager':
                    generate_users($gen_type, $num_to_gen);
                    $gen_type = NULL;
                    $num_to_gen = NULL;
                    break;
            }
        }
     }
?>