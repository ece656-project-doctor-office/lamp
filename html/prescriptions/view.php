<?php
    $get_prescription = sql_get_prescription($type);
    $prescription = getAssociativeArray($get_prescription);

    $get_patient_info = sql_get_user_first_last_name($value);
    $patient_info = getAssociativeArray($get_patient_info);
?>

<div class="doc-office__prescription--view">
    <h2>Prescription for <?php echo $patient_info[0]['firstName'] . ' ' . $patient_info[0]['lastName']; ?></h2>

    <b>Prescribed by:</b> <?php echo $prescription[0]['doctorLastName'], ', ' . $prescription[0]['doctorFirstName']; ?>
    <br /><br />
    <b>Prescribed on:</b> <?php echo date('M d, Y', strtotime($prescription[0]['prescribedDate'])) ?>
    <br /><br />
    <?php if ($prescription[0]['drugName']) { ?>
        <b>Drug:</b> <?php echo ucfirst(strtolower($prescription[0]['drugName'])); ?>
        <br /><br />
        <b>Dosage:</b> <?php echo $prescription[0]['dosage']; ?>

        <?php if ($prescription[0]['instructions']) { ?>
            <br /><br /><b>Instructions:</b> <?php echo $prescription[0]['instructions']; ?>
        <?php } ?>
    <?php } else { ?>
        <b>Prescription:</b><?php echo $prescription[0]['prescription']; ?>
    <?php } ?>
</div>