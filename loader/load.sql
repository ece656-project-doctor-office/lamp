load data infile '/loader/diseases.csv' ignore into table officeDB.Disease fields terminated by ',' lines terminated by '\n';

load data infile '/loader/drugs.csv' ignore into table officeDB.Drugs FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n' (
     @drugCode,
     @productCategorization,
     @class,
     @drugIdentificationNumber,
     @brandName,
     @descriptor,
     @pediatricFlag,
     @accessionNumber,
     @numberOfAis,
     @lastUpdateDate,
     @aiGroupNo,
     @classF,
     @brandNameF,
     @descriptorF
)
SET
     drugCode = @drugCode,
     productCategorization = @productCategorization,
     class = @class,
     drugIdentificationNumber = @drugIdentificationNumber,
     brandName = @brandName,
     descriptor = @descriptor,
     pediatricFlag = @pediatricFlag,
     accessionNumber = @accessionNumber,
     numberOfAis = @numberOfAis,
     lastUpdateDate = CASE
          WHEN @lastUpdateDate NOT IN ('', 'NA') THEN STR_TO_DATE(@lastUpdateDate, '%d-%b-%Y')
          else Null
     END,
     aiGroupNo = @aiGroupNo,
     classF = @classF,
     brandNameF = @brandNameF,
     descriptorF = @descriptorF;

load data infile '/loader/status.csv' ignore into table officeDB.DrugStatus FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n' (
     @drugCode,
     @statusFlag,
     @status,
     @historyDate,
     @statusF,
     @notrequired1,
     @notrequired2
)
SET
     drugCode = @drugCode,
     statusFlag = @statusFlag,
     status = @status,
     historyDate = CASE
          WHEN @historyDate NOT IN ('', 'NA') THEN STR_TO_DATE(@historyDate, '%d-%b-%Y')
          else Null
     END,
     statusF = @statusF;

load data infile '/loader/companies.csv' ignore into table officeDB.DrugCompanies FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n' (
     @nr1,
     @nr2,
     @companyCode,
     @companyName,
     @companyType,
     @nr3,
     @nr4,
     @nr5,
     @nr6,
     @suiteNumber,
     @streetName,
     @cityName,
     @province,
     @country,
     @postalCode,
     @nr7,
     @nr8,
     @nr9
)
SET
     companyCode = @companyCode,
     companyName = @companyName,
     companyType = @companyType,
     suiteNumber = @suiteNumber,
     streetName = @streetName,
     cityName = @cityName,
     province = @province,
     country = @country,
     postalCode = @postalCode;

load data infile '/loader/companies.csv' ignore into table officeDB.DrugSeller FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n' (
     @drugCode,
     @nr2,
     @companyCode,
     @companyName,
     @companyType,
     @nr3,
     @nr4,
     @nr5,
     @nr6,
     @suiteNumber,
     @streetName,
     @cityName,
     @province,
     @country,
     @postalCode,
     @nr7,
     @nr8,
     @nr9
)
SET
     drugCode = @drugCode,
     companyCode = @companyCode;

load data infile '/loader/Users.csv' ignore into table officeDB.Users FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n';

load data infile '/loader/Patients.csv' ignore into table officeDB.Patients FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n';

load data infile '/loader/Doctor.csv' ignore into table officeDB.Doctor FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n';

load data infile '/loader/Manager.csv' ignore into table officeDB.Manager FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n';

load data infile '/loader/DoctorSchedule.csv' ignore into table officeDB.DoctorSchedule FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n';

load data infile '/loader/Appointment.csv' ignore into table officeDB.Appointment FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n';

load data infile '/loader/Diagnosis.csv' ignore into table officeDB.Diagnosis FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n';

load data infile '/loader/Prescription.csv' ignore into table officeDB.Prescription FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n';

load data infile '/loader/PrescriptionDrugs.csv' ignore into table officeDB.PrescriptionDrugs FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n';