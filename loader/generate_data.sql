DROP TABLE IF EXISTS Address;
DROP TABLE IF EXISTS RegionOfWaterlooAddress;
DROP TABLE IF EXISTS Surnames;
DROP TABLE IF EXISTS FirstNames;
DROP TABLE IF EXISTS FirstNamesMale;
DROP TABLE IF EXISTS FirstNamesFemale;

create table officeDB.Address(
    unit char(30),
    city char(50),
    address char(100)
);

CREATE INDEX AddressIdx ON officeDB.Address (city);

LOAD DATA infile '/loader/addresses.csv' ignore INTO TABLE officeDB.Address FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n' (
    @latitude,
    @longitude,
    @source_id,
    @id,
    @group_id,
    @street_no,
    @street,
    @str_name,
    @str_type,
    @str_dir,
    @unit,
    @city,
    @postal_code,
    @full_addr,
    @city_pcs,
    @str_name_pcs,
    @str_type_pcs,
    @str_dir_pcs,
    @csduid,
    @csdname,
    @pruid,
    @provider
)
SET
    unit = @unit,
    city = @city,
    address = @full_addr;

CREATE TABLE officeDB.RegionOfWaterlooAddress (
    id int NOT NULL AUTO_INCREMENT,
    unit varchar(10),
    address varchar(100),
    city varchar(50),
    PRIMARY KEY (id)
);

INSERT INTO officeDB.RegionOfWaterlooAddress (unit, address, city)
SELECT
    unit,
    address,
    city
FROM
    Address
WHERE
    city = 'Waterloo' OR
    city = 'Kitchener' OR
    city = 'Cambridge' OR
    city = 'North Dumfries' OR
    city = 'Wellesley' OR
    city = 'Wilmot' OR
    city = 'Woolwich';

DROP TABLE IF EXISTS Address;

CREATE TABLE officeDB.FirstNames (
    id int NOT NULL AUTO_INCREMENT,
    firstName varchar(70) NOT NULL,
    gender char(1) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE officeDB.FirstNamesMale (
    id int NOT NULL AUTO_INCREMENT,
    firstName varchar(70) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE officeDB.FirstNamesFemale (
    id int NOT NULL AUTO_INCREMENT,
    firstName varchar(70) NOT NULL,
    PRIMARY KEY (id)
);

LOAD DATA infile '/loader/first_names_gender.csv' ignore INTO TABLE officeDB.FirstNames fields terminated by ',' lines terminated by '\r\n' (
    @firstName,
    @gender
)
SET
    firstName = trim(@firstName),
    gender = @gender;

INSERT INTO FirstNamesMale (firstName)
SELECT
    firstName
FROM
    FirstNames
WHERE
    gender = 'M';

INSERT INTO FirstNamesFemale (firstName)
SELECT
    firstName
FROM
    FirstNames
WHERE
    gender = 'F';

DROP TABLE FirstNames;

CREATE TABLE officeDB.Surnames (
    id int NOT NULL AUTO_INCREMENT,
    surname varchar(70) NOT NULL,
    PRIMARY KEY (id)
);

LOAD DATA infile '/loader/surnames.csv' ignore INTO TABLE officeDB.Surnames FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n' (
    @surname
)
SET
    surname = trim(@surname);