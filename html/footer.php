<div class="doc-office__footer">
    <div class="contained-width">
        <?php
            $get_footer_info = sql_get_footer_info();

            $footer_info = getAssociativeArray($get_footer_info);
        ?>

        <div class="doc-office__footer--wrapper">
            <div class="doc-office__footer--left">
                <?php echo $footer_info[0]['address']; ?><br />
                <?php echo $footer_info[0]['city']; ?>, <?php echo $footer_info[0]['province']; ?><br />
                <?php echo $footer_info[0]['postalCode']; ?>
            </div>
            <div class="doc-office__footer--right">
                <?php if ($footer_info[0]['email']) { ?>
                    <a href="mailto:<?php echo $footer_info[0]['email']; ?>" ?>
                        <?php echo $footer_info[0]['email']; ?>
                    </a>
                    <br />
                <?php } ?>
                Phone: <?php echo $footer_info[0]['phone']; ?><br />
                <?php if ($footer_info[0]['fax']) { ?>
                    Fax: <?php echo $footer_info[0]['fax']; ?><br />
                <?php } ?>
            </div>
        </div>
    </div>
</div>
