<?php

    // Check if the user is already logged in, if yes then redirect him to welcome page
    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
        header("location: /");
        exit;
    }

    // Processing form data when form is submitted
    if($_SERVER["REQUEST_METHOD"] == "POST"){
 
        // Check if username is empty
        if(empty(trim($_POST["username"]))){
            $username_err = "Please enter username.";
        }
        else{
            $username = trim($_POST["username"]);
        }

        // Check if password is empty
        if(empty(trim($_POST["password"]))){
            $password_err = "Please enter your password.";
        }
        else{
            $password = trim($_POST["password"]);
        }

        // Validate credentials
        if(empty($username_err) && empty($password_err)){

            // Query to check the username.
            $get_user = sql_get_user_from_username($username);

            // Query result from checking username.
            $user = getAssociativeArray($get_user);

            // If there are no rows, the username is wrong, set error.
            if (count($user) == 0) {
                $username_err = "Username was not found.";
            }
            else {

                // Query to get the user.
                $get_user = sql_get_user_from_username_password($username, $password);

                // Get the user info from the query result.
                $user_info = getAssociativeArray($get_user);

                // If there is no user record, set the password error.
                if (count($user_info) == 0) {
                    $password_err = "Password is incorrect.";
                }
                else {

                    if (!$user_info[0]['status']) {
                        $error_message = 'This user is no longer active, please contact the clinic.';
                    }
                    else {
                            
                        // Store data in session variables
                        $_SESSION["loggedin"] = true;
                        $_SESSION["userID"] = $user_info[0]['userID'];
                        $_SESSION["userRole"] = $user_info[0]['userRole'];                          
                        
                        // Redirect user to welcome page
                        header("location: /");
                    }
                }
            }
        }
    }
?>