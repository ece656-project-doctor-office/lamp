<?php
    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag for has errors.
        $has_errors = FALSE;

        // The array of fields.
        $fields = [];

        // The array of errors.
        $errors = [];

        // Check if user is empty
        if(empty(trim($_POST["userID"]))){
            $has_errors = TRUE;
            $errors['userInfo'] = "Please enter a patient.";
        }
        else{
            $fields['userInfo'] = trim($_POST["userInfo"]);
            $fields['userID'] = trim($_POST["userID"]);
        }

        // If there are no errors, go to patient record.
        if (!$has_errors) {

            // Redirect to patient record.
            Header("Location: /patient/view/" . $fields['userID']);
        }
    }   
?>