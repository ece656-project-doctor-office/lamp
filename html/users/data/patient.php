<div class="form-group">
    <label>Insurance number</label>
    <input
        type="text"
        name="insurancePolicyID"
        class="form-control"
        value="<?php echo $fields['insurancePolicyID'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['insurancePolicyID']; ?></span>
</div>

<?php if ($entity !== 'profile') { ?>
    <div class="form-group">
        <label>OHIP number</label>
        <input
            type="text"
            name="ohipID"
            class="form-control <?php echo (!empty($errors['ohipID'])) ? 'is-invalid' : ''; ?>"
            value="<?php echo $fields['ohipID'] ?? ''; ?>"
        >
        <span class="invalid-feedback"><?php echo $errors['ohipID']; ?></span>
    </div>
<?php } else { ?>
    <input type="hidden" name="ohipID" value="<?php echo $fields['ohipID']; ?>">
<?php } ?>