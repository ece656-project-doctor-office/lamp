<div class="form-group">
    <label>SIN</label>
    <input
        type="text"
        name="sin"
        class="form-control <?php echo (!empty($errors['sin'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['sin'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['sin']; ?></span>
</div>

<div class="form-group">
    <label>Designation</label>
    <input
        type="text"
        name="designation"
        class="form-control <?php echo (!empty($errors['designation'])) ? 'is-invalid' : ''; ?>"
        value="<?php echo $fields['designation'] ?? ''; ?>"
    >
    <span class="invalid-feedback"><?php echo $errors['designation']; ?></span>
</div>