<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="/css/jquery.datepick.css" rel="stylesheet">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 500px; padding: 20px; }
</style>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/jquery.plugin.min.js"></script>
<script src="/js/jquery.datepick.js"></script>

<script>
$(function() {
    $('#popupDatepicker').datepick({dateFormat: 'yyyy-mm-dd', yearRange: 'c-90:c+0'});
});
</script>

<div class="doc-office__schedule--edit doc-office__table">

    <?php include_once 'success_error.php' ?>

    <div class="wrapper">
        <h2>Edit a <?php echo $type; ?> schedule</h2>

        <?php
            $get_doc_info = sql_get_user_first_last_name($value);

            $doc_info = getAssociativeArray($get_doc_info);
        ?>
        <h3><?php echo $doc_info[0]['firstName'] . ' ' . $doc_info[0]['lastName']; ?></h3>

        <?php
            $get_doc_schedule = sql_get_doc_schedule(date('Y-m-d'), $value);

            $doc_schedule = getAssociativeArray($get_doc_schedule);
        ?>

        <?php if (count($doc_schedule) > 0) { ?>
            <table>
                <tr>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
                <?php foreach ($doc_schedule as $ds) { ?>
                    <tr>
                        <td><?php echo date("D M j, Y", strtotime($ds['scheduleDate'])); ?></td>
                        <td><a href="/schedule/delete/doctor/<?php echo $value; ?>/<?php echo $ds['scheduleDate']; ?>">Remove</a>
                    </tr>
                <?php } ?>
            </table>
        <?php } else { ?>
            <p>No doctor schedule has been created yet.</p>
        <?php } ?>

        <h2>Add a <?php echo $type; ?> schedule</h2>
        <h3><?php echo $doc_info[0]['firstName'] . ' ' . $doc_info[0]['lastName']; ?></h3>

        <div class="doc-office__addschedule">
            <form action="/schedule/edit/doctor/<?php echo $value; ?>" method="post">
                <div class="form-group">
                    <label>Schedule date</label>
                    <input
                        type="text"
                        id="popupDatepicker"
                        name="scheduleDate"
                        class="form-control <?php echo (!empty($errors['scheduleDate'])) ? 'is-invalid' : ''; ?>"
                        value="<?php echo $fields['scheduleDate'] ?? ''; ?>"
                    >
                    <span class="invalid-feedback"><?php echo $errors['scheduleDate']; ?></span>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>

            </form>
        </div>
    </div>
</div>