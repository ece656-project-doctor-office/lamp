<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous">
</script>
<script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
    integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
    crossorigin="anonymous">
</script>
<link rel="stylesheet" href="/css/jquery-ui.min.css">
<script type="text/javascript">
    $(function() {
        $("#diseaseInfo").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/autocomplete/choosedisease.php?term="+request.term,
                    dataType: "json",
                    type: "POST",
                    data: {
                        keyword: request.term
                    },
                    success: function(data){
                        response( $.map( data, function( item ) {
                            return {
                                    label: item.diseaseName,
                                    value: item.diseaseName,
                                    id: item.diseaseID
                                }
                            
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $("#diseaseID").val(ui.item.id);  // ui.item.value contains the id of the selected label
            }
        });
    });
</script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 500px; padding: 20px; }
</style>

<?php
    $get_patient = sql_get_user_first_last_name($type);
    $patient = getAssociativeArray($get_patient);
?>
<div class="doc-office__adddiagnosis">

    <?php if (isset($_SESSION['error_message'])) {?>
        <div class="error-message">
            <?php 
                echo $_SESSION['error_message'];
                $_SESSION['error_message'] = NULL;
            ?>
        </div>
    <?php } ?>

    <div class="wrapper">

        <h2>Add diagnosis</h2>
        <h3><?php echo $patient[0]['firstName'] . ' ' . $patient[0]['lastName']; ?></h3>

        <form action="/diagnosis/add/<?php echo $type; ?>" method="post">

            <div class="form-group">
                <label>Description</label>
                <textarea
                    name="diseaseDescription"
                    class="form-control <?php echo (!empty($errors['diseaseDescription'])) ? 'is-invalid' : ''; ?>"
                    rows="8"
                    cols="60"
                ><?php echo $fields['diseaseDescription'] ?? ''; ?></textarea>
                <span class="invalid-feedback"><?php echo $fields['diseaseDescription'] ?? ''; ?></span>
            </div>

            <div class="form-group ui-widget">
                <label>Enter a disease</label>
                <input
                    type="text"
                    id="diseaseInfo"
                    name="diseaseInfo"
                    class="form-control <?php echo (!empty($errors['diseaseInfo'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $fields['diseaseInfo'] ?? ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['diseaseInfo']; ?></span>
            </div>

            <input type="hidden" name="diseaseID" id="diseaseID" value="<?php echo $fields['diseaseID'] ?? ''; ?>" />

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>

        </form>

    </div>
</div>