<?php
    $get_num_of_doctors = sql_get_users_by_role('doctor');
    $num_of_doctors = getAssociativeArray($get_num_of_doctors);

    $get_doctor_age_ranges = sql_get_users_age_ranges('doctor');
    $doctor_age_ranges = getAssociativeArray($get_doctor_age_ranges);

    $get_doctors_by_cities = sql_get_users_by_attribute('doctor', 'city');
    $doctors_by_cities = getAssociativeArray($get_doctors_by_cities);

    $get_doctors_by_gender = sql_get_users_by_attribute('doctor', 'gender');
    $doctors_by_gender = getAssociativeArray($get_doctors_by_gender);
?>

<div class="doc-office__reports doc-office__reports--doctor-stats">
    <h2>Doctor Statistics</h2>
    <h3>Number of doctors: <?php echo $num_of_doctors[0]['numOfUsers']; ?></h3>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Doctor Age Ranges
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Age Range</th>';
                    $html .= '<th>Number of Doctor</th>';
                    $html .= '</tr>';

                    foreach ($doctor_age_ranges[0] as $index => $doctor_age_range) {
                        if ($doctor_age_range > 0) {
                            $html .= '<tr>';
                            $html .= '<td>' . $index . '</td>';
                            $html .= '<td>' . $doctor_age_range . '</td>';
                            $html .= '</tr>';
                        }
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Doctors by City
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>City</th>';
                    $html .= '<th>Number of Dcotors</th>';
                    $html .= '</tr>';

                    foreach ($doctors_by_cities as $doctor_by_city) {
                        $html .= '<tr>';
                        $html .= '<td>' . $doctor_by_city['city'] . '</td>';
                        $html .= '<td>' . $doctor_by_city['numOfUsers'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Doctors by Gender
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Gender</th>';
                    $html .= '<th>Number of Doctors</th>';
                    $html .= '</tr>';

                    foreach ($doctors_by_gender as $doctor_by_gender) {
                        $html .= '<tr>';
                        $html .= '<td>' . $doctor_by_gender['gender'] . '</td>';
                        $html .= '<td>' . $doctor_by_gender['numOfUsers'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>
</div>