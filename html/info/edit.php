<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="/css/jquery.datepick.css" rel="stylesheet">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 700px; padding: 20px; }
</style>

<div class="doc-office__editinfo">

    <?php if (isset($success_message)) {?>
        <div class="success-message">
            <?php echo $success_message; ?>
        </div>
    <?php } ?>

    <?php if (isset($error_message)) {?>
        <div class="error-message">
            <?php echo $error_message; ?>
        </div>
    <?php } ?>

    <div class="wrapper">
        <h2>Edit office info</h2>
        <form action="/info/edit" method="post">

            <div class="form-group">
                <label>Name</label>
                <input
                    type="text"
                    name="name"
                    class="form-control <?php echo (!empty($errors['name'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $fields['name'] ?? ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['name']; ?></span>
            </div>

            <div class="form-group">
                <label>Address</label>
                <input
                    type="text"
                    name="address"
                    class="form-control <?php echo (!empty($errors['address'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $fields['address'] ?? ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['address']; ?></span>
            </div>

            <div class="form-group">
                <label>City</label>
                <input
                    type="text"
                    name="city"
                    class="form-control <?php echo (!empty($errors['city'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $fields['city'] ?? ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['city']; ?></span>
            </div>

            <div class="form-group">
                <label>Province</label>
                <select name="province" class="form-control <?php echo (!empty($errors['province'])) ? 'is-invalid' : ''; ?>">
                    <option value="">-- Select --</option>
                    <?php foreach ($provinces as $prov) { ?>
                        <option value="<?php echo $prov['abbr']; ?>"<?php if (isset($fields['province']) && $fields['province'] == $prov['abbr']) { echo 'selected'; } ?>>
                            <?php echo $prov['fullName']; ?>
                        </option>
                    <?php } ?>
                </select>
                <span class="invalid-feedback"><?php echo $errors['province'] ?? ''; ?></span>
            </div>

            <div class="form-group">
                <label>Postal code</label>
                <input
                    type="text"
                    name="postalCode"
                    class="form-control <?php echo (!empty($errors['postalCode'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $fields['postalCode'] ?? ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['postalCode']; ?></span>
            </div>

            <div class="form-group">
                <label>Phone</label>
                <input
                    type="text"
                    name="phone"
                    class="form-control <?php echo (!empty($errors['phone'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $fields['phone'] ?? ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['phone']; ?></span>
            </div>

            <div class="form-group">
                <label>Fax</label>
                <input
                    type="text"
                    name="fax"
                    class="form-control <?php echo (!empty($errors['fax'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $fields['fax'] ?? ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['fax']; ?></span>
            </div>

            <div class="form-group">
                <label>Email</label>
                <input
                    type="text"
                    name="email"
                    class="form-control <?php echo (!empty($errors['email'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $fields['email'] ?? ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['email']; ?></span>
            </div>

            <div class="form-group">
                <label>Welcome message</label>
                <textarea
                    name="welcomeMessage"
                    class="form-control <?php echo (!empty($errors['welcomeMessage'])) ? 'is-invalid' : ''; ?>"
                    rows="25"
                    cols="60"
                >
                    <?php echo $fields['welcomeMessage'] ?? ''; ?>
                </textarea>
                <span class="invalid-feedback"><?php echo $errors['welcomeMessage']; ?></span>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>

        </form>
    </div>
</div>