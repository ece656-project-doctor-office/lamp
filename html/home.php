<?php
    $patient_flag = FALSE;

    if (isset($_SESSION['userRole']) && $_SESSION['userRole'] == 'patient') {
        $patient_flag = TRUE;

        $get_upcoming_patient_appointments = sql_get_patient_upcoming_appointments($_SESSION['userID'], date('Y-m-d'));
        $upcoming_patient_appointments = getAssociativeArray($get_upcoming_patient_appointments);

        $get_patient_prescriptions = sql_get_all_perscriptions($_SESSION['userID']);
        $patient_prescriptions = getAssociativeArray($get_patient_prescriptions);
    }
?>
<div class="doc-office__home<?php if ($patient_flag) { echo '--patient'; } ?>">
    <div class="doc-office__welcome-message">
        <?php
            $get_welcome_message = sql_get_welcome_message();

            $welcome_message = getAssociativeArray($get_welcome_message);

            echo $welcome_message[0]['welcomeMessage'];
        ?>
    </div>
    <?php if ($patient_flag) { ?>
        <div class="doc-office__patient-info">
            <details class="doc-office__details" open>
                <summary class="doc-office__details--summary">
                    Upcoming Appointments
                </summary>
                <div class="doc-office__details--content">
                    <?php if (count($upcoming_patient_appointments) > 0) { ?>
                        <ul class="appointment">
                            <?php foreach ($upcoming_patient_appointments as $upcoming_patient_appointment) { ?>
                                <li>
                                    <b>
                                        <?php echo date('M j, Y', strtotime($upcoming_patient_appointment['appointmentDate'])); ?>
                                    </b>
                                    <?php echo ' (' . date('g:i a', strtotime($upcoming_patient_appointment['appointmentStartTime'])); ?>
                                    <?php echo ' - ' . date('g:i a', strtotime($upcoming_patient_appointment['appointmentEndTime'])) . ')'; ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <div class="error-message">
                            You have no scheduled upcoming appointments.
                        </div>
                    <?php } ?>
                </div>
            </details>

            <details class="doc-office__details">
                <summary class="doc-office__details--summary">
                    Prescriptions
                </summary>
                <div class="doc-office__details--content">
                    <?php if (count($patient_prescriptions) > 0) { ?>
                        <ul class="prescription">
                            <?php foreach ($patient_prescriptions as $patient_prescription) { ?>
                                <li>
                                    <a href="prescriptions/view/<?php echo $patient_prescription['prescriptionID']; ?>/<?php echo $_SESSION['userID']; ?>">
                                        <?php
                                            if ($patient_prescription['drugCode']) {
                                                echo ucfirst(strtolower($patient_prescription['drugName']));
                                            }
                                            else {
                                                echo $patient_prescription['prescription'];
                                            }
                                        ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <div class="error-message">
                            You have no prescriptions.
                        </div>
                    <?php } ?>
                </div>
            </details>
        </div>
    <?php } ?>
</div>