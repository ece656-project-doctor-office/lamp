<div class="doc-office__reports">
    <h2>General Reports</h2>
    <a href="/reports/stats">Statistics</a>
    <h2>Patient Reports</h2>
    <a href="/reports/patient-stats">Statistics</a><br />
    <h2>Appointment Reports</h2>
    <a href="/reports/appointment-stats">Statistics</a><br />
    <h2>Drug Reports</h2>
    <a href="/reports/drug-stats">Statistics</a><br />
    <h2>Prescription Reports</h2>
    <a href="/reports/prescription-stats">Statistics</a>
    <a href="/reports/drug-companies">Drug Companies</a><br />
    <h2>Disease Reports</h2>
    <a href="/reports/disease-stats">Statistics</a>
    <h2>Doctor Reports</h2>
    <a href="/reports/doctor-stats">Statistics</a>
    <h2>Manager Reports</h2>
    <a href="/reports/manager-stats">Statistics</a>
</div>