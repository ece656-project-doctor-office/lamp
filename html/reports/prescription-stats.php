<?php
    $get_num_of_patients = sql_get_users_by_role('patient');
    $num_of_patients = getAssociativeArray($get_num_of_patients);

    $get_num_of_prescriptions = sql_get_num_of_prescriptions();
    $num_of_prescriptions = getAssociativeArray($get_num_of_prescriptions);

    $get_avg_num_prescriptions = sql_get_avg_num_prescriptions();
    $avg_num_prescriptions = getAssociativeArray($get_avg_num_prescriptions);

    $get_patients_with_max_prescriptions = sql_get_patients_with_max_or_min_prescriptions('max');
    $patients_with_max_prescriptions = getAssociativeArray($get_patients_with_max_prescriptions);

    $get_patients_with_min_prescriptions = sql_get_patients_with_max_or_min_prescriptions('min');
    $patients_with_min_prescriptions = getAssociativeArray($get_patients_with_min_prescriptions);
?>

<div class="doc-office__reports doc-office__reports--patient-prescriptions">
    <h2>Prescription Statistics</h2>
    <h3>Number of patients: <?php echo $num_of_patients[0]['numOfUsers']; ?></h3>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Details
        </summary>
        <div class="doc-office__details--content">
            <b>Number of prescriptions:</b> <?php echo $num_of_prescriptions[0]['numOfPrescriptions']; ?><br />
            <b>Average number of prescriptions per patient:</b> <?php echo $avg_num_prescriptions[0]['avgNumOfPrescriptions']; ?><br />
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Patient(s) with most prescriptions
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Patient</th>';
                    $html .= '<th>Number of Prescriptions</th>';
                    $html .= '<th>Age</th>';
                    $html .= '</tr>';

                    foreach ($patients_with_max_prescriptions as $patient_with_max_prescriptions) {
                        $html .= '<tr>';
                        $html .= '<td>';
                        $html .= '<a href="/patient/view/' . $patient_with_max_prescriptions['patientID'] . '">';
                        $html .= $patient_with_max_prescriptions['lastName'] . ', ' . $patient_with_max_prescriptions['firstName'];
                        $html .= '</a>';
                        $html .= '</td>';
                        $html .= '<td>' . $patient_with_max_prescriptions['numOfPrescriptions'] . '</td>';
                        $html .= '<td>' . $patient_with_max_prescriptions['age'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Patient(s) with least prescriptions
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Patient</th>';
                    $html .= '<th>Number of Prescriptions</th>';
                    $html .= '<th>Age</th>';
                    $html .= '</tr>';

                    foreach ($patients_with_min_prescriptions as $patient_with_min_prescriptions) {
                        $html .= '<tr>';
                        $html .= '<td>';
                        $html .= '<a href="/patient/view/' . $patient_with_max_prescriptions['patientID'] . '">';
                        $html .= $patient_with_min_prescriptions['lastName'] . ', ' . $patient_with_min_prescriptions['firstName'];
                        $html .= '</a>';
                        $html .= '</td>';
                        $html .= '<td>' . $patient_with_min_prescriptions['numOfPrescriptions'] . '</td>';
                        $html .= '<td>' . $patient_with_min_prescriptions['age'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>
</div>