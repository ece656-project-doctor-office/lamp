<?php
    $patientID = $type;

    $get_patient_info = sql_get_user_first_last_name($patientID);
    $patient_info = getAssociativeArray($get_patient_info);

    $get_diagnosis = sql_get_all_diagnosis($patientID);
    $diagnosis = getAssociativeArray($get_diagnosis);

    if (count($diagnosis) == 0) {
        $_SESSION['error_message'] = 'There are no diagnosis for this patient, add one before adding a perscription.';
        Header("Location: /patient/view/" . $patientID);
    }

    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag if form has errors.
        $has_errors = FALSE;

        // Check for the require drug checkboxes.
        if (isset($_POST["requireDrug"])) {

            // Check if require drug is empty.
            if(empty(trim($_POST["requireDrug"]))){
                $has_errors = TRUE;
                $errors['requireDrug'] = "Please select if drug is required.";
            }
            else{
                $fields['requireDrug'] = trim($_POST["requireDrug"]);
            }
        }
        else {
            $errors['requireDrug'] = "Please select if drug is required.";
        }

        // Check if diagnosis is empty.
        if(empty(trim($_POST["diagnosisID"]))){
            $has_errors = TRUE;
            $errors['diagnosisID'] = "Please select the diagnosis.";
        }
        else{
            $fields['diagnosisID'] = trim($_POST["diagnosisID"]);
        }

        if (isset($_POST["requireDrug"]) && $_POST["requireDrug"] == 'require') {

            // Check if drug is empty.
            if(empty(trim($_POST["drugCode"]))){
                $has_errors = TRUE;
                $errors['drugInfo'] = "Please enter the drug.";
            }
            else{
                $fields['drugInfo'] = trim($_POST["drugInfo"]);
            }

            // Get tde drugCode field, no error checking required.
            $fields['drugCode'] = $_POST['drugCode'];

            // Check if dosage is empty.
            if(empty(trim($_POST["dosage"]))){
                $has_errors = TRUE;
                $errors['dosage'] = "Please enter the dosage.";
            }
            else{
                $fields['dosage'] = trim($_POST["dosage"]);
            }

            // Get instructions and prescription which requires no error checking.
            $fields['instructions'] = trim($_POST['instructions']);
            $fields['prescription'] = trim($_POST['prescription']);
        }
        else {

            // Just get the fields, no error checking when drug not required.
            $fields['drugInfo'] = trim($_POST["drugInfo"]);
            $fields['drugCode'] = $_POST['drugCode'];
            $fields['dosage'] = "Please enter the dosage.";
            $fields['instructions'] = trim($_POST['instructions']);

            // Check if prescription is empty.
            if(empty(trim($_POST["prescription"]))){
                $has_errors = TRUE;
                $errors['prescription'] = "Please enter the prescription.";
            }
            else{
                $fields['prescription'] = trim($_POST["prescription"]);
            }
        }

        // If there are no errors insert the prescription.
        if (!$has_errors) {

            // Get the sql to insert the prescription.
            $insert_prescription = sql_insert_prescription($fields, $_SESSION['userID']);

            // Perform the insertion and if successful, check if
            // we have to insert the prescription drugs.
            // If the insertion failed, set error message.
            if ($conn->query($insert_prescription) === TRUE) {

                // If a drug is required, then get the last inserted id,
                // and insert the prescription drugs.
                if ($fields['requireDrug'] == 'require') {

                    // Set the prescription id from the last inserted id.
                    $fields['prescriptionID'] = $conn->insert_id;

                    // Get the sql to insert the prescription drug.
                    $insert_prescription_drug = sql_insert_prescription_drug($fields);

                    // Insert the prescription drug.
                    $insert_prescription_drug_flag = insertQuery($insert_prescription_drug);

                    // Set the success or error message based on insert flag.
                    if ($insert_prescription_drug_flag) {

                        // Set the success message.
                        $_SESSION['success_message'] = 'The prescription has been successfully added.';

                        // Redirect back to patient view page.
                        Header("Location: /patient/view/" . $patientID);
                    }
                    else {

                        // Error message that insertion of prescription drug failed.
                        $local_error_message = 'There was an error adding the prescription drug';
                    }
                }
                else {

                    // Set the success message.
                    $_SESSION['success_message'] = 'The prescription has been successfully added.';

                    // Redirect back to patient view page.
                    Header("Location: /patient/view/" . $patientID);
                }
            }
            else {

                // Error message that insertion into prescription failed.
                $local_error_message = 'There was an error adding the prescription.';
            }
        }
    }
?>