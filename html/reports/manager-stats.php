<?php
    $get_num_of_managers = sql_get_users_by_role('manager');
    $num_of_managers = getAssociativeArray($get_num_of_managers);

    $get_manager_age_ranges = sql_get_users_age_ranges('manager');
    $manager_age_ranges = getAssociativeArray($get_manager_age_ranges);

    $get_managers_by_cities = sql_get_users_by_attribute('manager', 'city');
    $mangers_by_cities = getAssociativeArray($get_managers_by_cities);

    $get_managers_by_gender = sql_get_users_by_attribute('manager', 'gender');
    $managers_by_gender = getAssociativeArray($get_managers_by_gender);
?>

<div class="doc-office__reports doc-office__reports--doctor-stats">
    <h2>Manager Statistics</h2>
    <h3>Number of managers: <?php echo $num_of_managers[0]['numOfUsers']; ?></h3>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Manager Age Ranges
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Age Range</th>';
                    $html .= '<th>Number of Doctor</th>';
                    $html .= '</tr>';

                    foreach ($manager_age_ranges[0] as $index => $manager_age_range) {
                        if ($manager_age_range > 0) {
                            $html .= '<tr>';
                            $html .= '<td>' . $index . '</td>';
                            $html .= '<td>' . $manager_age_range . '</td>';
                            $html .= '</tr>';
                        }
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Managers by City
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>City</th>';
                    $html .= '<th>Number of Managers</th>';
                    $html .= '</tr>';

                    foreach ($mangers_by_cities as $manager_by_city) {
                        $html .= '<tr>';
                        $html .= '<td>' . $manager_by_city['city'] . '</td>';
                        $html .= '<td>' . $manager_by_city['numOfUsers'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Managers by Gender
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Gender</th>';
                    $html .= '<th>Number of Managers</th>';
                    $html .= '</tr>';

                    foreach ($managers_by_gender as $manager_by_gender) {
                        $html .= '<tr>';
                        $html .= '<td>' . $manager_by_gender['gender'] . '</td>';
                        $html .= '<td>' . $manager_by_gender['numOfUsers'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>
</div>