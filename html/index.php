<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    session_start();
    include_once 'connection.php';
    include_once 'code/sql_statements.php';
    include_once 'code/functions.php';
    include_once 'global.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/css/styles.css">
    </head>
    <body>
        <div class="doc-office">
            <?php include_once 'header.php'; ?>
            <?php include_once 'menu.php'; ?>
            <?php include_once 'loggedin.php'; ?>
            <?php include_once 'content.php'; ?>
            <?php include_once 'footer.php'; ?>
        </div>
    </body>
</html>

