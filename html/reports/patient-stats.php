<?php
    $get_num_of_patients = sql_get_users_by_role('patient');
    $num_of_patients = getAssociativeArray($get_num_of_patients);

    $get_patient_age_ranges = sql_get_users_age_ranges('patient');
    $patient_age_ranges = getAssociativeArray($get_patient_age_ranges);

    $get_patients_by_cities = sql_get_users_by_attribute('patient', 'city');
    $patients_by_cities = getAssociativeArray($get_patients_by_cities);

    $get_patients_by_gender = sql_get_users_by_attribute('patient', 'gender');
    $patients_by_gender = getAssociativeArray($get_patients_by_gender);
?>

<div class="doc-office__reports doc-office__reports--patient-stats">
    <h2>Patient Statistics</h2>
    <h3>Number of patients: <?php echo $num_of_patients[0]['numOfUsers']; ?></h3>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Patient Age Ranges
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Age Range</th>';
                    $html .= '<th>Number of Patients</th>';
                    $html .= '</tr>';

                    foreach ($patient_age_ranges[0] as $index => $patient_age_range) {
                        $html .= '<tr>';
                        $html .= '<td>' . $index . '</td>';
                        $html .= '<td>' . $patient_age_range . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Patients by City
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>City</th>';
                    $html .= '<th>Number of Patients</th>';
                    $html .= '</tr>';

                    foreach ($patients_by_cities as $patient_by_city) {
                        $html .= '<tr>';
                        $html .= '<td>' . $patient_by_city['city'] . '</td>';
                        $html .= '<td>' . $patient_by_city['numOfUsers'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Patients by Gender
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Gender</th>';
                    $html .= '<th>Number of Patients</th>';
                    $html .= '</tr>';

                    foreach ($patients_by_gender as $patient_by_gender) {
                        $html .= '<tr>';
                        $html .= '<td>' . $patient_by_gender['gender'] . '</td>';
                        $html .= '<td>' . $patient_by_gender['numOfUsers'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>
</div>