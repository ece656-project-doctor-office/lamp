<?php if (isset($_SESSION['success_message'])) {?>
    <div class="success-message">
        <?php 
            echo $_SESSION['success_message']; 
            $_SESSION['success_message'] = NULL;
        ?>
    </div>
<?php } ?>

<?php if (isset($_SESSION['error_message'])) {?>
    <div class="error-message">
        <?php 
            echo $_SESSION['error_message'];
            $_SESSION['error_message'] = NULL;
        ?>
    </div>
<?php } ?>