<?php

    /**
     * SQL to get number of addresses.
     *
     * @return string
     *   The sql to get the number of addresses.
     */
    function sql_get_num_of_addresses(): string {

        // SQL to get the number of addresses.
        $get_num_of_addresses = '
            SELECT
                count(id) as numOfAddresses
            FROM
                RegionOfWaterlooAddress
        ';

        return $get_num_of_addresses;
    }

    /**
     * SQL to get an address.
     *
     * @param int $id
     *   The id of the address.
     *
     * @return string
     *   The sql to get an address.
     */
    function sql_get_address(int $id): string {

        // SQL to get an address.
        $get_address = '
            SELECT 
                *
            FROM
                RegionOfWaterlooAddress
            WHERE
                id = ' . $id;

        return $get_address;
    }

    /**
     * SQL to get the number of first names.
     *
     * @param string $gender
     *   The gender of the first name.
     *
     * @return string
     *   The sql to get the number of first names.
     */
    function sql_get_num_of_first_names(string $gender): string {

        // Get the table name for first names based on gender.
        if ($gender == 'M') {
            $table = 'FirstNamesMale';
        }
        else {
            $table = 'FirstNamesFemale';
        }

        // SQL to get the number of first names.
        $get_num_of_first_names = '
            SELECT
                count(id) as numOfFirstNames
            FROM
                '. $table;

        return $get_num_of_first_names;
    }

    /**
     * SQL to get a first name.
     *
     * @param string $gender
     *   The gender of the first name.
     * @param int $id
     *   The id of the first name.
     *
     * @return string
     *   The sql to get a first name.
     */
    function sql_get_first_name(string $gender, int $id): string {

        // Get the table name for first names based on gender.
        if ($gender == 'M') {
            $table = 'FirstNamesMale';
        }
        else {
            $table = 'FirstNamesFemale';
        }

        // SQL to get a first name.
        $get_first_name = '
            SELECT
                firstName
            FROM
                '. $table . '
            WHERE
                id = ' . $id;

        return $get_first_name;
    }

    /**
     * SQL to get number of last names.
     *
     * @return string
     *   The sql to get the number of last names.
     */
    function sql_get_num_of_last_names(): string {

        // SQL to get the number of last names.
        $get_num_of_last_names = '
            SELECT
                count(id) AS numOfLastNames
            FROM
                Surnames
        ';

        return $get_num_of_last_names;
    }

    /**
     * SQL to get a last name.
     *
     * @param int $id
     *   The id of the last name.
     *
     * @return string
     *   The sql to get a last name.
     */
    function sql_get_last_name(int $id): string {

        // SQL to get a last name.
        $get_last_name = '
            SELECT
                surname
            FROM
                Surnames
            WHERE
                id = ' . $id;

        return $get_last_name;
    }

    /**
     * SQL to get all the disease IDs.
     *
     * @return string
     *   The sql to get all disease IDs.
     */
    function sql_get_disease_ids(): string {

        // SQL to get all disease IDs.
        $get_disease_ids = '
            SELECT diseaseID FROM Disease
        ';

        return $get_disease_ids;
    }

    /**
     * SQL to get all the doctor ids.
     *
     * @return string
     *   The sql to get all the doctor ids.
     */
    function sql_get_doctor_ids(): string {

        // SQL to get all the doctor ids.
        $get_doctor_ids = '
            SELECT
                userID
            FROM
                Users
                INNER JOIN UserRoles USING (userRoleID)
            WHERE
                userRole = "doctor" AND
                status = 1
        ';

        return $get_doctor_ids;
    }

    /**
     * SQL to get all drug codes.
     *
     * @return string
     *   The sql to get all drug codes.
     */
    function sql_get_drug_codes() {

        // SQL to get all drug codes.
        $get_drug_codes = '
            SELECT
                drugCode
            FROM
                Drugs
        ';

        return $get_drug_codes;
    }
?>