<?php
    $get_drug = sql_get_drug($type);
    $drug = getAssociativeArray($get_drug);
    $drug = $drug[0];

    $get_drug_company = sql_get_drug_company($type);
    $drug_company = getAssociativeArray($get_drug_company);
    $drug_company = $drug_company;
?>

<div class="doc-office__drugs--view">
    <h2>View Drug Information</h2>

    <h3><?php echo $drug['drugName']; ?></h3>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Details
        </summary>
        <div class="doc-office__details--content">

            <?php if ($drug['productCategorization']) { ?>
                <b>Product Categorization:</b> <?php echo $drug['productCategorization']; ?><br />
            <?php } ?>

            <?php if ($drug['class']) { ?>
                <b>Class:</b> <?php echo $drug['class']; ?><br />
            <?php } ?>

            <?php if ($drug['DIN']) { ?>
                <b>DIN:</b> <?php echo $drug['DIN']; ?><br />
            <?php } ?>

            <?php if ($drug['descriptor']) { ?>
                <b>Descriptor:</b> <?php echo $drug['descriptor']; ?><br />
            <?php } ?>

            <?php if ($drug['pediatricFlag']) { ?>
                <b>Kid safe:</b> <?php echo $drug['pediatricFlag']; ?><br />
            <?php } ?>

            <?php if ($drug['status']) { ?>
                <b>Status:</b> <?php echo ucfirst(strtolower($drug['status'])); ?> <br />
            <?php } ?>

            <?php if ($drug['historyDate']) { ?>
                <b>Date:</b> <?php echo date('F j, Y', strtotime($drug['historyDate'])) ?> <br />
            <?php } ?>

        </div>
    </details>

    <?php if (count($drug_company) > 0) { ?>
        <?php $drug_company = $drug_company[0]; ?>
        <details class="doc-office__details">
            <summary class="doc-office__details--summary">
                Company Info
            </summary>
            <div class="doc-office__details--content">
                <b><?php echo ucwords(strtolower($drug_company['companyName'])); ?></b><br />
                <?php if ($drug_company['suiteNumber']) { echo $drug_company['suiteNumber'] . ' '; } ?><?php echo ucwords(strtolower($drug_company['streetName'])); ?><br />
                <?php echo ucwords(strtolower($drug_company['cityName'])); ?>, <?php echo ucwords(strtolower($drug_company['province'])); ?><br />
                <?php echo ucwords(strtolower($drug_company['country'])); ?><br />
                <?php echo ucwords(strtolower($drug_company['postalCode'])); ?>
            </div>
        </details>
    <?php } ?>
</div>