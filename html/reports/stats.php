<?php
    $get_num_of_patients = sql_get_users_by_role('patient');
    $num_of_patients = getAssociativeArray($get_num_of_patients);

    $get_num_of_doctors = sql_get_users_by_role('doctor');
    $num_of_doctors = getAssociativeArray($get_num_of_doctors);

    $get_num_of_managers = sql_get_users_by_role('manager');
    $num_of_mangers = getAssociativeArray($get_num_of_managers);

    $get_num_of_users = sql_get_users_by_role(NULL);
    $num_of_users = getAssociativeArray($get_num_of_users);

    $get_users_by_gender = sql_get_users_by_attribute(NULL, 'gender');
    $users_by_gender = getAssociativeArray($get_users_by_gender);
?>
<div class="doc-office__reports--stats">
    <h2>General Statistics</h2>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Number of Users
        </summary>
        <div class="doc-office__details--content">
            <b>Patients:</b> <?php echo $num_of_patients[0]['numOfUsers']; ?><br />
            <b>Doctors:</b> <?php echo $num_of_doctors[0]['numOfUsers']; ?><br />
            <b>Managers:</b> <?php echo $num_of_mangers[0]['numOfUsers']; ?><br />
            <b>Total:</b> <?php echo $num_of_users[0]['numOfUsers']; ?><br />
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Users by Gender
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Gender</th>';
                    $html .= '<th>Number of Users</th>';
                    $html .= '</tr>';

                    foreach ($users_by_gender as $users_by_gender) {
                        $html .= '<tr>';
                        $html .= '<td>' . $users_by_gender['gender'] . '</td>';
                        $html .= '<td>' . $users_by_gender['numOfUsers'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>
</div>