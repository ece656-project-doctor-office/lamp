<?php
    $get_num_drug_companies = sql_get_num_drug_companies();
    $num_drug_companies = getAssociativeArray($get_num_drug_companies);

    $get_avg_drugs_per_drug_company = sql_get_avg_drugs_per_drug_company();
    $avg_drugs_per_drug_company = getAssociativeArray($get_avg_drugs_per_drug_company);

    $get_max_drug_companies = sql_get_max_min_drug_companies('max');
    $max_drug_companies = getAssociativeArray($get_max_drug_companies);

    $get_min_drug_companies = sql_get_max_min_drug_companies('min');
    $min_drug_companies = getAssociativeArray($get_min_drug_companies);
?>
<div class="doc-office__reports doc-office__reports--drug-companies">
    <h2>Drug Company Report</h2>
    <h3></h3>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Details
        </summary>
        <div class="doc-office__details--content">
            <b>Number of drug companies:</b> <?php echo $num_drug_companies[0]['numOfDrugCompanies']; ?><br />
            <b>Averge number of drugs per drug company:</b> <?php echo $avg_drugs_per_drug_company[0]['avgDrugs']; ?>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Drug company with the most drugs sold
        </summary>
        <div class="doc-office__details--content">

            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Num of Drugs</th>';
                    $html .= '<th>Company</th>';
                    $html .= '<th>Address</th>';
                    $html .= '<th>City</th>';
                    $html .= '<th>Province/State</th>';
                    $html .= '<th>Country</th>';
                    $html .= '<th>Postal/Zip Code</th>';
                    $html .= '</tr>';

                    foreach ($max_drug_companies as $max_drug_company) {
                        $html .= '<tr>';
                        $html .= '<td>' . $max_drug_company['numOfDrugs'] . '</td>';
                        $html .= '<td>' . ucwords(strtolower($max_drug_company['companyName'])) . '</td>';
                        $html .= '<td>';
                        $html .= $max_drug_company['suiteNumber'] ? $max_drug_company['suiteNumber'] . ' ' : '';
                        $html .= ucwords(strtolower($max_drug_company['streetName']));
                        $html .= '</td>';
                        $html .= '<td>' . ucwords(strtolower($max_drug_company['cityName'])) . '</td>';
                        $html .= '<td>' . ucwords(strtolower($max_drug_company['province'])) . '</td>';
                        $html .= '<td>' . ucwords(strtolower($max_drug_company['country'])) . '</td>';
                        $html .= '<td>' . $max_drug_company['postalCode'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>

    <details class="doc-office__details" open>
        <summary class="doc-office__details--summary">
            Drug company with the least drugs sold
        </summary>
        <div class="doc-office__details--content">
            <div class="doc-office__table">
                <?php
                    $html = '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Num of Drugs</th>';
                    $html .= '<th>Company</th>';
                    $html .= '<th>Address</th>';
                    $html .= '<th>City</th>';
                    $html .= '<th>Province/State</th>';
                    $html .= '<th>Country</th>';
                    $html .= '<th>Postal/Zip Code</th>';
                    $html .= '</tr>';

                    foreach ($min_drug_companies as $min_drug_company) {
                        $html .= '<tr>';
                        $html .= '<td>' . $min_drug_company['numOfDrugs'] . '</td>';
                        $html .= '<td>' . ucwords(strtolower($min_drug_company['companyName'])) . '</td>';
                        $html .= '<td>';
                        $html .= $min_drug_company['suiteNumber'] ? $min_drug_company['suiteNumber'] . ' ' : '';
                        $html .= ucwords(strtolower($min_drug_company['streetName']));
                        $html .= '</td>';
                        $html .= '<td>' . ucwords(strtolower($min_drug_company['cityName'])) . '</td>';
                        $html .= '<td>' . ucwords(strtolower($min_drug_company['province'])) . '</td>';
                        $html .= '<td>' . ucwords(strtolower($min_drug_company['country'])) . '</td>';
                        $html .= '<td>' . $min_drug_company['postalCode'] . '</td>';
                        $html .= '</tr>';
                    }

                    $html .= '</table>';

                    echo $html;
                ?>
            </div>
        </div>
    </details>
</div>