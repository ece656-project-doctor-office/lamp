<?php

    // Processing form data when form is submitted.
    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Flag for has errors.
        $has_errors = FALSE;

        // The array of fields.
        $fields = [];

        // The array of errors.
        $errors = [];

        // Check if drug id is empty
        if(empty(trim($_POST["drugCode"]))){
            $has_errors = TRUE;
            $errors['drugInfo'] = "Please enter a drug.";
        }
        else{
            $fields['drugInfo'] = trim($_POST["drugInfo"]);
            $fields['drugCode'] = trim($_POST["drugCode"]);
        }

        // If there is not form error, redirect to view drug.
        if(!$has_errors) {

            Header("Location: /drugs/view/" . $fields['drugCode']);
        }
    }
?>