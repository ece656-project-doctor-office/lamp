<style>

    @media only screen and (min-width: 320px) {
        .doc-office__hero--image {
            background-image: url("/images/header/doc_office_xsmall.png");
        }
    }
    @media only screen and (min-width: 481px) {
        .doc-office__hero--image {
            background-image: url("/images/header/doc_office_small.png");
        }
    }
    @media only screen and (min-width: 769px) {
        .doc-office__hero--image {
            background-image: url("/images/header/doc_office_med.png");
        }
    }
    @media only screen and (min-width: 1025px) {
        .doc-office__hero--image {
            background-image: url("/images/header/doc_office_large.png");
        }
    }
    @media only screen and (min-width: 1201px) {
        .doc-office__hero--image {
            background-image: url("/images/header/doc_office_xlarge.png");
        }
    }
</style>

<div class="doc-office__header">
    <div class="doc-office__hero--image">
        <div class="doc-office__name">
            <?php
                $get_office_name = sql_get_office_name();
                $office_name = getAssociativeArray($get_office_name);

                echo $office_name[0]['name'];
            ?>
        </div>
    </div>
</div>
