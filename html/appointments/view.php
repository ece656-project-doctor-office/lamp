<?php
    if (isset($_POST['dateSelected'])) {
        $date = $_POST['dateSelected'];
    }
    elseif (!isset($type)) {
        $date = date('Y-m-d');
    }
    else {
        $date = $type;
    }
?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="/css/jquery.datepick.css" rel="stylesheet">
<style>
    body{ font: 14px sans-serif; }
    .wrapper{ width: 500px; padding: 20px; }
</style>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/jquery.plugin.min.js"></script>
<script src="/js/jquery.datepick.js"></script>

<script>
$(function() {
	$('#popupDatepicker').datepick({dateFormat: 'yyyy-mm-dd', yearRange: 'c-90:c+0'});
});
</script>

<div class="doc-office__appointments--view">
    <div class="wrapper">
        <h2>View appointments</h2>
        <h5>(<?php echo date ('l F, j, Y', strtotime($date)); ?>)</h5>

        <form action="/appointments/view" method="post">
            <div class="form-group">
                <input
                    type="text"
                    id="popupDatepicker"
                    name="dateSelected"
                    class="form-control <?php echo (!empty($errors['dateSelected'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $date ?? ''; ?>"
                >
                <span class="invalid-feedback"><?php echo $errors['dateSelected']; ?></span>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
        <div class="doc-office__appointments-list doc-office__table">
            <?php
                $get_doctor_appointments = sql_get_doctor_appointments_by_day($date, $_SESSION['userID']);
                $doctor_appointments = getAssociativeArray($get_doctor_appointments);

                $html = '';

                if (count($doctor_appointments) > 0) {
                    $html .= '<table>';

                    $html .= '<tr>';
                    $html .= '<th>Time</th>';
                    $html .= '<th>Patient</th>';
                    $html .= '</tr>';

                    foreach ($doctor_appointments as $doctor_appointment) {
                        $html .= '<tr>';

                        $html .= '<td>';
                        $html .= date('g:i a', strtotime($doctor_appointment['appointmentStartTime']));
                        $html .= '-';
                        $html .= date('g:i a', strtotime($doctor_appointment['appointmentEndTime']));
                        $html .= '</td>';

                        $html .= '<td>';
                        $html .= '<a href="/patient/view/' . $doctor_appointment['patientID'] . '">';
                        $html .= $doctor_appointment['firstName'] . ' ' . $doctor_appointment['lastName'];
                        $html .= '</a>';
                        $html .= '</td>';

                        $html .= '</tr>';
                    }

                    $html .= '</table>';
                }
                else {
                    $html .= '<div class="error-message">You do not have any appointments scheduled for this day.</div>';
                }

                echo $html;
            ?>
        </div>
    </div>
</div>