<?php

    define("RAND_START_YEAR", 2015);
    define("RAND_END_YEAR", 2023);

    /**
     * Function to get the number of addresses.
     *
     * @return int
     *   The number of addresses.
     */
    function get_num_of_addresses(): int {

        // Get the number of addresses from the database.
        $get_num_of_addresses = sql_get_num_of_addresses();

        // Return just the number of addresses.
        return getAssociativeArray($get_num_of_addresses)[0]['numOfAddresses'];
    }

    /**
     * Function to get a random address.
     *
     * @param int $num_of_addresses
     *   The number of addresses in the table.
     *
     * @return array
     *   An array of address fields.
     */
    function get_rand_address($num_of_addresses): array {

        // Get a random address.
        $rand_address = rand(1, $num_of_addresses);

        // Get the address from the database.
        $get_address = sql_get_address($rand_address);
        $address = getAssociativeArray($get_address);

        // Return just the address.
        return $address[0];
    }

    /**
     * Function to get a random gender.
     *
     * @return string
     *   Return Male (M) or Female (F).
     */
    function get_rand_gender(): string {

        // Get a random number between 1 and 2.
        $rand_gender = rand(1, 2);

        // Reurn a gender based on random number.
        if ($rand_gender == 1) {
            return 'M';
        }
        else {
            return 'F';
        }
    }

    /**
     * Function to get the number of first names.
     *
     * @param string $gender
     *   The gender of the first name.
     *
     * @return int
     *   The number of first names.
     */
    function get_num_of_first_names(string $gender): int {

        // Get the number of first names from the database.
        $get_num_of_first_names = sql_get_num_of_first_names($gender);

        // Return the actual number of first names.
        return getAssociativeArray($get_num_of_first_names)[0]['numOfFirstNames'];
    }

    /**
     * Function to get a first name.
     *
     * @param string $gender
     *   The gender of the first name (so we know what table to use).
     * @param int it
     *   The id of the first name to get.
     *
     * @return string
     *   The actual first name.
     */
    function get_first_name(string $gender, int $id): string {

        // Get the first name from the database.
        $get_first_name = sql_get_first_name($gender, $id);
        $first_name = getAssociativeArray($get_first_name);

        // Return the actual first name.
        return getAssociativeArray($get_first_name)[0]['firstName'];
    }

    /**
     * Function to get the number of last names.
     *
     * @return string
     *   The number of last names.
     */
    function get_num_of_last_names(): string {

        // Get the number of last names from the database.
        $get_num_of_last_names = sql_get_num_of_last_names();

        // Return the actual number of last names.
        return getAssociativeArray($get_num_of_last_names)[0]['numOfLastNames'];
    }

    /**
     * Function to get a last name.
     *
     * @param int $id
     *   The id of the last name.
     *
     * @return string
     *   The last name.
     */
    function get_last_name(int $id): string {

        // Get the last name from the database.
        $get_last_name = sql_get_last_name($id);

        // Return the actual last name.
        return getAssociativeArray($get_last_name)[0]['surname'];
    }

    /**
     * Function to get a date of birth.
     *
     * @param string $userRole
     *   The user role, so that we can restrict the range of dates.
     *
     * @return string
     *   A random date of birth.
     */
    function get_date_of_birth(string $userRole): string {


        // If patient, use start date up to 100 years old.
        // If not, limit to 70 years old.
        if ($userRole == 'patient') {
            $start = strtotime("1 January 1921");
        }
        else {
            $start = strtotime("1 January 1951");
        }

        // If patient, get end date up to this year.
        // If not limit to 22 years old minimum.
        if ($userRole == 'patient') {
            $end = strtotime("31 December 2021");
        }
        else {
            $end = strtotime("31 December 2000");
        }

        // Get a random date between start and end.
        $timestamp = mt_rand($start, $end);

        // Return the date of birth.
        return date("Y-m-d", $timestamp);
    }

    /**
     * Function to get a phone number.
     *
     * @return string
     *   A random phone number.
     */
    function get_phone_number(): string {

        // The area codes in Ontario.
        $area_codes = [
            '226',
            '249',
            '289',
            '343',
            '365',
            '416',
            '437',
            '519',
            '548',
            '613',
            '647',
            '705',
            '807',
            '905',
        ];

        // Get a random area code.
        $phone = $area_codes[rand(0, count($area_codes) - 1)];

        // Get 7 random digits.
        for($i = 0; $i < 7; $i++) {
            $phone .= rand(1, 9);
        }

        return $phone;
    }

    /**
     * Function to get a random email.
     *
     * @param string $first_name
     *   The user first name.
     * @param string $last_name
     *   The user last name.
     *
     * @return string
     *   A random email address.
     */
    function get_email(string $first_name, string $last_name): string {

        // List of domains to use.
        $domains = [
            'gmail.com',
            'sympatico.ca',
            'hotmail.com',
            'outlook.com',
            'bell.net',
            'rogers.com',
            'telus.ca',
            'aol.com',
            'webmail.com',
            'microsoft.com',
            'fido.ca',
        ];

        // Get the first part of the email address which
        // is the users first and last name.
        $email = trim($first_name . $last_name);

        // Remove any spaces.
        $email = str_replace(' ', '', $email);

        // Add the at simple.
        $email .= '@';

        // Add a random domain.
        $email .= $domains[rand(1, count($domains) - 1)];

        return $email;
    }

    /**
     * Function to get a random postal code.
     *
     * @return string
     *   A random postal code.
     */
    function get_postal_code(): string {

        // The enumerated alphabet.
        $alphabet = range('A', 'Z');

        // Starting string of postal code.
        $postal_code = '';

        // Add a random letter and number three times to get
        // a random postal code.
        for ($i = 0; $i < 3; $i++) {
        
            $postal_code .= $alphabet[rand(0, count($alphabet) - 1)];

            $postal_code .= rand(1, 9);
        }

        return $postal_code;
    }

    /**
     * Function to get a random insurance policy.
     *
     * @return string
     *   A random insurance policy.
     */
    function get_insurance_policy(): string {

        // An enumnerated alphabet array.
        $alphabet = range('A', 'Z');

        // Get a random card length.
        $card_length = rand(5, 15);

        // The starting card value.
        $card = '';

        // Step through the random card length and either
        // add a random letter of random number.
        for ($i = 0; $i < $card_length; $i++) {

            // Add a random letter or number.
            if (rand(0, 1)) {
                $card .= rand(1, 9);
            }
            else {
                $card .= $alphabet[rand(0, count($alphabet) - 1)];
            }
        }

        return $card;
    }

    /**
     * Function to get a random ohip number.
     *
     * @return string
     *   A random ohip number.
     */
    function get_ohip(): string {

        // An enumnerated alphabet array.
        $alphabet = range('A', 'Z');

        // The starting ohip value.
        $ohip = '';

        // Add 9 random digits to the ohip.
        for ($i = 0; $i < 10; $i++) {
            $ohip .= rand(1, 9);
        }

        // Add two random letters to the ohip.
        $ohip .= $alphabet[rand(0, count($alphabet) - 1)];
        $ohip .= $alphabet[rand(0, count($alphabet) - 1)];

        return $ohip;
    }

    /**
     * Function to get random user fields.
     *
     * @param string $userRole
     *   The user role to random fields for (patient, doctor, manager).
     *
     * @return array
     *   Array of fields for the user based on the user role.
     */
    function get_rand_user_fields(string $userRole): array {

        // Get the number of addresses, male first names,
        // female first names and last names in the databse.
        // We do this to save computational time when doing
        // large generations of data.
        $num_of_addresses = get_num_of_addresses();
        $num_of_male_first_names = get_num_of_first_names('M');
        $num_of_female_first_names = get_num_of_first_names('F');
        $num_of_last_names = get_num_of_last_names();

        // Get the random address.
        $address = get_rand_address($num_of_addresses);

        // Set the address fields based on random address.
        $fields['addressLineOne'] = ($address['unit'] ?? '') . $address['address'];
        $fields['city'] = $address['city'];
        $fields['province'] = 'ON';
        $fields['addressLineTwo'] = '';

        // Get a random gender.
        $fields['gender'] = get_rand_gender();

        // Based on the gender get a random first name.
        if ($fields['gender'] == 'M') {
            $fields['firstName'] = trim(get_first_name('M',  rand(1, $num_of_male_first_names)));
        }
        else {
            $fields['firstName'] = trim(get_first_name('F', rand(1, $num_of_female_first_names)));
        }

        // Get a random last name.
        $fields['lastName'] = trim(get_last_name(rand(1, $num_of_last_names)));

        // Get a random date of birth.
        $fields['dateOfBirth'] = get_date_of_birth($userRole);

        // Get a random phone number.
        $fields['phone'] = get_phone_number();

        // Get a random email.
        $fields['email'] = get_email($fields['firstName'], $fields['lastName']);

        // Get a random postal code.
        $fields['postalCode'] = get_postal_code();

        // Return the fields.
        return $fields;
    }

    /**
     * Function to get the random patient fields.
     *
     * @return array
     *   An array of random patient fields.
     */
    function get_rand_patient_fields(): array {

        // Get a random insurance policy.
        $fields['insurancePolicyID'] = get_insurance_policy();

        // Get a random ohip.
        $fields['ohipID'] = get_ohip();

        // Return the random patient fields.
        return $fields;
    }

    /**
     * Function to get random doctor fields.
     *
     * @return array
     *   An array of random doctor fields.
     */
    function get_rand_doctor_fields(): array {

        // Get a random SIN.
        $fields['sin'] = get_sin();

        // Just use M.D. for designation.
        $fields['designation'] = 'M.D.';

        // Return the random doctor fields.
        return $fields;
    }

    /**
     * Function to get random manager fields.
     *
     * @return array
     *   An array of random manager fields.
     */
    function get_rand_manager_fields(): array {

        // Get a random SIN.
        $fields['sin'] = get_sin();

        // Return the random manager fields.
        return $fields;
    }

    /**
     * Function to get a random SIN.
     *
     * @return string
     *   An random SIN.
     */
    function get_sin(): string {

        // Starting value of the SIN.
        $sin = '';

        // Get 9 random digits for SIN.
        for ($i = 0; $i < 9; $i++) {
            $sin .= rand(1, 9);
        }

        // Return the random SIN.
        return $sin;
    }

    /**
     * Function to generate users.
     *
     * @param string $type
     *   The type of user to generate.
     * @param int $num_of_users
     *   The number of users to generate.     
     */
    function generate_users(string $type, int $num_of_users): void {

        set_time_limit(3600);

        // Counters for success and failure.
        $successes = 0;
        $failures = 0;

        for ($i = 0; $i < $num_of_users; $i++) {

            // Get the common random user fields.
            $fields = get_rand_user_fields($type);

            // Get the random fields based on the type of user.
            switch ($type) {
                case 'patient':
                    $fields = array_merge($fields, get_rand_patient_fields());
                    break;
                case 'doctor':
                    $fields = array_merge($fields, get_rand_doctor_fields());
                    break;
                case 'manager':
                    $fields = array_merge($fields, get_rand_manager_fields());
                    break;
            }

            // Get the username, if not created correctly, it will be NULL.
            $username = create_user($fields, $type);

            // If the insert worked, we can add the patient.
            if ($username) {
        
                // Get the user id from the username.
                $fields['userID'] = get_user_id_from_username($username);

                // Get the sql statement to insert user into specific table.
                switch ($type) {
                    case 'patient':
                        $insert_user_table = sql_insert_patient($fields);
                        break;
                    case 'doctor':
                        $insert_user_table = sql_insert_doctor($fields);
                        break;
                    case 'manager':
                        $insert_user_table = sql_insert_manager($fields);
                        break;
                }

                // Flag to see if inserting user into specific worked.
                $insert_user_table_flag = insertQuery($insert_user_table);

                // If this is a patient, ensure that we generate diagnosis,
                // prescriptions and appointments.
                if ($type == 'patient') {

                    // Generate some diagnosis for the patient.
                    generate_diagnosis($fields['userID']);

                    // Generate some prescriptions for the patient.
                    generate_prescriptions($fields['userID']);

                    // Generatae some appointments for the patient.
                    generate_appointments($fields['userID']);
                }
                else if ($type == 'doctor') {
                    generate_doctor_schedule($fields['userID']);
                }

                // Increment the counter based on the insert flag.
                if ($insert_user_table_flag) {
                    $successes++;
                }
                else {
                    $failures++;
                }
            }
        }

        // Output success message if there are successes.
        if ($successes) {
            $_SESSION['success_message'] = 'Successfully created ' . $successes . ' ' . $type . 's';
        }

        // Output the failure message if there are failures.
        if ($failures) {
            $_SESSION['error_message'] = 'There were ' . $failures . ' when creating ' . $type . 's';
        }

        set_time_limit(30);
    }

    /**
     * Function get random words.
     *
     * @param int $words
     *   The number of words.
     * @param int $max_length
     *   The maximum length of the words.
     *
     * @return string
     *   The random words.
     */
    function get_random_words(int $words, int $max_length): string {

        // The string to return.
        $string = '';

        // Step through for the number of words.
        for ($o = 1; $o <= $words; $o++) {

            // The list of vowels.
            $vowels = array("a", "e", "i", "o", "u");

            // The list of consonants.
            $consonants = array(
                'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
                'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
            );
     
            // The random word.
            $word = '';

            // Get a random length for the word based on length.
            $rand_length = rand(2, $max_length);

            // Get a letter for the word.
            for ($i = 1; $i <= $rand_length; $i++) {

                // Rnadomly get a vowel or consonant.
                if (rand(0, 1)) {
                    $word .= $consonants[rand(0, 19)];
                }
                else {
                    $word .= $vowels[rand(0, 4)];
                }
           }

           // Add to the string randomly.
           $string .= mb_substr($word, 0, $max_length);

           // Add a space to the string.
           $string .= " ";
        }

        // Return the random string.
        return trim(mb_substr($string, 0, -1));
    }

    /**
     * Function to get a random date and time.
     *
     * @param string $start_year
     *   The starting year for the random date.
     * @param string $end_year
     *   The end year for the random date.
     *
     * @return string
     *   A random date that is not Sat or Sun and between 9 am and 3pm,
     *   excluding the lunch hour.
     */
    function get_random_date(string $start_year, string $end_year): string {

        // Flag to keep generating dates, we do not
        // want Saturdays or Sundays.
        $gen_date = TRUE;

        // Loop until we find a good date.
        while ($gen_date == TRUE) {

            // The start and end dates to get a random date.
            $start = strtotime("1 January " . $start_year);
            $end = strtotime("31 December " . $end_year);

            // Get a random time stamp.
            $timestamp = mt_rand($start, $end);

            // Get the day of the week from the random timestamp.
            $date = date('D', $timestamp);

            // If the date is not a Sat or Sun, turn of flag.
            if ($date !== 'Sat' && $date !== 'Sun') {
                $gen_date = FALSE;
            }
        }

        return date('Y-m-d', $timestamp);
    }

    /**
     * Function to get a random date and time.
     *
     * @return string
     *   A random date that is not Sat or Sun and between 9 am and 3pm,
     *   excluding the lunch hour.
     */
    function get_random_date_time(): string {

        // Flag to keep generating dates, we do not
        // want Saturdays or Sundays.
        $gen_date = TRUE;

        // Loop until we find a good date.
        while ($gen_date == TRUE) {

            // The start and end dates to get a random date.
            $start = strtotime("1 January 2019");
            $end = strtotime("31 December 2021");

            // Get a random time stamp.
            $timestamp = mt_rand($start, $end);

            // Get the day of the week from the random timestamp.
            $date = date('D', $timestamp);

            // If the date is not a Sat or Sun, turn of flag.
            if ($date !== 'Sat' && $date !== 'Sun') {
                $gen_date = FALSE;
            }
        }

        // Flag to continue generating an hour, we do not want
        // times over lunch (12).
        $gen_hour = TRUE;

        // Keep generating hours until we do not have 12.
        while ($gen_hour) {

            // Get a random hours, since 24 hour anywhere between
            // 9 am and 3 pm (15h).
            $rand_hour = rand(9, 15);

            // If the randome hours is not 12, set flag to stop
            // generating hours.
            if ($rand_hour !== 12) {
                $gen_hour = FALSE;
            }
        }

        // Get a random minute between 0 and 59.
        $rand_min = rand(0, 59);

        // If the random hour is less than 10, add
        // the leading 0.
        if ($rand_min < 10) {
            $rand_min = '0' . $rand_min;
        }

        // Get a random second between 0 and 59.
        $rand_sec = rand(0, 59);

        // If the random second is less than 10, add
        // the leading 0.
        if ($rand_sec < 10) {
            $rand_sec = '0' . $rand_sec;
        }

        // Now we can set the time.
        $rand_time = $rand_hour . ':' . $rand_min . ':' . $rand_sec;
        
        // Return the random date in the proper format.
        return date('Y-m-d', $timestamp) . ' ' . $rand_time;
    }

    /**
     * Function to get a random disease.
     *
     * @return int
     *   A random disease ID.
     */
    function get_random_disease(): int {

        // Get all the disease ids.
        $get_disease_ids = sql_get_disease_ids();
        $disease_ids = getAssociativeArray($get_disease_ids);

        // Get a random disease ID.
        $rand_disease_id = rand(0, count($disease_ids) - 1);

        // Return the random disease ID.
        return $disease_ids[$rand_disease_id]['diseaseID'];
    }

    /**
     * Function to get a random doctor.
     *
     * @return int
     *   The id of a random doctor.
     */
    function get_random_doctor(): int {

        // Get all the doctor ids.
        $get_doctor_ids = sql_get_doctor_ids();
        $doctor_ids = getAssociativeArray($get_doctor_ids);

        // Get a random doctor.
        $rand_doctor_id = rand(0, count($doctor_ids) - 1);

        return $doctor_ids[$rand_doctor_id]['userID'];
    }

    /**
     * Function to create random diagnosis for a patient.
     *
     * @param int $patientID
     *   The id of the patient.
     */
    function generate_diagnosis(int $patientID): void {

        // Get a random number of diagnosis.
        $rand_diagnosis = rand(6, 20);

        // Generate the random number of diagnosis.
        for ($i = 0; $i < $rand_diagnosis; $i++) {

            // Get a random doctor ID.
            $doctorID = get_random_doctor();

            // Get a random disease description.
            $diseaseDescription = get_random_words(rand(3, 10), 10);

            // Get a random disease ID.
            $diseaseID = get_random_disease();

            // Get a random date.
            $date = get_random_date_time();

            // Get sql to insert a diagnosis.
            $insert_diagnosis = sql_insert_diagnosis($doctorID, $patientID, $diseaseDescription, $diseaseID, $date);

            // Perform the insert.
            $insert_diagnosis_flag = insertQuery($insert_diagnosis);
        }
    }

    /**
     * Function to generate prescriptions for a patient.
     *
     * @param int $patientID
     *  The id of the patient.
     */
    function generate_prescriptions(int $patientID): void {

        global $conn;

        // Get the diagnosis for a patient.
        $get_diagnosis = sql_get_all_diagnosis($patientID);
        $diagnosis = getAssociativeArray($get_diagnosis);

        // Step through each diagnosis and create at least one prescription.
        for ($i = 0; $i < count($diagnosis); $i++) {

            // Get a random amount of prescriptions to add but at least one.
            $rand_prescription = rand(1, 4);

            // Generate a random amount of prescriptions.
            for ($j = 0; $j < $rand_prescription; $j++) {

                // Reset the fields array.
                $feilds = [];

                // Set the diagnosis id.
                $fields['diagnosisID'] = $diagnosis[$i]['diagnosisID'];

                // Get a random if drug is required for the prescription.
                if (rand(0, 1)) {
                    $fields['requireDrug'] = 'require';
                }
                else {
                    $fields['requireDrug'] = 'norequire';
                }

                // If there is a drug required, get the rest of the fields.
                if ($fields['requireDrug'] == 'require') {

                    // Get all the drug codes.
                    $get_drug_codes = sql_get_drug_codes();
                    $drug_codes = getAssociativeArray($get_drug_codes);

                    // Get a random drug code.
                    $rand_drug_code = rand(0, count($drug_codes));
                    $fields['drugCode'] = $drug_codes[$rand_drug_code]['drugCode'];

                    // Get some random words for the dosage.
                    $fields['dosage'] = get_random_words(rand(3, 10), 6);

                    // Get a random insturctions.
                    if (rand(0, 1)) {
                        $fields['instructions'] = $diseaseDescription = get_random_words(rand(3, 10), 15);
                    }
                    else {
                        $fields['instructions'] =  NULL;
                    }

                    // Set the prescription field to NULL, not using it.
                    $fields['prescription'] = NULL;
                }
                else {

                    // Get random words for the prescription field.
                    $fields['prescription'] = $diseaseDescription = get_random_words(rand(3, 10), 15);
                }

                // Get the sql to insert the prescription.
                $insert_prescription = sql_insert_prescription($fields, $patientID);

                // Perform the insertion and if successful, check if
                // we have to insert the prescription drugs.
                // If the insertion failed, set error message.
                if ($conn->query($insert_prescription) === TRUE) {

                    // If a drug is required, then get the last inserted id,
                    // and insert the prescription drugs.
                    if ($fields['requireDrug'] == 'require') {

                        // Set the prescription id from the last inserted id.
                        $fields['prescriptionID'] = $conn->insert_id;

                        // Get the sql to insert the prescription drug.
                        $insert_prescription_drug = sql_insert_prescription_drug($fields);

                        // Insert the prescription drug.
                        $insert_prescription_drug_flag = insertQuery($insert_prescription_drug);
                    }
                }
            }
        }
    }

    /**
     * Function to get random days off for a doctor.
     *
     * @return array
     *   An array of randomly generated days off for a doctor.
     */
    function get_random_doctor_schedule_dates(): array {

        // The array of days off.
        $dates = [];

        // Step through each of the years and generate days off.
        for ($year = RAND_START_YEAR; $year <= RAND_END_YEAR; $year++) {

            // Generate 20-40 random dates.
            for ($i = 0; $i < rand(20, 40); $i++) {

                // Flag to keep generating dates, we do not
                // want Saturdays or Sundays.
                $gen_date = TRUE;

                // Loop until we find a good date.
                while ($gen_date == TRUE) {

                    // The start and end dates to get a random date.
                    $start = strtotime("1 January ". $year);
                    $end = strtotime("31 December " . $year);

                    // Get a random time stamp.
                    $timestamp = mt_rand($start, $end);

                    // Get the day of the week from the random timestamp.
                    $date = date('D', $timestamp);

                    // If the date is not a Sat or Sun, turn of flag.
                    if ($date !== 'Sat' && $date !== 'Sun') {

                        // Get the proper date format.
                        $insert_date = date('Y-m-d', $timestamp);

                        // Ensure that the date is not already in the array.
                        // If it is not, put it in and set the flag to stop generating.
                        if (!in_array($insert_date, $dates)) {
                            $gen_date = FALSE;
                            $dates[] = $insert_date;
                        }
                    }
                }
            }
        }

        return $dates;
    }

    /**
     * Function to generate random doctor schedule.
     *
     * @param int $doctorID
     *   The id of a doctor.
     */
    function generate_doctor_schedule(int $doctorID): void {

        // Get the random dates for a doctors schedule.
        $dates = get_random_doctor_schedule_dates();

        // Step through each of the dates and insert them.
        foreach ($dates as $date) {
            $insert_doctor_schedule = sql_add_doctor_schedule($doctorID, $date);
            $insert_doctor_schedule_flag = insertQuery($insert_doctor_schedule);
        }
    }

    /**
     * Function to generate a bunch of appointments for patients.
     *
     * @param int $patientID
     *   The id of the patient.
     */
    function generate_appointments(int $patientID): void {

        // Generate a random amount of appointments between the defined
        // random start and end years.
        for ($i = RAND_START_YEAR; $i <= RAND_END_YEAR; $i++) {

            // Get a random amount of appointments to create.
            $rand_patient_appointments = rand(6, 15);

            // Create the random amount of appointments.
            for ($j = 0; $j < $rand_patient_appointments; $j++) {

                // Flag to keep generating an appointment.
                $gen_appointment = TRUE;

                // The counter to keep track of number of attempted
                // generations, used so do not hit infinite loop,
                // if there are no more appointments.
                $gen_counter = 0;

                // Keep generating appointments until the flag is unset.
                while ($gen_appointment) {

                    // Get a random date that is not Sat or Sun.
                    $date = get_random_date($i, $i);

                    // Flag to continue generating an hour, we do not want
                    // times over lunch (12).
                    $gen_hour = TRUE;

                    // Keep generating hours until we do not have 12.
                    while ($gen_hour) {

                        // Get a random hours, since 24 hour anywhere between
                        // 9 am and 3 pm (15h).
                        $rand_hour = rand(9, 15);

                        // If the randome hours is not 12, set flag to stop
                        // generating hours.
                        if ($rand_hour !== 12) {
                            $gen_hour = FALSE;
                        }
                    }

                    // Set the start time.
                    $start_time = $date . ' ' . $rand_hour . ':';

                    // List of mins to choose from.
                    $list_of_mins = [
                        '00',
                        '20',
                        '40',
                    ];

                    // Get a random minutes.
                    $start_time .= $list_of_mins[rand(0, 2)] . ':00';

                    // Get the end time which is 20 minutes past the start time.
                    $end_time = strtotime("+20 minutes", strtotime($start_time));

                    // Set the format for the end time.
                    $end_time = date('Y-m-d H:i:s', $end_time);

                    // Get the ids of doctors available at that time and date.
                    $get_doctor_ids = sql_get_doctor_appointments_by_day_and_start_time($date);
                    $doctor_ids = getAssociativeArray($get_doctor_ids);

                    // If there is at least one doctor avaiable.
                    // If not then increment the gen counter.
                    if (count($doctor_ids) > 0) {

                        // Get a random doctorID.
                        $doctorID = $doctor_ids[rand(0, count($doctor_ids) - 1)]['doctorID'];

                        // Get the number of appointments with that doctor and date.
                        $get_appointment_count = sql_get_appointment_count($doctorID, $start_time);
                        $appointment_count = getAssociativeArray($get_appointment_count);
                        $appointment_count = $appointment_count[0]['numOfAppointments'];

                        // If there is no appoinment add it.
                        // If not increment the gen counter.
                        if ($appointment_count == 0) {
                            $insert_appointment = sql_insert_appointment($patientID, $doctorID, $date, $start_time, $end_time);
                            $insert_appointment_flag = insertQuery($insert_appointment);
                            $gen_appointment = FALSE;
                        }
                        else {
                            $gen_counter++;
                        }
                    }
                    else {
                        $gen_counter++;
                    }

                    // If we have hit the generation counter limit, set the flag,
                    // so that we do not end up in an infite loop.
                    if ($gen_counter > 8) {
                        $gen_appointment = FALSE;
                    }
                }
            }
        }
     }
?>